<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mantrada_dachau' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );


/** API related Configuration */ 

/* USE FOLLOWING AS API_PATH FOR LIVE PAYMENT ENVIRONMENT
https://api.payengine.de/v1 
AND
FOLLOWING FOR TEST PAYMENT ENVIRONMENT
https://apitest.payengine.de/v1
*/
define('API_PATH', 'https://apitest.payengine.de/v1');
define('MERCHANT_ID', '');
define('API_PRIVATE_KEY', '');

/** END of API related Configuration */




/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[29gA{^MF}5!Zpi<Y)gLnbDtv%%fj~ /iBXkeXK*rk95*}9!gVP$Z[M,ZmFP9O/f' );
define( 'SECURE_AUTH_KEY',  'z[*IuDw,duDAH4Ic8+RX8~P84I_ARwL(fM4(Va+iqHU?}{9GPn!a s.p}g<jPNc3' );
define( 'LOGGED_IN_KEY',    '}Fd%Son9_F|dV05qs5KX-},m~>m/[>c%[epI&Rnr%>,-vh`odS&+Te,6L;aL?Jo=' );
define( 'NONCE_KEY',        '3FWl`=T6z9ZdVXgicY)U5T>]j3=w*hN=e!E4*_z{U#&qww)LIBPnaJL`W]My+YSy' );
define( 'AUTH_SALT',        'YSkFzq, g4f=GhW!#p0WnU6.(ovU$P[eO^!8MWP<L1a!wGf^yP/pmbbDT[$^P=L>' );
define( 'SECURE_AUTH_SALT', 'oY6E8(NsNgZEa7E:RG}Kj*xsvi_CX[_m9T}S!EskJ`+VPEH<_,o`a;0ymi)c5w29' );
define( 'LOGGED_IN_SALT',   'ZS7O_C-Fx4oe5l1MJ<HE0nKBr%7M.[2,CCA(JN<+i@},>7(8b?s_r:r%:&^Ff3:i' );
define( 'NONCE_SALT',       'KEj-&x|nDJo35,j,tnF;[x+ &UCpb:nrKS#W1myc9CXAK1qb@@|)y0(| x[7UPN9' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
