<?php get_header(); ?>
<div class="page-contents-wrap">
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
	
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-page-wrapper entry-content clearfix">
	<?php
	if ( has_post_thumbnail() ) {
		echo '<div class="post-format-media">';

		$posthead_size="cinnamon-restaurant-image-full";
		$lightbox_status= get_post_meta($post->ID, 'pagemeta_meta_lightbox', true);
		$image_link=cinnamon_restaurant_featured_image_link($post->ID);

		$open_link = false;
		echo cinnamon_restaurant_display_post_image (
		$post->ID,
		$have_image_url=false,
		$link=false,
		$type=$posthead_size,
		$post->post_title,
		$class="postformat-image" 
		);
		echo '</div>';
	}
	the_excerpt();
	?>
	</div>
				
	</div><!-- .entry-content -->

	<?php endwhile; else: ?>
<?php endif; ?>
</div>
<?php get_footer(); ?>