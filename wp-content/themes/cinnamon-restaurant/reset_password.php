<?php /* Template Name: reset_password */ ?>
<?php
if(isset($_GET['uid']) && isset($_GET['hsh'])){
global $wpdb;
$user_table = $wpdb->prefix.app_users;
$user_id = $_GET['uid'];
$hash = $_GET['hsh'];

} else {
    die("invalid link provided!");
}

 $user = $wpdb->get_results("select * from $user_table  where ID = $user_id and user_activation_key = '$hash' ");
if(count($user) == 0){
 die("Invalid link provided!");
} 


 ?>
 

<div class="container text-center" style="vertical-align:middle">

<div class="password-reset-form">
<div class="text-center logo">
<h3 style="font-family: 'Josefin Sans', sans-serif; text-align: center; margin-top: 8em; color: #90bc1d;">Reset Password</h3>
<p style="font-family: 'Josefin Sans', sans-serif; text-align: center; color: #90bc1d;">Please type in your New Password Below.</p>

</div>
<form action="" method="POST">
    
<div class="form-group" style="font-family: 'Josefin Sans', sans-serif; margin-top: 2em; text-align: center;">
<p>
<input class="my-form" style="font-family: 'Josefin Sans', sans-serif; width: 16em; padding: 20px; font-size: 16px; color: #90bc1d; border-radius: 0.5em; -webkit-box-shadow: 2px 17px 23px -12px rgba(0,0,0,0.22); -moz-box-shadow: 2px 17px 23px -12px rgba(0,0,0,0.22); box-shadow: 2px 17px 23px -12px rgba(0,0,0,0.22); border: white;" id = "password" name="password" required="" type="password" placeholder="New Password" />
</p>
<p>
<input  class="my-form" style="font-family: 'Josefin Sans', sans-serif; width: 16em; padding: 20px; ; font-size: 16px; color: #90bc1d; border-radius: 0.5em; margin-top: 1em; -webkit-box-shadow: 2px 17px 23px -12px rgba(0,0,0,0.22); -moz-box-shadow: 2px 17px 23px -12px rgba(0,0,0,0.22); box-shadow: 2px 17px 23px -12px rgba(0,0,0,0.22); border: white;" id = "repassword" name="repassword" required="" type="password" placeholder="Re-type Password" />
</p>
<button class="reset-btn" style="font-family: 'Josefin Sans', sans-serif; width: 150px; height: 39px; left: 231px; top: 352px; background: #90BC1D; border-radius: 7px; border: none; color: white; font-size: 16px; margin-top: 2em; -webkit-box-shadow: 2px 17px 23px -12px rgba(0,0,0,0.22); -moz-box-shadow: 2px 17px 23px -12px rgba(0,0,0,0.22); box-shadow: 2px 17px 23px -12px rgba(0,0,0,0.22);" type="submit" onclick = "return warn_alert()">Reset Password</button>


</div>
</form></div>

</div>
<script type = "text/javascript">
function warn_alert(){
            var password = document.getElementById("password").value;
            var re_password = document.getElementById("repassword").value;

      if(password != re_password){
            alert("Password did not match, please try again!");
            document.getElementById("password").focus();
            return false;
      }

        
}
</script>
<?php
if(!empty($_POST) && $_SERVER['REQUEST_METHOD'] == 'POST'){
$user_password = $_POST['password'];
    $hash = password_hash($user_password, PASSWORD_DEFAULT);
$update_status=$wpdb->update($user_table,array('user_pass' => $hash),array('ID' => $user_id ));

if($update_status){
    $wpdb->update($user_table,array('user_activation_key' => ""),array('ID' => $user_id ));
    $home_url = get_home_url();
    $link = $home_url."/password-reset-successful/";
    wp_redirect($link);
}

} 
?>