<?php
function cinnamon_restaurant_post_metadata() {
	$mtheme_sidebar_options = cinnamon_restaurant_generate_sidebarlist("post");

	$mtheme_imagepath =  get_template_directory_uri() . '/framework/options/images/metaboxes/';

	$mtheme_post_metapack=array();

	$mtheme_post_metapack['main'] = array(
		'id' => 'common-pagemeta-box',
		'title' => esc_html__('General Page Metabox','cinnamon-restaurant'),
		'page' => 'post',
		'context' => 'normal',
		'priority' => 'core',
		'fields' => array(
			array(
				'name' => esc_html__('Page Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_page_section_id',
				'type' => 'break',
				'sectiontitle' => esc_html__('Page Settings','cinnamon-restaurant'),
				'std' => ''
				),
			array(
				'name' => esc_html__('Attach Images','cinnamon-restaurant'),
				'id' => 'pagemeta_image_attachments',
				'std' => esc_html__('Upload Images','cinnamon-restaurant'),
				'type' => 'image_gallery',
				'desc' => '<div class="metabox-note">' . esc_html__('Attach images. Images can be used to generate when using shortcodes.','cinnamon-restaurant') . '</div>'
				),
			array(
				'name' => esc_html__('Author Bio','cinnamon-restaurant'),
				'id' => 'pagemeta_post_authorbio',
				'std' => 'rightsidebar',
				'desc' => esc_html__('Display Author Bio','cinnamon-restaurant'),
				'type' => 'select',
				'options' => array(
					'default' => esc_html__('Theme options default','cinnamon-restaurant'),
					'activate' => esc_html__('Activate','cinnamon-restaurant'),
					'disable' => esc_html__('Disable','cinnamon-restaurant')
					)
				),
			array(
				'name' => esc_html__('Page Layout','cinnamon-restaurant'),
				'id' => 'pagemeta_sep_page_options',
				'type' => 'seperator',
				),
			array(
				'name' => esc_html__('Page Style','cinnamon-restaurant'),
				'id' => 'pagemeta_pagestyle',
				'type' => 'image',
				'std' => 'rightsidebar',
				'desc' => esc_html__('Note: Edge to Edge - same as 100% Width Template','cinnamon-restaurant'),
				'options' => array(
					'rightsidebar' => $mtheme_imagepath . 'page-right-sidebar.png',
					'leftsidebar' => $mtheme_imagepath . 'page-left-sidebar.png',
					'nosidebar' => $mtheme_imagepath . 'page-no-sidebar.png',
					'edge-to-edge' => $mtheme_imagepath . 'page-edge-to-edge.png')
				),
			array(
				'name' => esc_html__('Page Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_background_section_id',
				'type' => 'break',
				'sectiontitle' => esc_html__('Page Background','cinnamon-restaurant'),
				'std' => ''
				),
			array(
				'name' => esc_html__('Page Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_sep-page_backgrounds',
				'type' => 'seperator',
				),
			array(
				'name' => esc_html__('Header Type','cinnamon-restaurant'),
				'id' => 'pagemeta_header_type',
				'type' => 'select',
				'std' => 'enable',
				'desc' => esc_html__('Header Type for Horizontal menu','cinnamon-restaurant'),
				'options' => array(
					'default' => esc_html__('Default','cinnamon-restaurant'),
					'transparent' => esc_html__('Transparent','cinnamon-restaurant'),
					'transparent-invert' => esc_html__('Transparent Inverse','cinnamon-restaurant')
					)
				),
			array(
				'name' => esc_html__('Choice of Sidebar','cinnamon-restaurant'),
				'id' => 'pagemeta_sidebar_choice',
				'type' => 'select',
				'desc' => esc_html__('For Sidebar Active Pages and Posts','cinnamon-restaurant'),
				'options' => $mtheme_sidebar_options
				),
			array(
				'name' => esc_html__('Switch Menu','cinnamon-restaurant'),
				'id' => 'pagemeta_menu_choice',
				'type' => 'select',
				'desc' => esc_html__('Select a different menu for this page','cinnamon-restaurant'),
				'options' => cinnamon_restaurant_generate_menulist()
				),
			array(
				'name' => esc_html__('Page Title','cinnamon-restaurant'),
				'id' => 'pagemeta_page_title',
				'type' => 'select',
				'desc' => esc_html__('Page Title','cinnamon-restaurant'),
				'std' => 'default',
				'options' => array(
					'default' => esc_html__('Default','cinnamon-restaurant'),
					'show' => esc_html__('Show','cinnamon-restaurant'),
					'hide' => esc_html__('Hide','cinnamon-restaurant')
					)
				)
		)
);

$mtheme_post_metapack['video'] = array(
	'id' => 'video-meta-box',
	'title' => esc_html__('Video Metabox','cinnamon-restaurant'),
	'page' => 'post',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => esc_html__('HTML5 Video','cinnamon-restaurant'),
			'id' => 'pagemeta_video_meta_section1_id',
			'type' => 'break',
			'sectiontitle' => esc_html__('HTML5 Video','cinnamon-restaurant'),
			'std' => ''
			),
		array(
			'name' => esc_html__('Video Title','cinnamon-restaurant'),
			'id' => 'pagemeta_video_title',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Title for Video','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('M4V File URL','cinnamon-restaurant'),
			'id' => 'pagemeta_video_m4v_file',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Enter M4V File URL ( Required )','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('OGV File URL','cinnamon-restaurant'),
			'id' => 'pagemeta_video_ogv_file',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Enter OGV File URL','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('Poster Image','cinnamon-restaurant'),
			'id' => 'pagemeta_video_poster_file',
			'type' => 'upload',
			'target' => 'image',
			'std' => '',
			'desc' => esc_html__('Poster Image','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('Video Hosts','cinnamon-restaurant'),
			'id' => 'pagemeta_video_meta_section2_id',
			'type' => 'break',
			'std' => '',
			'sectiontitle' => esc_html__('Video Hosts','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('Youtube Video ID','cinnamon-restaurant'),
			'id' => 'pagemeta_video_youtube_id',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Youtube video ID','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('Vimeo Video ID','cinnamon-restaurant'),
			'id' => 'pagemeta_video_vimeo_id',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Vimeo video ID','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('Daily Motion Video ID','cinnamon-restaurant'),
			'id' => 'pagemeta_video_dailymotion_id',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Daily Motion video ID','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('Google Video ID','cinnamon-restaurant'),
			'id' => 'pagemeta_video_google_id',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Google video ID','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('Video Embed Code','cinnamon-restaurant'),
			'id' => 'pagemeta_video_embed_code',
			'type' => 'textarea',
			'std' => '',
			'desc' => esc_html__('Video Embed code. You can grab embed codes from hosted video sites.','cinnamon-restaurant')
			)
		)
	);

$mtheme_post_metapack['audio'] = array(
	'id' => 'audio-meta-box',
	'title' => esc_html__('Audio Metabox','cinnamon-restaurant'),
	'page' => 'post',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => esc_html__('Audio Embed code','cinnamon-restaurant'),
			'id' => 'pagemeta_audio_meta_section1_id',
			'type' => 'break',
			'sectiontitle' => esc_html__('Audio Embed code','cinnamon-restaurant'),
			'std' => ''
			),
		array(
			'name' => esc_html__('Audio Embed code','cinnamon-restaurant'),
			'id' => 'pagemeta_audio_embed',
			'type' => 'textarea',
			'std' => '',
			'desc' => esc_html__('eg. Soundcloud embed code','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('HTML5 Audio','cinnamon-restaurant'),
			'id' => 'pagemeta_audio_meta_section2_id',
			'type' => 'break',
			'sectiontitle' => esc_html__('HTML5 Audio','cinnamon-restaurant'),
			'std' => ''
			),
		array(
			'name' => esc_html__('MP3 file','cinnamon-restaurant'),
			'id' => 'pagemeta_meta_audio_mp3',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Please provide full url. eg. http://www.domain.com/path/audiofile.mp3','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('M4A file','cinnamon-restaurant'),
			'id' => 'pagemeta_meta_audio_m4a',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Please provide full url. eg. http://www.domain.com/path/audiofile.m4a','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('OGA file','cinnamon-restaurant'),
			'id' => 'pagemeta_meta_audio_ogg',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Please provide full url. eg. http://www.domain.com/path/audiofile.ogg','cinnamon-restaurant')
			)
		)
	);

$mtheme_post_metapack['link'] = array(
	'id' => 'link-meta-box',
	'title' => esc_html__('Link Metabox','cinnamon-restaurant'),
	'page' => 'post',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => esc_html__('Link URL','cinnamon-restaurant'),
			'id' => 'pagemeta_meta_link',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Please provide full url. eg. http://www.domain.com/path/','cinnamon-restaurant')
			)
		)
	);

$mtheme_post_metapack['image'] = array(
	'id' => 'image-meta-box',
	'title' => esc_html__('Image Metabox','cinnamon-restaurant'),
	'page' => 'post',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => esc_html__('Enable Lightbox','cinnamon-restaurant'),
			'id' => 'pagemeta_meta_lightbox',
			'type' => 'select',
			'options' => array(
				'enabled_lightbox' => esc_html__('Enable Lightbox','cinnamon-restaurant'),
				'disable_lightbox' => esc_html__('Disable Lighbox','cinnamon-restaurant')
				)
			)
		)
	);

$mtheme_post_metapack['quote'] = array(
	'id' => 'quote-meta-box',
	'title' => esc_html__('Quote Metabox','cinnamon-restaurant'),
	'page' => 'post',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => esc_html__('Quote','cinnamon-restaurant'),
			'id' => 'pagemeta_meta_quote',
			'type' => 'textarea',
			'std' => '',
			'desc' => esc_html__('Enter quote here','cinnamon-restaurant')
			),
		array(
			'name' => esc_html__('Author','cinnamon-restaurant'),
			'id' => 'pagemeta_meta_quote_author',
			'type' => 'text',
			'std' => '',
			'desc' => esc_html__('Author','cinnamon-restaurant')
			)
		)
	);
return $mtheme_post_metapack;
}
add_action('admin_init', 'cinnamon_restaurant_add_boxes');

// Add meta box
function cinnamon_restaurant_add_boxes() {
	$mtheme_post_metapack = cinnamon_restaurant_post_metadata();
	add_meta_box($mtheme_post_metapack['main']['id'], $mtheme_post_metapack['main']['title'], 'cinnamon_restaurant_common_show_box', $mtheme_post_metapack['main']['page'], $mtheme_post_metapack['main']['context'], $mtheme_post_metapack['main']['priority']);
	add_meta_box($mtheme_post_metapack['video']['id'], $mtheme_post_metapack['video']['title'], 'cinnamon_restaurant_video_show_box', $mtheme_post_metapack['video']['page'], $mtheme_post_metapack['video']['context'], $mtheme_post_metapack['video']['priority']);
	add_meta_box($mtheme_post_metapack['link']['id'], $mtheme_post_metapack['link']['title'], 'cinnamon_restaurant_link_show_box', $mtheme_post_metapack['link']['page'], $mtheme_post_metapack['link']['context'], $mtheme_post_metapack['link']['priority']);
	add_meta_box($mtheme_post_metapack['image']['id'], $mtheme_post_metapack['image']['title'], 'cinnamon_restaurant_image_show_box', $mtheme_post_metapack['image']['page'], $mtheme_post_metapack['image']['context'], $mtheme_post_metapack['image']['priority']);
	add_meta_box($mtheme_post_metapack['quote']['id'], $mtheme_post_metapack['quote']['title'], 'cinnamon_restaurant_quote_show_box', $mtheme_post_metapack['quote']['page'], $mtheme_post_metapack['quote']['context'], $mtheme_post_metapack['quote']['priority']);
	add_meta_box($mtheme_post_metapack['audio']['id'], $mtheme_post_metapack['audio']['title'], 'cinnamon_restaurant_audio_show_box', $mtheme_post_metapack['audio']['page'], $mtheme_post_metapack['audio']['context'], $mtheme_post_metapack['audio']['priority']);
}

// Callback function to show fields in meta box
function cinnamon_restaurant_video_show_box() {
	$mtheme_post_metapack = cinnamon_restaurant_post_metadata();
	$mtheme_video_meta_box = $mtheme_post_metapack['video'];
	cinnamon_restaurant_generate_metaboxes($mtheme_video_meta_box, get_the_id() );
}

function cinnamon_restaurant_audio_show_box() {
	$mtheme_post_metapack = cinnamon_restaurant_post_metadata();
	$mtheme_audio_meta_box = $mtheme_post_metapack['audio'];
	cinnamon_restaurant_generate_metaboxes($mtheme_audio_meta_box, get_the_id() );
}

function cinnamon_restaurant_common_show_box() {
	$mtheme_post_metapack = cinnamon_restaurant_post_metadata();
	$mtheme_common_meta_box = $mtheme_post_metapack['main'];
	cinnamon_restaurant_generate_metaboxes($mtheme_common_meta_box,get_the_id());
}

function cinnamon_restaurant_link_show_box() {
	$mtheme_post_metapack = cinnamon_restaurant_post_metadata();
	$mtheme_link_meta_box = $mtheme_post_metapack['link'];
	cinnamon_restaurant_generate_metaboxes($mtheme_link_meta_box, get_the_id() );
}

function cinnamon_restaurant_image_show_box() {
	$mtheme_post_metapack = cinnamon_restaurant_post_metadata();
	$mtheme_image_meta_box = $mtheme_post_metapack['image'];
	cinnamon_restaurant_generate_metaboxes($mtheme_image_meta_box, get_the_id() );
}

function cinnamon_restaurant_quote_show_box() {
	$mtheme_post_metapack = cinnamon_restaurant_post_metadata();
	$mtheme_quote_meta_box = $mtheme_post_metapack['quote'];
	cinnamon_restaurant_generate_metaboxes($mtheme_quote_meta_box, get_the_id() );
}
?>