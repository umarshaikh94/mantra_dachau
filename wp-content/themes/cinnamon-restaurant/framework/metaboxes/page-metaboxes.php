<?php
function cinnamon_restaurant_page_metadata() {
	$mtheme_sidebar_options = cinnamon_restaurant_generate_sidebarlist("page");

	$mtheme_imagepath =  get_template_directory_uri() . '/framework/options/images/metaboxes/';

	$mtheme_common_page_box = array(
		'id' => 'common-pagemeta-box',
		'title' => esc_html__('General Page Metabox','cinnamon-restaurant'),
		'page' => 'page',
		'context' => 'normal',
		'priority' => 'core',
		'fields' => array(
			array(
				'name' => esc_html__('Page Options','cinnamon-restaurant'),
				'id' => 'pagemeta_page_section_id',
				'type' => 'break',
				'sectiontitle' => esc_html__('Page Options','cinnamon-restaurant'),
				'std' => ''
				),
			array(
				'name' => esc_html__('Page Options','cinnamon-restaurant'),
				'id' => 'pagemeta_sep_page_options',
				'type' => 'seperator',
				),
			array(
				'name' => esc_html__('Attach Images','cinnamon-restaurant'),
				'id' => 'pagemeta_image_attachments',
				'std' => esc_html__('Upload Images','cinnamon-restaurant'),
				'type' => 'image_gallery',
				'desc' => '<div class="metabox-note">' . esc_html__('Attach images. Images can be used to generate when using shortcodes.','cinnamon-restaurant') . '</div>'
				),
			array(
				'name' => esc_html__('Page Layout','cinnamon-restaurant'),
				'id' => 'pagemeta_sep_page_options',
				'type' => 'seperator',
				),
			array(
				'name' => esc_html__('Page Style','cinnamon-restaurant'),
				'id' => 'pagemeta_pagestyle',
				'type' => 'image',
				'std' => 'rightsidebar',
				'desc' => esc_html__('Note: Edge to Edge - same as 100% Width Template','cinnamon-restaurant'),
				'options' => array(
					'rightsidebar' => $mtheme_imagepath . 'page-right-sidebar.png',
					'leftsidebar' => $mtheme_imagepath . 'page-left-sidebar.png',
					'nosidebar' => $mtheme_imagepath . 'page-no-sidebar.png',
					'split-page' => $mtheme_imagepath . 'page-split-page.png',
					'edge-to-edge' => $mtheme_imagepath . 'page-edge-to-edge.png')
				),
			array(
				'name' => esc_html__('Page Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_background_section_id',
				'type' => 'break',
				'sectiontitle' => esc_html__('Page Background','cinnamon-restaurant'),
				'std' => ''
				),
			array(
				'name' => esc_html__('Page Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_sep-page_backgrounds',
				'type' => 'seperator',
				),
			array(
				'name' => esc_html__('Header Type','cinnamon-restaurant'),
				'id' => 'pagemeta_header_type',
				'type' => 'select',
				'std' => 'enable',
				'desc' => esc_html__('Header Type for Horizontal menu','cinnamon-restaurant'),
				'options' => array(
					'default' => esc_html__('Default','cinnamon-restaurant'),
					'transparent' => esc_html__('Transparent','cinnamon-restaurant'),
					'transparent-invert' => esc_html__('Transparent Inverse','cinnamon-restaurant')
					)
				),
			array(
				'name' => esc_html__('Choice of Sidebar','cinnamon-restaurant'),
				'id' => 'pagemeta_sidebar_choice',
				'type' => 'select',
				'desc' => esc_html__('For Sidebar Active Pages and Posts','cinnamon-restaurant'),
				'options' => $mtheme_sidebar_options
				),
			array(
				'name' => esc_html__('Switch Menu','cinnamon-restaurant'),
				'id' => 'pagemeta_menu_choice',
				'type' => 'select',
				'desc' => esc_html__('Select a different menu for this page','cinnamon-restaurant'),
				'options' => cinnamon_restaurant_generate_menulist()
				),
			array(
				'name' => esc_html__('Page Title','cinnamon-restaurant'),
				'id' => 'pagemeta_page_title',
				'type' => 'select',
				'desc' => esc_html__('Page Title','cinnamon-restaurant'),
				'std' => 'default',
				'options' => array(
					'default' => esc_html__('Default','cinnamon-restaurant'),
					'show' => esc_html__('Show','cinnamon-restaurant'),
					'hide' => esc_html__('Hide','cinnamon-restaurant')
					)
				)
		)
	);
	return $mtheme_common_page_box;
}
add_action('admin_init', 'cinnamon_restaurant_add_box');
// Add meta box
function cinnamon_restaurant_add_box() {
	add_meta_box('common-pagemeta-box', esc_html__('General Page Metabox','cinnamon-restaurant'), 'cinnamon_restaurant_common_show_pagebox', 'page', 'normal', 'core');
}

function cinnamon_restaurant_common_show_pagebox() {
	$mtheme_common_page_box = cinnamon_restaurant_page_metadata();
	cinnamon_restaurant_generate_metaboxes( $mtheme_common_page_box,get_the_id() );
}
?>