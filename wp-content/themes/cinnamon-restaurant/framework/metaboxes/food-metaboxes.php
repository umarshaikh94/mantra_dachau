<?php
function cinnamon_restaurant_food_metadata() {
	$mtheme_sidebar_options = cinnamon_restaurant_generate_sidebarlist("food");
	
	$bg_slideshow_pages = get_posts('post_type=mtheme_featured&orderby=title&numberposts=-1&order=ASC');

	if ($bg_slideshow_pages) {
		$options_bgslideshow['none'] = "Not Selected";
		foreach($bg_slideshow_pages as $key => $list) {
			$custom = get_post_custom($list->ID);
			if ( isset($custom["fullscreen_type"][0]) ) { 
				$slideshow_type=$custom["fullscreen_type"][0]; 
			} else {
				$slideshow_type="";
			}
			if ($slideshow_type<>"Fullscreen-Video") {
				$options_bgslideshow[$list->ID] = $list->post_title;
			}
		}
	} else {
		$options_bgslideshow[0]="Featured pages not found.";
	}

	$mtheme_imagepath =  get_template_directory_uri() . '/framework/options/images/metaboxes/';
	$mtheme_imagepath_alt =  get_template_directory_uri() . '/framework/options/images/';

	$mtheme_food_box = array(
		'id' => 'foodmeta-box',
		'title' => esc_html__('Food Metabox','cinnamon-restaurant'),
		'page' => 'page',
		'context' => 'normal',
		'priority' => 'core',
		'fields' => array(
			array(
				'name' => esc_html__('Food Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_food_section_id',
				'type' => 'break',
				'sectiontitle' => esc_html__('Food Settings','cinnamon-restaurant'),
				'std' => ''
				),
			array(
				'name' => esc_html__('Food Options','cinnamon-restaurant'),
				'id' => 'pagemeta_sep_page_options',
				'type' => 'seperator',
				),
			array(
				'name' => esc_html__('Food Price','cinnamon-restaurant'),
				'id' => 'pagemeta_price_food',
				'type' => 'text',
				'heading' => 'subhead',
				'desc' => esc_html__('Price of Food','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => esc_html__('Food Size Variations ( Size ) ( Price )','cinnamon-restaurant'),
				'id' => 'pagemeta_price_repeat',
				'type' => 'repeat_text',
				'heading' => 'subhead',
				'desc' => esc_html__('Variation Price of Food','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => esc_html__('Purchase URL','cinnamon-restaurant'),
				'id' => 'pagemeta_purchase_food',
				'type' => 'text',
				'heading' => 'subhead',
				'desc' => esc_html__('Purchase URL','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => esc_html__('New Addition','cinnamon-restaurant'),
				'id' => 'pagemeta_new_food',
				'type' => 'select',
				'desc' => esc_html__('Display as a new item.','cinnamon-restaurant'),
				'std' => 'no',
				'options' => array(
					'no' => esc_html__('No','cinnamon-restaurant'),
					'yes' => esc_html__('Yes','cinnamon-restaurant')
				)
			),
			array(
				'name' => esc_html__('Chef Recommended','cinnamon-restaurant'),
				'id' => 'pagemeta_chef_food',
				'type' => 'select',
				'desc' => esc_html__('Display as chef recommended.','cinnamon-restaurant'),
				'std' => 'no',
				'options' => array(
					'no' => esc_html__('No','cinnamon-restaurant'),
					'yes' => esc_html__('Yes','cinnamon-restaurant')
				)
			),
			array(
				'name' => esc_html__('Hide this item','cinnamon-restaurant'),
				'id' => 'pagemeta_hide_food',
				'type' => 'select',
				'desc' => esc_html__('Hide this item.','cinnamon-restaurant'),
				'std' => 'no',
				'options' => array(
					'no' => esc_html__('No','cinnamon-restaurant'),
					'yes' => esc_html__('Yes','cinnamon-restaurant')
				)
			)
		)
);

$max_labels   = 10;
$found_labels = false;
for ($foodlabel=1; $foodlabel <= $max_labels; $foodlabel++ ) {
	$foodlabel_name = cinnamon_restaurant_get_option_data('foodlabel-'.$foodlabel);
	if ( '' !== $foodlabel_name) {
		$found_labels = true;
	}
}

if ( $found_labels ) {
	$mtheme_food_box['fields'][] =
		array(
			'name' => esc_html__('Custom Labels','cinnamon-restaurant'),
			'id' => 'pagemeta_food_custom_labels',
			'type' => 'break',
			'sectiontitle' => esc_html__('Custom Food Labels','cinnamon-restaurant'),
			'std' => ''
			);
	$mtheme_food_box['fields'][] =
		array(
			'name' => esc_html__('Custom Food Labels','cinnamon-restaurant'),
			'id' => 'pagemeta_sep_page_custom_labels',
			'type' => 'seperator',
			);
}
for ( $foodlabel=1; $foodlabel <= $max_labels; $foodlabel++ ) {

	$foodlabel_name = cinnamon_restaurant_get_option_data( 'foodlabel-'.$foodlabel );

	if ( '' !== $foodlabel_name) {

		$mtheme_food_box['fields'][] = 
			array(
				'name'    => $foodlabel_name,
				'id'      => 'foodlabel-'.$foodlabel,
				'type'    => 'select',
				'desc'    => esc_html__('Custom label','cinnamon-restaurant'),
				'std'     => 'no',
				'options' => array(
					'no'  => esc_html__('No','cinnamon-restaurant'),
					'yes' => esc_html__('Yes','cinnamon-restaurant')
				)
			);
	}
}

return $mtheme_food_box;
}
add_action("admin_init", "cinnamon_restaurant_fooditemmetabox_init");
function cinnamon_restaurant_fooditemmetabox_init(){
	add_meta_box("mtheme_foodInfo-meta", esc_html__("Food Options","cinnamon-restaurant"), "cinnamon_restaurant_fooditem_metaoptions", "mtheme_food", "normal", "low");
}
/*
* Meta options for Food post type
*/
function cinnamon_restaurant_fooditem_metaoptions(){
	$mtheme_food_box = cinnamon_restaurant_food_metadata();
	cinnamon_restaurant_generate_metaboxes($mtheme_food_box,get_the_id());
}
?>