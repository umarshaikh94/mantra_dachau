<?php
function cinnamon_restaurant_events_metadata() {
	$mtheme_imagepath =  get_template_directory_uri() . '/framework/options/images/metaboxes/';
	$mtheme_imagepath_alt =  get_template_directory_uri() . '/framework/options/images/';

	$mtheme_sidebar_options = cinnamon_restaurant_generate_sidebarlist("events");

	// Pull all the Featured into an array
	$bg_slideshow_pages = get_posts('post_type=mtheme_featured&orderby=title&numberposts=-1&order=ASC');

	if ($bg_slideshow_pages) {
		$options_bgslideshow['none'] = "Not Selected";
		foreach($bg_slideshow_pages as $key => $list) {
			$custom = get_post_custom($list->ID);
			if ( isset($custom["fullscreen_type"][0]) ) { 
				$slideshow_type=$custom["fullscreen_type"][0]; 
			} else {
			$slideshow_type="";
			}
			if ($slideshow_type<>"Fullscreen-Video") {
				$options_bgslideshow[$list->ID] = $list->post_title;
			}
		}
	} else {
		$options_bgslideshow[0]="Featured pages not found.";
	}

	$mtheme_events_box = array(
		'id' => 'eventsmeta-box',
		'title' => esc_html__('Events Metabox','cinnamon-restaurant'),
		'page' => 'page',
		'context' => 'normal',
		'priority' => 'core',
		'fields' => array(
			array(
				'name' => esc_html__('Event Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_events_section_id',
				'type' => 'break',
				'sectiontitle' => esc_html__('Events Settings','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => esc_html__('Event Options','cinnamon-restaurant'),
				'id' => 'pagemeta_sep_page_options',
				'type' => 'seperator',
				),
			array(
				'name' => esc_html__('Attach Images','cinnamon-restaurant'),
				'id' => 'pagemeta_image_attachments',
				'std' => esc_html__('Upload Images','cinnamon-restaurant'),
				'type' => 'image_gallery',
				'desc' => '<div class="metabox-note">' . esc_html__('Attach images. Images can be used to generate when using shortcodes.','cinnamon-restaurant') . '</div>'
			),
			array(
				'name' => esc_html__('Gallery description text','cinnamon-restaurant'),
				'id' => 'pagemeta_thumbnail_desc',
				'heading' => 'subhead',
				'type' => 'textarea',
				'desc' => esc_html__('Description text to displayed with evernts gallery thumbnail','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => esc_html__('Event Status','cinnamon-restaurant'),
				'id' => 'pagemeta_event_notice',
				'class' => 'event_notice',
				'type' => 'select',
				'desc' => esc_html__('Event Status','cinnamon-restaurant'),
				'options' => array(
					'active' => esc_html__('Active','cinnamon-restaurant'),
					'inactive' => esc_html__('Hide from Listings','cinnamon-restaurant'),
					'postponed' => esc_html__('Display as Postponed','cinnamon-restaurant'),
					'cancelled' => esc_html__('Display as Cancelled','cinnamon-restaurant')
					),
			),
			array(
				'name' => esc_html__('Event Date','cinnamon-restaurant'),
				'id' => 'pagemeta_event_startdate',
				'type' => 'datepicker',
				'class' => 'textsmall',
				'heading' => 'subhead',
				'desc' => esc_html__('Start date','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => 'End Date',
				'id' => 'pagemeta_event_enddate',
				'type' => 'datepicker',
				'class' => 'textsmall',
				'heading' => 'subhead',
				'desc' => esc_html__('End date','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => esc_html__('Venue','cinnamon-restaurant'),
				'id' => 'pagemeta_event_venue_name',
				'type' => 'text',
				'heading' => 'subhead',
				'desc' => esc_html__('Venue Name','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => '',
				'id' => 'pagemeta_event_venue_street',
				'type' => 'text',
				'heading' => 'subhead',
				'desc' => esc_html__('Street','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => '',
				'id' => 'pagemeta_event_venue_state',
				'type' => 'text',
				'class' => 'textsmall',
				'heading' => 'subhead',
				'desc' => esc_html__('State','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => '',
				'id' => 'pagemeta_event_venue_postal',
				'type' => 'text',
				'class' => 'textsmall',
				'heading' => 'subhead',
				'desc' => esc_html__('Zip/Postal Code','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => '',
				'id' => 'pagemeta_event_venue_country',
				'type' => 'country',
				'heading' => 'subhead',
				'desc' => esc_html__('Event country','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => '',
				'id' => 'pagemeta_event_venue_phone',
				'type' => 'text',
				'class' => 'textsmall',
				'heading' => 'subhead',
				'desc' => esc_html__('Phone','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => '',
				'id' => 'pagemeta_event_venue_website',
				'type' => 'text',
				'class' => 'textsmall',
				'heading' => 'subhead',
				'desc' => esc_html__('Website','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => '',
				'id' => 'pagemeta_event_venue_currency',
				'type' => 'text',
				'class' => 'textsmall',
				'heading' => 'subhead',
				'desc' => esc_html__('Cost Currency Symbol','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => '',
				'id' => 'pagemeta_event_venue_cost',
				'type' => 'text',
				'class' => 'textsmall',
				'heading' => 'subhead',
				'desc' => esc_html__('Cost Value','cinnamon-restaurant'),
				'std' => ''
			),
			array(
				'name' => esc_html__('Page Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_page_section_id',
				'type' => 'break',
				'sectiontitle' => esc_html__('Page Settings','cinnamon-restaurant'),
				'std' => ''
				),
			array(
				'name' => esc_html__('Page Layout','cinnamon-restaurant'),
				'id' => 'pagemeta_page_title_seperator',
				'type' => 'seperator',
				),
			array(
				'name' => esc_html__('Page Style','cinnamon-restaurant'),
				'id' => 'pagemeta_pagestyle',
				'type' => 'image',
				'std' => 'rightsidebar',
				'desc' => esc_html__('Note: Edge to Edge - same as 100% Width Template','cinnamon-restaurant'),
				'options' => array(
					'rightsidebar' => $mtheme_imagepath . 'page-right-sidebar.png',
					'leftsidebar' => $mtheme_imagepath . 'page-left-sidebar.png',
					'nosidebar' => $mtheme_imagepath . 'page-no-sidebar.png',
					'edge-to-edge' => $mtheme_imagepath . 'page-edge-to-edge.png')
				),
			array(
				'name' => esc_html__('Page Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_background_section_id',
				'type' => 'break',
				'sectiontitle' => esc_html__('Page Background','cinnamon-restaurant'),
				'std' => ''
				),
			array(
				'name' => esc_html__('Page Settings','cinnamon-restaurant'),
				'id' => 'pagemeta_sep-page_backgrounds',
				'type' => 'seperator',
				),
			array(
				'name' => esc_html__('Header Type','cinnamon-restaurant'),
				'id' => 'pagemeta_header_type',
				'type' => 'select',
				'std' => 'enable',
				'desc' => esc_html__('Header Type for Horizontal menu','cinnamon-restaurant'),
				'options' => array(
					'default' => esc_html__('Default','cinnamon-restaurant'),
					'transparent' => esc_html__('Transparent','cinnamon-restaurant'),
					'transparent-invert' => esc_html__('Transparent Inverse','cinnamon-restaurant')
					)
				),
			array(
				'name' => esc_html__('Choice of Sidebar','cinnamon-restaurant'),
				'id' => 'pagemeta_sidebar_choice',
				'type' => 'select',
				'desc' => esc_html__('For Sidebar Active Pages and Posts','cinnamon-restaurant'),
				'options' => $mtheme_sidebar_options
				),
			array(
				'name' => esc_html__('Switch Menu','cinnamon-restaurant'),
				'id' => 'pagemeta_menu_choice',
				'type' => 'select',
				'desc' => esc_html__('Select a different menu for this page','cinnamon-restaurant'),
				'options' => cinnamon_restaurant_generate_menulist()
				),
			array(
				'name' => esc_html__('Page Title','cinnamon-restaurant'),
				'id' => 'pagemeta_page_title',
				'type' => 'select',
				'desc' => esc_html__('Page Title','cinnamon-restaurant'),
				'std' => 'default',
				'options' => array(
					'default' => esc_html__('Default','cinnamon-restaurant'),
					'show' => esc_html__('Show','cinnamon-restaurant'),
					'hide' => esc_html__('Hide','cinnamon-restaurant')
					)
				)
		)
	);
	return $mtheme_events_box;
}
add_action("admin_init", "cinnamon_restaurant_eventsitemmetabox_init");
function cinnamon_restaurant_eventsitemmetabox_init(){
	add_meta_box("mtheme_eventsInfo-meta", esc_html__("Events Options","cinnamon-restaurant"), "cinnamon_restaurant_eventsitem_metaoptions", "mtheme_events", "normal", "low");
}
/*
* Meta options for Events post type
*/
function cinnamon_restaurant_eventsitem_metaoptions(){
	$mtheme_events_box = cinnamon_restaurant_events_metadata();
	cinnamon_restaurant_generate_metaboxes($mtheme_events_box,get_the_id());
}
?>