<?php
function cinnamon_restaurant_generate_background_css ($the_image) {
	$background_tag = false;
	if ($the_image) {
		$background_tag = 'body { background-image: url('.esc_url($the_image).'); }';
	}
	if ( is_404() ) {
		$background_tag .= '.error404 .container-wrapper{ background: none; }';
	}
	return $background_tag;
}
$dynamic_css='';
if ( is_singular() ) {
	$bg_choice= get_post_meta( get_the_id(), 'cinnamon_restaurant_meta_background_choice', true);
	$image_link=cinnamon_restaurant_featured_image_link( get_the_id() );

	$page_data = get_post_custom( get_the_id() );
	if (isSet($page_data['pagemeta_pagestyle'][0])) {
		$pagestyle = $page_data['pagemeta_pagestyle'][0];
		if ($pagestyle=="split-page") {
			$split_page_image_url = cinnamon_restaurant_featured_image_link( get_the_id() );
			if ( isSet($split_page_image_url) ) {
				$dynamic_css .= '.split-page-image { background-image: url('.esc_url($split_page_image_url).'); }';
			}
		}
	}
}
if ( is_404() ) {
	$bg_choice= "featured_image";
	$image_link= cinnamon_restaurant_get_option_data('general_404_image');
}
$site_in_maintenance = cinnamon_restaurant_maintenance_check();
if ( $site_in_maintenance ) {
	$maintenance_image_link= cinnamon_restaurant_get_option_data('maintenance_bg');
	$dynamic_css .= cinnamon_restaurant_generate_background_css ($maintenance_image_link);
}
$maintenance_color=cinnamon_restaurant_get_option_data('maintenance_color');
if ($maintenance_color) {
	$dynamic_css .= "
	.site-maintenance-text {
	color:".$maintenance_color.";
	}
	";
}
if (!isSet($bg_choice) ) {
	$bg_choice="options_image";
}

if (isSet($fullscreen_slideshowpost)) {
	if ($fullscreen_slideshowpost != "none" && $fullscreen_slideshowpost<>"") {
		$bg_choice="Fullscreen Post Slideshow";
	}
}
if ( is_archive() || is_search() ) {
	if (!isSet( $_GET['photostock'] ) ) {
		$image_link= cinnamon_restaurant_get_option_data('general_background_image');
		$dynamic_css .= cinnamon_restaurant_generate_background_css ($image_link);
	}
}

if ( cinnamon_restaurant_page_is_woo_shop() ) {
	$woo_shop_post_id = get_option( 'woocommerce_shop_page_id' );
	$bg_choice= get_post_meta($woo_shop_post_id, 'pagemeta_meta_background_choice', true);
	$image_link=cinnamon_restaurant_featured_image_link($woo_shop_post_id);
}
if ( is_singular('mtheme_proofing') ) {
	$client_id = get_post_meta( get_the_id() , 'pagemeta_client_names', true);
	if ( isSet($client_id) ) {
		if ( post_password_required($client_id) ) {
			$bg_choice="featured_image";
			$image_link=cinnamon_restaurant_featured_image_link(get_the_id());
		}
	}
}
if ( is_singular('mtheme_clients') ) {
	if ( post_password_required() ) {
		$bg_choice="featured_image";
		$client_background_image_id = get_post_meta( get_the_id() , 'pagemeta_client_background_image', true);
		if (isSet($client_background_image_id)) {
			$image_link=$client_background_image_id;
			$dynamic_css .= cinnamon_restaurant_generate_background_css ($image_link);
		} else {
			$image_link = '';
		}
	}
}
if ( !cinnamon_restaurant_is_fullscreen_home()) {
	if ($bg_choice != "none") {
		switch ($bg_choice) {
			case "featured_image" :
				$dynamic_css .= cinnamon_restaurant_generate_background_css ($image_link);
			break;
		}
	}
}
if ( is_singular('mtheme_featured') ) {
	if ( post_password_required() ) {
		$bg_choice="featured_image";
		$image_link=cinnamon_restaurant_featured_image_link(get_the_id());
	}
}

$accent_border = '
ul#thumb-list li.current-thumb,
ul#thumb-list li.current-thumb:hover,
.home-step:hover .step-element img,
.home-step-wrap li,
.gridblock-element:hover,
.gridblock-grid-element:hover,
.gridblock-displayed:hover,
.entry-content blockquote,
.person:hover .person-image img,
.like-vote-icon,
#gridblock-timeline .blog-grid-element-left:before,
#gridblock-timeline .blog-grid-element-right:before,
#header-searchform #hs,
.pagination span.current,
.pagination span.current:after,
.sidebar h3:after,
.woocommerce .quantity input.qty:hover,
.woocommerce #content .quantity input.qty:hover,
.woocommerce-page .quantity input.qty:hover,
.woocommerce-page #content .quantity input:hover,
.woocommerce .quantity input.qty:focus,
.woocommerce #content .quantity input.qty:focus,
.woocommerce-page .quantity input.qty:focus,
.woocommerce-page #content .quantity input:focus,
.entry-content-wrapper .sticky .postformat_contents,
.entry-content-wrapper.post-is-sticky .type-post,
.woocommerce nav.woocommerce-pagination ul li span.current,
.woocommerce nav.woocommerce-pagination ul li span.current:after,
.portfolio-nav-item a:hover,
.portfolio-nav-item a:hover:after,
.food-item-header,
.flatpickr-calendar .flatpickr-day.selected,
.flatpickr-calendar .flatpickr-day.selected:focus,
.flatpickr-calendar .flatpickr-day.selected:hover,
.flatpickr-calendar .flatpickr-day.today,
.flatpickr-calendar .flatpickr-day.today:focus,
.flatpickr-calendar .flatpickr-day.today:hover';

$accent_border_top = '
.homemenu ul ul';

$accent_border_left = '
.food-notice-active .food-item-wrap-outer';

$accent_border_bottom = '
.entry-content .ui-tabs .ui-tabs-nav .ui-state-active a,
.entry-content .ui-tabs .ui-tabs-nav .ui-state-active a:hover,
.entry-content .food-tabs-outer .ui-tabs .ui-tabs-nav .ui-state-active a,
.entry-content .food-tabs-outer .ui-tabs .ui-tabs-nav .ui-state-active a:hover,
.food-item-header';

$accent_background = '
.wpcf7-form .chosen-container .chosen-results li.highlighted,
.mtheme-opentable-form .chosen-container .chosen-results li.highlighted,
.entry-content .food-tabs-outer .ui-tabs .ui-tabs-nav .ui-state-active a,
.entry-content .food-tabs-outer .ui-tabs .ui-tabs-nav .ui-state-active a:hover,
.pagination span.current,
.pagination ul li span.current,
.blog-timeline-month,
.food-chef-recommended,
.food-new-item,
.food-notice,
.flatpickr-calendar .flatpickr-day.selected,
.flatpickr-calendar .flatpickr-day.selected:focus,
.flatpickr-calendar .flatpickr-day.selected:hover,
.flatpickr-calendar .flatpickr-day.today:focus,
.flatpickr-calendar .flatpickr-day.today:hover,
.pace .pace-progress';

$accent_color = '
.photocard-wrap-type-one .photocard-content-wrap.photocard-dark h3.section-subtitle,
.photocard-wrap-type-one .photocard-content-wrap.photocard-default h3.section-subtitle,
.photocard-wrap-type-one .photocard-content-wrap.photocard-dark .heading-block h3,
.photocard-wrap-type-one .photocard-content-wrap.photocard-default .heading-block h3,
.photocard-wrap-type-two .photocard-content-wrap.photocard-dark h3.section-subtitle,
.photocard-wrap-type-two .photocard-content-wrap.photocard-default h3.section-subtitle,
.photocard-wrap-type-two .photocard-content-wrap.photocard-dark .heading-block h3,
.photocard-wrap-type-two .photocard-content-wrap.photocard-default .heading-block h3,
.homemenu .sub-menu li.current-menu-item > a,
.entry-content .ui-accordion-header.ui-state-active a,
.entry-content h2.section-sub-title,
.entry-content h3.photocard-subtitle,
.flatpickr-calendar .flatpickr-next-month:hover,
.flatpickr-calendar .flatpickr-prev-month:hover,
.entry-content .ui-tabs .ui-tabs-nav .ui-state-active a,
.entry-content .ui-tabs .ui-tabs-nav .ui-state-active a:hover';

$accent_fill = '
.flatpickr-calendar .flatpickr-next-month svg:hover,
.flatpickr-calendar .flatpickr-prev-month svg:hover';

$theme_accent_color = cinnamon_restaurant_get_option_data('theme_accent_color');
if ($theme_accent_color<>"") {
	$dynamic_css .= cinnamon_restaurant_change_class($accent_border,"border",$theme_accent_color,'');
	$dynamic_css .= cinnamon_restaurant_change_class($accent_border_top,"border-top-color",$theme_accent_color,'');
	$dynamic_css .= cinnamon_restaurant_change_class($accent_border_left,"border-left-color",$theme_accent_color,'');
	$dynamic_css .= cinnamon_restaurant_change_class($accent_border_bottom,"border-bottom-color",$theme_accent_color,'');
	$dynamic_css .= cinnamon_restaurant_change_class($accent_background,"background",$theme_accent_color,'');
	$dynamic_css .= cinnamon_restaurant_change_class($accent_color,"color",$theme_accent_color,'');
	$dynamic_css .= cinnamon_restaurant_change_class($accent_fill,"fill",$theme_accent_color,'');
}

$heading_classes='
button,
.woocommerce .product h1,
.woocommerce .product h2,
.woocommerce .product h3,
.woocommerce .product h4,
.woocommerce .product h5,
.woocommerce .product h6,
.entry-title-wrap h1,
.entry-content h1,
.entry-content h2,
.entry-content h3,
.entry-content h4,
.entry-content h5,
.entry-content h6,
.blog-grid-element-content .work-details h4,
.blog-grid-element-content .work-details h4 a,
.gridblock-grid-element .boxtitle-hover a,
.homemenu .sf-menu .mega-item .children-depth-0 h6,
.work-details h4 a,
.work-details h4,
.proofing-client-title,
.comment-reply-title,
.woocommerce ul.products li.product h3,
.woocommerce-page ul.products li.product h3,
h1,
h2,
h3,
h4,
h5,
h6,
.sidebar h3,
.entry-title h1,
.portfolio-end-block h2.section-title';

$page_contents_classes='
body,
input,textarea,
.pricing-wrap,
.pricing-table .pricing-row,
.entry-content,
.mtheme-button,
.sidebar-widget,
ul.vertical_images .vertical-images-title,
.section-description, .entry-title-subheading,
#password-protected label,
.wpcf7-form p,
.homemenu .sf-menu .megamenu-textbox,
.mtheme-lightbox .lg-sub-html
';

$hero_title_class = '.hero-text-wrap .hero-title,.hero-text-wrap .hero-subtitle';

$section_title_class = '.hero-text-wrap .hero-title,h1.entry-title,.entry-content h1.section-title,.entry-content .heading-block h2.photocard-title,h2.food-main-title,.mtheme-opentable-form h2';
$section_subtitle_class = '.hero-text-wrap .hero-subtitle,.entry-content h2.section-sub-title,.entry-content h3.photocard-subtitle';

$slideshow_title_classes = '.slideshow_title, .static_slideshow_title';
$slideshow_caption_classes = '.slideshow_caption, .static_slideshow_caption';
//Font
if (cinnamon_restaurant_get_option_data('default_googlewebfonts')) {
	$dynamic_css .= cinnamon_restaurant_apply_font ( "page_contents" , $page_contents_classes );
	$dynamic_css .= cinnamon_restaurant_apply_font ( "heading_font" , $heading_classes );
	$dynamic_css .= cinnamon_restaurant_apply_font ( "menu_font" , ".homemenu .sf-menu a, .homemenu .sf-menu,.homemenu .sf-menu ul li a,.responsive-mobile-menu ul.mtree > li > a,.responsive-mobile-menu,.vertical-menu ul.mtree,.vertical-menu ul.mtree a,.vertical-menu ul.mtree > li > a" );
	$dynamic_css .= cinnamon_restaurant_apply_font ( "super_title" , $slideshow_title_classes );
	$dynamic_css .= cinnamon_restaurant_apply_font ( "super_caption" , $slideshow_caption_classes );
	$dynamic_css .= cinnamon_restaurant_apply_font ( "section_font_title" , $section_title_class );
	$dynamic_css .= cinnamon_restaurant_apply_font ( "section_font_subtitle" , $section_subtitle_class );
}

//Logo
$logo_height=cinnamon_restaurant_get_option_data('logo_height');
if ($logo_height) {
	$dynamic_css .= '.logo img { height: '.$logo_height.'px; }';
}
$sticky_logo_height=cinnamon_restaurant_get_option_data('sticky_logo_height');
if ($sticky_logo_height) {
	$dynamic_css .= '.stickymenu-zone.sticky-menu-activate .logo img { width:auto; height: '.$sticky_logo_height.'px; }';
}
$logo_topmargin=cinnamon_restaurant_get_option_data('logo_topmargin');
if ($logo_topmargin) {
	$dynamic_css .= '.logo img,.center-menu .logo img { top: '.$logo_topmargin.'px; }';
}
$sticky_logo_topmargin=cinnamon_restaurant_get_option_data('sticky_logo_topmargin');
if ($sticky_logo_topmargin) {
	$dynamic_css .= '.split-menu .stickymenu-zone.sticky-menu-activate .logo img, .left-align-logo .stickymenu-zone.sticky-menu-activate .logo img .compact-menu .logo img { top: '.$sticky_logo_topmargin.'px; }';
}
$logo_leftmargin=cinnamon_restaurant_get_option_data('logo_leftmargin');
if ($logo_leftmargin) {
	$dynamic_css .= '.logo img { margin-left: '.$logo_leftmargin.'px; }';
}
//StockPhoto background archive
$stockphoto_header_archiveimage=cinnamon_restaurant_get_option_data('stockphoto_header_archiveimage');
if ($stockphoto_header_archiveimage<>"") {
	$dynamic_css .= '.archive .stockheader-wrap, .tax-phototag .stockheader-wrap, .searching-for-photostock-term .stockheader-wrap { background-image: url('.esc_url($stockphoto_header_archiveimage).'); }';
}
$general_background_color= cinnamon_restaurant_get_option_data('general_background_color');
if ( $general_background_color<>"" ) {
	$dynamic_css .= 'body { background-color: '.$general_background_color.'; }';
}
//Vertical Menu Logo
$verticalmenu_bgimage=cinnamon_restaurant_get_option_data('verticalmenu_bgimage');
if ($verticalmenu_bgimage) {
	$dynamic_css .= 'body .vertical-menu,body.theme-is-light .vertical-menu { background-image: url('.esc_url($verticalmenu_bgimage).'); }';
}
$vlogo_width=cinnamon_restaurant_get_option_data('vlogo_width');
if ($vlogo_width) {
	$dynamic_css .= '.vertical-logoimage { width: '.$vlogo_width.'px; }';
}
$vlogo_topmargin=cinnamon_restaurant_get_option_data('vlogo_topmargin');
if ($vlogo_topmargin) {
	$dynamic_css .= '.vertical-logoimage { margin-top: '.$vlogo_topmargin.'px; }';
}
$vlogo_leftmargin=cinnamon_restaurant_get_option_data('vlogo_leftmargin');
if ($vlogo_leftmargin) {
	$dynamic_css .= '.vertical-logoimage { margin-left: '.$vlogo_leftmargin.'px; }';
}
$responsive_logo_width = cinnamon_restaurant_get_option_data('responsive_logo_width');
if ($responsive_logo_width) {
	$dynamic_css .= '.logo-mobile .logoimage { width: '.$responsive_logo_width.'px; }';
	$dynamic_css .= '.logo-mobile .logoimage { height: auto; }';
}
$responsive_logo_topmargin = cinnamon_restaurant_get_option_data('responsive_logo_topmargin');
if ($responsive_logo_topmargin) {
	$dynamic_css .= '.logo-mobile .logoimage { top: '.$responsive_logo_topmargin.'px; }';
}

//Preloader
$preloader_color=cinnamon_restaurant_get_option_data('preloader_color');
if ($preloader_color) {
$preloader_color_rgb=cinnamon_restaurant_hex2RGB($preloader_color,true);
$preloader_bar_style_set = '
.pace .pace-progress{
    background: '.$preloader_color.';
}
';
	$dynamic_css .= $preloader_bar_style_set;
}
$preloader_logo = cinnamon_restaurant_get_option_data('preloader_logo');
if ($preloader_logo <> "") {
	$dynamic_css .= '.pace { background-image: url('.esc_url($preloader_logo).'); }';
}
$preloader_logo_width = cinnamon_restaurant_get_option_data('preloader_logo_width');
if ($preloader_logo_width <> 0) {
	$dynamic_css .= '.pace { background-size:'.$preloader_logo_width.'px auto; }';
}
//Preloader Logo
$preloader_bgcolor=cinnamon_restaurant_get_option_data('preloader_bgcolor');
if ($preloader_bgcolor) {
	$dynamic_css .= "
	.pace {
	background-color:".$preloader_bgcolor.";
	}
	";
}

// Menu colors

$menubar_backgroundcolor=cinnamon_restaurant_get_option_data('menubar_backgroundcolor');
$menubar_backgroundcolor_rgb=cinnamon_restaurant_hex2RGB($menubar_backgroundcolor,true);

$menubar_backgroundopacity ="0.8";
$sticky_menubar_backgroundopacity ="0.8";
$custom_menubar_backgroundopacity=cinnamon_restaurant_get_option_data('menubar_backgroundopacity');
if (isSet($custom_menubar_backgroundopacity) &&  $custom_menubar_backgroundopacity<>"") {
	$menubar_backgroundopacity=$custom_menubar_backgroundopacity/100;
}

$stickymenubar_backgroundopacity=cinnamon_restaurant_get_option_data('stickymenubar_backgroundopacity');
if (isSet($stickymenubar_backgroundopacity) &&  $stickymenubar_backgroundopacity<>"") {
	$sticky_menubar_backgroundopacity=$stickymenubar_backgroundopacity/100;
}

if ($menubar_backgroundcolor<>"") {
	$dynamic_css .= '.outer-wrap, .page-is-fullscreen .outer-wrap,.sticky-menu-on.header-is-transparent-invert .outer-wrap { background:rgba('. $menubar_backgroundcolor_rgb .','.$menubar_backgroundopacity.'); }';
	$dynamic_css .= '.sticky-menu-on .outer-wrap, .sticky-menu-on.page-is-fullscreen .outer-wrap { background:rgba('. $menubar_backgroundcolor_rgb .','.$sticky_menubar_backgroundopacity.'); }';
}
$menu_linkcolor=cinnamon_restaurant_get_option_data('menu_linkcolor');
if ($menu_linkcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.menu-social-header .social-header-wrap ul li.social-icon i, .social-sharing-toggle , .menu-social-header .social-header-wrap ul li.contact-text a',"color",$menu_linkcolor,'');
	$dynamic_css .= cinnamon_restaurant_change_class('.homemenu > ul > li > a, .header-cart i,.sticky-menu-activate .homemenu > ul > li > a,.stickymenu-zone.sticky-menu-activate .homemenu > ul > li > a',"color",$menu_linkcolor,'');
	$dynamic_css .= cinnamon_restaurant_change_class('.homemenu .sf-menu li.menu-item a:before',"border-color",$menu_linkcolor,'');
	$dynamic_css .= cinnamon_restaurant_change_class('.sidebar-toggle-menu-trigger span::before, .sidebar-toggle-menu-trigger span::after, .sidebar-toggle-menu-trigger span',"background",$menu_linkcolor,'');
}
$menu_linkhovercolor=cinnamon_restaurant_get_option_data('menu_linkhovercolor');
if ($menu_linkhovercolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.mobile-social-header .social-header-wrap ul li.social-icon:hover i, .social-sharing-toggle:hover i, .fullscreen-slide-dark .social-sharing-toggle:hover i, .stickymenu-zone .social-sharing-toggle:hover i, .mobile-social-header .social-header-wrap ul li.contact-text:hover a, .menu-social-header .social-header-wrap ul li.social-icon:hover i, .menu-social-header .social-header-wrap ul li.contact-text:hover a',"color",$menu_linkhovercolor,'');
	$dynamic_css .= cinnamon_restaurant_change_class('.homemenu ul li a:hover, .header-cart i:hover,.sticky-menu-activate .homemenu ul li a:hover,.stickymenu-zone.sticky-menu-activate .homemenu ul li a:hover',"color",$menu_linkhovercolor,'');
	$dynamic_css .= cinnamon_restaurant_change_class('.homemenu .sf-menu li.menu-item a:before',"border-color",$menu_linkhovercolor,'');
}

$menu_titlelinkhover_color=cinnamon_restaurant_get_option_data('menu_titlelinkhover_color');
if ($menu_titlelinkhover_color) {
	$dynamic_css .= cinnamon_restaurant_change_class('.mainmenu-navigation .homemenu ul li a:hover',"color",$menu_titlelinkhover_color,'');
}

$menusubcat_bgcolor=cinnamon_restaurant_get_option_data('menusubcat_bgcolor');
if ($menusubcat_bgcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.homemenu .sf-menu .mega-item .children-depth-0, .homemenu ul ul',"background-color",$menusubcat_bgcolor,'');
}

$currentmenu_linkcolor=cinnamon_restaurant_get_option_data('currentmenu_linkcolor');
if ($currentmenu_linkcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.homemenu > ul > li.current-menu-item > a,.homemenu .sub-menu li.current-menu-item > a,.mainmenu-navigation .homemenu > ul > li.current-menu-item > a',"color",$currentmenu_linkcolor,'');
}
$currentmenusubcat_linkcolor=cinnamon_restaurant_get_option_data('currentmenusubcat_linkcolor');
if ($currentmenusubcat_linkcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.mainmenu-navigation .homemenu ul ul li.current-menu-item > a',"color",$currentmenusubcat_linkcolor,'');
}

$menusubcat_linkcolor=cinnamon_restaurant_get_option_data('menusubcat_linkcolor');
if ($menusubcat_linkcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.mainmenu-navigation .homemenu ul ul li a',"color",$menusubcat_linkcolor,'');
}

$menusubcat_linkhovercolor=cinnamon_restaurant_get_option_data('menusubcat_linkhovercolor');
if ($menusubcat_linkhovercolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.mainmenu-navigation .homemenu ul ul li:hover>a',"color",$menusubcat_linkhovercolor,'');
}
$menusubcat_linkunderlinecolor=cinnamon_restaurant_get_option_data('menusubcat_linkunderlinecolor');
if ($menusubcat_linkunderlinecolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.homemenu ul.sub-menu > li.menu-item',"border-color",$menusubcat_linkunderlinecolor,'');
}
$menu_parentactive_color=cinnamon_restaurant_get_option_data('menu_parentactive_color');
if ($menu_parentactive_color) {
	$dynamic_css .= cinnamon_restaurant_change_class('.homemenu li.current-menu-item a:before, .homemenu li.current-menu-ancestor a:before ',"background-color",$menu_parentactive_color,'');
}
$menu_search_color=cinnamon_restaurant_get_option_data('menu_search_color');
if ($menu_search_color) {
	$dynamic_css .= cinnamon_restaurant_change_class('.header-search i.icon-search,.header-search i.icon-remove',"color",$menu_search_color,'');
}
$menu_search_hovercolor=cinnamon_restaurant_get_option_data('menu_search_hovercolor');
if ($menu_search_hovercolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.header-search i.icon-search:hover,.header-search i.icon-remove:hover',"color",$menu_search_hovercolor,'');
}

// Share modal 
$share_bgcolor=cinnamon_restaurant_get_option_data('share_bgcolor');
$share_bgcolor_rgba=cinnamon_restaurant_hex2RGB($share_bgcolor,true);
if ($share_bgcolor) {
	$dynamic_css .= '#social-modal { background:rgba('. $share_bgcolor_rgba .',0.8); }';
}
$share_iconcolor=cinnamon_restaurant_get_option_data('share_iconcolor');
if ($share_iconcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('#social-modal .page-share li i,.social-modal-cross',"color",$share_iconcolor,'');
}

// Vertical Menu
$vmenubar_bgcolor=cinnamon_restaurant_get_option_data('vmenubar_bgcolor');
$vmenubar_bgcolor_rgba=cinnamon_restaurant_hex2RGB($vmenubar_bgcolor,true);
if ($vmenubar_bgcolor) {
	$more_menuClasses = '.theme-is-light .vertical-menu, .theme-is-light .simple-menu, .theme-is-light .responsive-mobile-menu,';
	$more_menuClasses .= '.theme-is-dark .vertical-menu, .theme-is-dark .simple-menu, .theme-is-dark .responsive-mobile-menu';
	$dynamic_css .= $more_menuClasses .' { background:rgba('. $vmenubar_bgcolor_rgba .',1); }';
}
$vmenubar_linkcolor=cinnamon_restaurant_get_option_data('vmenubar_linkcolor');
if ($vmenubar_linkcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.theme-is-light .vertical-menu ul.mtree li li a,.theme-is-dark .vertical-menu ul.mtree li li a,.theme-is-light .vertical-menu ul.mtree a,.theme-is-dark .vertical-menu ul.mtree a,.vertical-menu ul.mtree > li > a, .vertical-menu ul.mtree li.mtree-node > a:before,.theme-is-light .vertical-menu ul.mtree li.mtree-node > a::before,.vertical-menu ul.mtree a',"color",$vmenubar_linkcolor,'');
}
$vmenubar_linkhovercolor=cinnamon_restaurant_get_option_data('vmenubar_linkhovercolor');
if ($vmenubar_linkhovercolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.vertical-menu ul.mtree li > a:hover,.vertical-menu .social-header-wrap ul li.social-icon:hover i',"color",$vmenubar_linkhovercolor,'');
	//$dynamic_css .= cinnamon_restaurant_change_class('.vertical-menu ul.mtree li.mtree-open > a,.vertical-menu ul.mtree li.mtree-open > a:hover, .vertical-menu ul.mtree a:hover',"color",$vmenubar_linkhovercolor,'');
}
$vmenubar_linkactivecolor=cinnamon_restaurant_get_option_data('vmenubar_linkactivecolor');
if ($vmenubar_linkactivecolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.vertical-menu ul.mtree li.mtree-open > a,.vertical-menu ul.mtree li.mtree-open > a:hover, .vertical-menu ul.mtree a:hover',"color",$vmenubar_linkactivecolor,'');
}
$vmenubar_socialcolor=cinnamon_restaurant_get_option_data('vmenubar_socialcolor');
if ($vmenubar_socialcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.vertical-menu .social-header-wrap ul li.social-icon i,.vertical-menu .social-header-wrap ul li.contact-text,.vertical-menu .social-header-wrap ul li.contact-text a',"color",$vmenubar_socialcolor,'');
}
$vmenubar_copyrightcolor=cinnamon_restaurant_get_option_data('vmenubar_copyrightcolor');
if ($vmenubar_copyrightcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.vertical-footer-wrap .fullscreen-footer-info',"color",$vmenubar_copyrightcolor,'');
	$dynamic_css .= '.vertical-footer-wrap .fullscreen-footer-info { border-top-color:'. $vmenubar_copyrightcolor .'; }';
}
$vmenubar_hlinecolor=cinnamon_restaurant_get_option_data('vmenubar_hlinecolor');
if ($vmenubar_hlinecolor) {
	$dynamic_css .= '.vertical-menu ul.mtree a,ul.mtree li.mtree-node > ul > li:last-child { border-bottom-color:'. $vmenubar_hlinecolor .'; }';
}


$vmenu_active_itemcolor=cinnamon_restaurant_get_option_data('vmenu_active_itemcolor');
if ($vmenu_active_itemcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('ul.mtree li.mtree-open > a:hover, ul.mtree li.mtree-open > a',"color",$vmenu_active_itemcolor,'');
}
$vmenu_itemcolor=cinnamon_restaurant_get_option_data('vmenu_itemcolor');
if ($vmenu_itemcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('ul.mtree a',"color",$vmenu_itemcolor,'');
}
$vmenu_hover_itemcolor=cinnamon_restaurant_get_option_data('vmenu_hover_itemcolor');
if ($vmenu_hover_itemcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('ul.mtree li> a:hover,ul.mtree li.mtree-active > a:hover,ul.mtree li.mtree-active > a',"color",$vmenu_hover_itemcolor,'');
}
$vmenu_search_itemcolor=cinnamon_restaurant_get_option_data('vmenu_search_itemcolor');
if ($vmenu_search_itemcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.menu-is-vertical .header-search i',"color",$vmenu_search_itemcolor,'');
}
$vmenubar_socialcolor=cinnamon_restaurant_get_option_data('vmenubar_socialcolor');
if ($vmenubar_socialcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.menu-is-vertical .vertical-footer-wrap .social-icon a, .menu-is-vertical .vertical-footer-wrap .social-icon i, .menu-is-vertical .vertical-footer-wrap .social-header-wrap ul li.social-icon i, .menu-is-vertical .vertical-footer-wrap .social-header-wrap ul li.contact-text a',"color",$vmenubar_socialcolor,'');
}


// Slideshow Color

$slideshow_title=cinnamon_restaurant_get_option_data('slideshow_title');
if ($slideshow_title) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.slideshow_title', "color",$slideshow_title,'' );
}
$slideshow_captiontxt=cinnamon_restaurant_get_option_data('slideshow_captiontxt');
if ($slideshow_captiontxt) {
	$dynamic_css .= cinnamon_restaurant_change_class( '#slidecaption .slideshow_caption', "color",$slideshow_captiontxt,'' );
}
$slideshow_buttontxt=cinnamon_restaurant_get_option_data('slideshow_buttontxt');
if ($slideshow_buttontxt) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.slideshow_content_link a, .static_slideshow_content_link a', "color",$slideshow_buttontxt,'' );
}
$slideshow_buttonborder=cinnamon_restaurant_get_option_data('slideshow_buttonborder');
if ($slideshow_buttonborder) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.slideshow_content_link a, .static_slideshow_content_link a', "border-color",$slideshow_buttonborder,'' );
}
$slideshow_buttonhover_text=cinnamon_restaurant_get_option_data('slideshow_buttonhover_text');
if ($slideshow_buttonhover_text) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.slideshow_content_link a:hover, .static_slideshow_content_link a:hover', "color",$slideshow_buttonhover_text,'' );
}
$slideshow_buttonhover_bg=cinnamon_restaurant_get_option_data('slideshow_buttonhover_bg');
if ($slideshow_buttonhover_bg) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.slideshow_content_link a:hover, .static_slideshow_content_link a:hover', "background-color",$slideshow_buttonhover_bg,'' );
	$dynamic_css .= cinnamon_restaurant_change_class( '.slideshow_content_link a:hover, .static_slideshow_content_link a:hover', "border-color",$slideshow_buttonhover_bg,'' );
}
$slideshow_captionbg=cinnamon_restaurant_get_option_data('slideshow_captionbg');
$slideshow_captionbg_rgb=cinnamon_restaurant_hex2RGB($slideshow_captionbg,true);
if ($slideshow_captionbg) {
	$dynamic_css .= "#slidecaption {
background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(".$slideshow_captionbg_rgb.",0.55) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(100%,rgba(".$slideshow_captionbg_rgb.",0.55)));
background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(".$slideshow_captionbg_rgb.",0.55) 100%);
background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(".$slideshow_captionbg_rgb.",0.55) 100%);
background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(".$slideshow_captionbg_rgb.",0.55) 100%);
background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(".$slideshow_captionbg_rgb.",0.55) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='".$slideshow_captionbg."', endColorstr='".$slideshow_captionbg."',GradientType=0 );
}";
}
$slideshow_transbar=cinnamon_restaurant_get_option_data('slideshow_transbar');
$slideshow_transbar_rgb=cinnamon_restaurant_hex2RGB($slideshow_transbar,true);
if ($slideshow_transbar) {
	$dynamic_css .= "
	#progress-bar {
	background:".$slideshow_transbar.";
	}
	";
}
$slideshow_currthumbnail=cinnamon_restaurant_get_option_data('slideshow_currthumbnail');
if ($slideshow_currthumbnail) {
	$dynamic_css .= cinnamon_restaurant_change_class( 'ul#thumb-list li.current-thumb', "border-color",$slideshow_currthumbnail,'');
}

$menu_text_capitalize=cinnamon_restaurant_get_option_data('menu_text_capitalize');
if ($menu_text_capitalize) {
	$dynamic_css .= '.homemenu .sf-menu a { text-transform: uppercase; }';
}
$hmenu_text_size=cinnamon_restaurant_get_option_data('hmenu_text_size');
if ($hmenu_text_size<>"") {
	$dynamic_css .= '.homemenu ul li a,.homemenu ul ul li a { font-size:'.$hmenu_text_size.'px;}';
}
$hmenu_text_letterspacing=cinnamon_restaurant_get_option_data('hmenu_text_letterspacing');
if ($hmenu_text_letterspacing<>"") {
	$dynamic_css .= '.homemenu ul li a,.homemenu ul ul li a { letter-spacing:'.$hmenu_text_letterspacing.'px;}';
}
$hmenu_text_weight=cinnamon_restaurant_get_option_data('hmenu_text_weight');
if ($hmenu_text_weight<>"") {
	$dynamic_css .= '.homemenu ul li a,.homemenu ul ul li a { font-weight:'.$hmenu_text_weight.';}';
}

$hmenu_item_gap=cinnamon_restaurant_get_option_data('hmenu_item_gap');
if ($hmenu_item_gap<>"") {
	$dynamic_css .= '.homemenu .sf-menu li{ margin-left:'.$hmenu_item_gap.'px;}';
}

$page_opacity_customize=cinnamon_restaurant_get_option_data('page_opacity_customize');
if ($page_opacity_customize) {
	$dynamic_css .= '.container-wrapper, .fullscreen-protected #password-protected { background:'.$page_opacity_customize.'; }';
}
$page_bgcolor=cinnamon_restaurant_get_option_data('page_bgcolor');
if ($page_bgcolor) {
	$dynamic_css .= '.container-wrapper, .fullscreen-protected #password-protected,#fotorama-container-wrap { background:'.$page_bgcolor.'; }';
}
$page_contentscolor=cinnamon_restaurant_get_option_data('page_contentscolor');
if ($page_contentscolor) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.gridblock-four .work-description, .gridblock-three .work-description, .gridblock-two .work-description, .gridblock-one .work-description, .slideshow-box-info .work-description, .entry-content .smaller-content, .entry-content, .woocommerce #tab-description p, .woocommerce .entry-summary div[itemprop="description"], .blog-details-section .the-month, .post-meta-time-archive, #password-protected p, .post-password-form p, #password-protected label, #gridblock-filters .griblock-filters-subcats a, .person h4.staff-position, .gridblock-parallax-wrap .work-description,.woocommerce .entry-summary div[itemprop="description"],.entry-content,.entry-content .pullquote-left,.entry-content .pullquote-right,.entry-content .pullquote-center', "color",$page_contentscolor,'' );
}
$page_contentsheading=cinnamon_restaurant_get_option_data('page_contentsheading');
if ($page_contentsheading) {
$content_headings = '
.woocommerce div.product .product_title,
.woocommerce #content div.product .product_title,
.woocommerce-page div.product .product_title,
.woocommerce-page #content div.product .product_title,
.entry-content h1,
.entry-content h2,
.entry-content h3,
.entry-content h4,
.entry-content h5,
.entry-content h6,
h1.entry-title,
.entry-content h1.section-title,
.work-details h4,
.work-details h4 a,
.client-company a:hover,
.portfolio-share li a:hover,
.min-search .icon-search:hover,
.entry-content .entry-post-title h2 a,
ul.gridblock-listbox .work-details h4 a:hover
';
	$dynamic_css .= cinnamon_restaurant_change_class( $content_headings, "color",$page_contentsheading,'' );
}

$footer_bgcolor=cinnamon_restaurant_get_option_data('footer_bgcolor');
if ($footer_bgcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.footer-section', "background",$footer_bgcolor,'' );
}
$footer_iconcolor=cinnamon_restaurant_get_option_data('footer_iconcolor');
if ($footer_iconcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class( '#footer .social-icon i', "color",$footer_iconcolor,'important' );
}
$footer_text=cinnamon_restaurant_get_option_data('footer_text');
if ($footer_text) {
	$dynamic_css .= cinnamon_restaurant_change_class( '#copyright,.footer-section,.footer-section .sidebar,.footer-section .contact_address_block .about_info', "color",$footer_text,'' );
}
$footer_link=cinnamon_restaurant_get_option_data('footer_link');
if ($footer_link) {
	$dynamic_css .= cinnamon_restaurant_change_class( '#copyright a,.footer-section a,.footer-section .sidebar-widget a', "color",$footer_link,'' );
}
$footer_linkhover=cinnamon_restaurant_get_option_data('footer_linkhover');
if ($footer_linkhover) {
	$dynamic_css .= cinnamon_restaurant_change_class( '#copyright a:hover,.footer-section a:hover,.footer-section .sidebar-widget a:hover', "color",$footer_linkhover,'' );
}
$footer_hline=cinnamon_restaurant_get_option_data('footer_hline');
if ($footer_hline) {
	$dynamic_css .= cinnamon_restaurant_change_class( '#footer.sidebar ul li', "border-top-color",$footer_hline,'' );
}



$fullscreen_toggle_color = cinnamon_restaurant_get_option_data('fullscreen_toggle_color');
if ($fullscreen_toggle_color) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.menu-toggle',"color", $fullscreen_toggle_color,'' );
}
$fullscreen_toggle_bg = cinnamon_restaurant_get_option_data('fullscreen_toggle_bg');
if ($fullscreen_toggle_bg) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.menu-toggle',"background-color", $fullscreen_toggle_bg,'' );
	$dynamic_css .= cinnamon_restaurant_change_class( '.menu-toggle:after',"border-color", $fullscreen_toggle_bg,'' );
}

$fullscreen_toggle_hovercolor = cinnamon_restaurant_get_option_data('fullscreen_toggle_hovercolor');
if ($fullscreen_toggle_hovercolor) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.menu-toggle:hover',"color", $fullscreen_toggle_hovercolor,'' );
}

$fullscreen_toggle_hoverbg = cinnamon_restaurant_get_option_data('fullscreen_toggle_hoverbg');
if ($fullscreen_toggle_hoverbg) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.menu-toggle:hover',"background-color", $fullscreen_toggle_hoverbg,'' );
	$dynamic_css .= cinnamon_restaurant_change_class( '.menu-toggle:hover:after',"border-color", $fullscreen_toggle_hoverbg,'' );
}

$footer_copyrightbg=cinnamon_restaurant_get_option_data('footer_copyrightbg');
$footer_copyrightbg_rgb=cinnamon_restaurant_hex2RGB($footer_copyrightbg,true);
if ($footer_copyrightbg) {
	$dynamic_css .= '#copyright { background:rgba('. $footer_copyrightbg_rgb .',0.8); }';
}
$footer_copyrighttext=cinnamon_restaurant_get_option_data('footer_copyrighttext');
if ($footer_copyrighttext) {
	$dynamic_css .= cinnamon_restaurant_change_class( '#copyright', "color",$footer_copyrighttext,'' );
}


$sidebar_headingcolor=cinnamon_restaurant_get_option_data('sidebar_headingcolor');
if ($sidebar_headingcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.sidebar h3', "color",$sidebar_headingcolor,'' );
}
$sidebar_linkcolor=cinnamon_restaurant_get_option_data('sidebar_linkcolor');
if ($sidebar_linkcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class( '#recentposts_list .recentpost_info .recentpost_title, #popularposts_list .popularpost_info .popularpost_title,.sidebar a', "color",$sidebar_linkcolor,'' );
}
$sidebar_bgcolor=cinnamon_restaurant_get_option_data('sidebar_bgcolor');
if ($sidebar_bgcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.sidebar-wrap, .sidebar-wrap-single', "background",$sidebar_bgcolor,'' );
}
$sidebar_textcolor=cinnamon_restaurant_get_option_data('sidebar_textcolor');
if ($sidebar_textcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class( '.contact_address_block .about_info, #recentposts_list p, #popularposts_list p,.sidebar-widget ul#recentcomments li,.sidebar', "color",$sidebar_textcolor,'' );
}

if ( cinnamon_restaurant_get_option_data('custom_font_css')<>"" ) {
	$dynamic_css .= cinnamon_restaurant_get_option_data('custom_font_css');
}

$photowall_title_color=cinnamon_restaurant_get_option_data('photowall_title_color');
if ($photowall_title_color) {
$dynamic_css .= cinnamon_restaurant_change_class( '.photowall-title', "color",$photowall_title_color,'' );
}

$photowall_description_color=cinnamon_restaurant_get_option_data('photowall_description_color');
if ($photowall_description_color) {
$dynamic_css .= cinnamon_restaurant_change_class( '.photowall-desc', "color",$photowall_description_color,'' );
}

$photowall_hover_titlecolor=cinnamon_restaurant_get_option_data('photowall_hover_titlecolor');
if ($photowall_hover_titlecolor) {
$dynamic_css .= cinnamon_restaurant_change_class( '.photowall-item:hover .photowall-title', "color",$photowall_hover_titlecolor,'' );
}
$photowall_hover_descriptioncolor=cinnamon_restaurant_get_option_data('photowall_hover_descriptioncolor');
if ($photowall_hover_descriptioncolor) {
$dynamic_css .= cinnamon_restaurant_change_class( '.photowall-item:hover .photowall-desc', "color",$photowall_hover_descriptioncolor,'' );
}

$photowall_description_color=cinnamon_restaurant_get_option_data('photowall_description_color');
if ($photowall_description_color) {
$dynamic_css .= cinnamon_restaurant_change_class( '.photowall-desc', "color",$photowall_description_color,'' );
}

// The icons
$forum_icon_css = cinnamon_restaurant_set_fontawesome('fa fa-cubes' , cinnamon_restaurant_get_option_data('forum_icon') , $get_css_code = true );
if ( isSet($forum_icon_css) ) {
	$dynamic_css .= '
	#bbpress-forums ul.forum li.bbp-forum-info:before {
	content:"'.$forum_icon_css.'";
	}
	';
}

$blog_allowedtags=cinnamon_restaurant_get_option_data('blog_allowedtags');
if ($blog_allowedtags) {
$dynamic_css .= '.form-allowed-tags { display:none; }';
}
$dynamic_css .= stripslashes_deep( cinnamon_restaurant_get_option_data('custom_css') );

// Lightbox
$disable_lightbox_fullscreen=cinnamon_restaurant_get_option_data('disable_lightbox_fullscreen');
if ($disable_lightbox_fullscreen) {
	$dynamic_css .= '.mtheme-lightbox .lg-fullscreen { display:none; }';
}
$disable_lightbox_sizetoggle=cinnamon_restaurant_get_option_data('disable_lightbox_sizetoggle');
if ($disable_lightbox_sizetoggle) {
	$dynamic_css .= '.mtheme-lightbox #lg-actual-size { display:none; }';
}
$disable_lightbox_zoomcontrols=cinnamon_restaurant_get_option_data('disable_lightbox_zoomcontrols');
if ($disable_lightbox_zoomcontrols) {
	$dynamic_css .= '.mtheme-lightbox #lg-zoom-out,.lg-toolbar #lg-zoom-in { display:none; }';
}
$disable_lightbox_autoplay=cinnamon_restaurant_get_option_data('disable_lightbox_autoplay');
if ($disable_lightbox_autoplay) {
	$dynamic_css .= '.mtheme-lightbox .lg-autoplay-button { display:none; }';
}
$disable_lightbox_share=cinnamon_restaurant_get_option_data('disable_lightbox_share');
if ($disable_lightbox_share) {
	$dynamic_css .= '.mtheme-lightbox .lg-share-icon { display:none; }';
}
$disable_lightbox_count=cinnamon_restaurant_get_option_data('disable_lightbox_count');
if ($disable_lightbox_count) {
	$dynamic_css .= '.mtheme-lightbox #lg-counter { display:none; }';
}
$disable_lightbox_title=cinnamon_restaurant_get_option_data('disable_lightbox_title');
if ($disable_lightbox_title) {
	$dynamic_css .= '.mtheme-lightbox .lg-sub-html { display:none; }';
}
$lightbox_bgcolor=cinnamon_restaurant_get_option_data('lightbox_bgcolor');
if ($lightbox_bgcolor) {
	$dynamic_css .= '.body .lg-backdrop, .mtheme-lightbox.lg-outer { background:'.$lightbox_bgcolor.'; }';
}
$lightbox_elementbgcolor=cinnamon_restaurant_get_option_data('lightbox_elementbgcolor');
if ($lightbox_elementbgcolor) {
	$dynamic_css .= '.mtheme-lightbox .lg-actions .lg-icon,.mtheme-lightbox .lg-sub-html, .mtheme-lightbox .lg-toolbar { background:'.$lightbox_elementbgcolor.'; }';
}
$lightbox_elementcolor=cinnamon_restaurant_get_option_data('lightbox_elementcolor');
if ($lightbox_elementcolor) {
	$dynamic_css .= '.mtheme-lightbox #lg-counter,.mtheme-lightbox #lg-counter, .mtheme-lightbox .lg-sub-html, .mtheme-lightbox .lg-toolbar .lg-icon, .mtheme-lightbox .lg-actions .lg-next, .mtheme-lightbox .lg-actions .lg-prev { color:'.$lightbox_elementcolor.'; }';
}

// Font size
$copyright_fontsize=cinnamon_restaurant_get_option_data('copyright_fontsize');
if ($copyright_fontsize<>"") {
$dynamic_css .= '
#copyright {
	font-size:'.$copyright_fontsize.'px;
}';
}
$copyright_lineheight=cinnamon_restaurant_get_option_data('copyright_lineheight');
if ($copyright_lineheight<>"") {
$dynamic_css .= '
#copyright {
	line-height:'.$copyright_lineheight.'px;
}';
}
$pagecontent_fontsize=cinnamon_restaurant_get_option_data('pagecontent_fontsize');
if ($pagecontent_fontsize<>"") {
$dynamic_css .= '
.entry-content,
.woocommerce #tab-description p,
.woocommerce .entry-summary div[itemprop="description"]{
	font-size:'.$pagecontent_fontsize.'px;
}';
}
// Line Height
$pagecontent_lineheight=cinnamon_restaurant_get_option_data('pagecontent_lineheight');
if ($pagecontent_lineheight<>"") {
$dynamic_css .= '
.entry-content,
.woocommerce #tab-description p,
.woocommerce .entry-summary div[itemprop="description"] {
	line-height:'.$pagecontent_lineheight.'px;
}';
}
// Letter Spacing
$pagecontent_letterspacing=cinnamon_restaurant_get_option_data('pagecontent_letterspacing');
if ($pagecontent_letterspacing<>"") {
$dynamic_css .= '
.entry-content,
.woocommerce #tab-description p,
.woocommerce .entry-summary div[itemprop="description"] {
	letter-spacing:'.$pagecontent_letterspacing.'px;
}';
}
// Font Weight
$pagecontent_fontweight=cinnamon_restaurant_get_option_data('pagecontent_fontweight');
if ($pagecontent_fontweight<>"") {
$dynamic_css .= '
.entry-content,
.woocommerce #tab-description p,
.woocommerce .entry-summary div[itemprop="description"] {
	font-weight:'.$pagecontent_fontweight.';
}';
}
$pagetitle_size=cinnamon_restaurant_get_option_data('pagetitle_size');
if ($pagetitle_size<>"") {
	$dynamic_css .= '.entry-title-wrap h1 { font-size:'.$pagetitle_size.'px; line-height:'.$pagetitle_size.'px; }';
}
$pagetitle_letterspacing=cinnamon_restaurant_get_option_data('pagetitle_letterspacing');
if ($pagetitle_letterspacing<>"") {
	$dynamic_css .= '.entry-title-wrap h1 { letter-spacing:'.$pagetitle_letterspacing.'px; }';
}
$pagetitle_weight=cinnamon_restaurant_get_option_data('pagetitle_weight');
if ($pagetitle_weight<>"") {
	$dynamic_css .= '.entry-title-wrap h1 { font-weight:'.$pagetitle_weight.'; }';
}
//Mobile Menu color
$mobilemenubar_bgcolor=cinnamon_restaurant_get_option_data('mobilemenubar_bgcolor');
if ($mobilemenubar_bgcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.theme-is-dark .mobile-menu-toggle,.mobile-menu-toggle,.header-is-simple.theme-is-dark .mobile-menu-icon,.header-is-simple.theme-is-light .mobile-menu-icon',"background-color",$mobilemenubar_bgcolor,'');
}
$mobilemenubar_togglecolor=cinnamon_restaurant_get_option_data('mobilemenubar_togglecolor');
if ($mobilemenubar_togglecolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.mobile-toggle-menu-trigger span::before, .mobile-toggle-menu-trigger span::after, .mobile-toggle-menu-open .mobile-toggle-menu-trigger span::before, .mobile-toggle-menu-open .mobile-toggle-menu-trigger span::after, .mobile-toggle-menu-trigger span',"background",$mobilemenubar_togglecolor,'');
	$dynamic_css .= cinnamon_restaurant_change_class('.mobile-sharing-toggle',"color",$mobilemenubar_togglecolor,'');
}
$mobilemenu_bgcolor=cinnamon_restaurant_get_option_data('mobilemenu_bgcolor');
if ($mobilemenu_bgcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('.responsive-mobile-menu,.theme-is-light .responsive-mobile-menu',"background-color",$mobilemenu_bgcolor,'');
	$dynamic_css .= '.theme-is-dark .responsive-mobile-menu #mobile-searchform input { border-color: rgba(255,255,255,0.1); }';
	$dynamic_css .= '.theme-is-light .responsive-mobile-menu #mobile-searchform input { border-color: rgba(0,0,0,0.1); }';
}
$mobilemenu_bgimage=cinnamon_restaurant_get_option_data('mobilemenu_bgimage');
if ($mobilemenu_bgimage) {
	$dynamic_css .= 'body .responsive-mobile-menu,body.theme-is-light .responsive-mobile-menu { background-image: url('.esc_url($mobilemenu_bgimage).'); }';
}
$mobilemenu_texticons=cinnamon_restaurant_get_option_data('mobilemenu_texticons');
$mobilemenu_texticons_rgb=cinnamon_restaurant_hex2RGB($mobilemenu_texticons,true);
if ($mobilemenu_bgcolor) {
	$dynamic_css .= cinnamon_restaurant_change_class('
	.theme-is-light .responsive-mobile-menu #mobile-searchform input,
	.theme-is-dark .responsive-mobile-menu #mobile-searchform input,
	.theme-is-light .responsive-mobile-menu ul.mtree li li a,
	.theme-is-light .responsive-mobile-menu ul.mtree li a,
	.header-is-simple.theme-is-light .responsive-mobile-menu ul.mtree li li a,
	.theme-is-dark .responsive-mobile-menu ul.mtree li li a,
	.theme-is-dark .responsive-mobile-menu ul.mtree li a,
	.header-is-simple.theme-is-dark .responsive-mobile-menu ul.mtree li li a,
	.header-is-simple.theme-is-dark .responsive-mobile-menu ul.mtree li a,
	.theme-is-light .responsive-mobile-menu ul.mtree a,
	.header-is-simple.theme-is-light .responsive-mobile-menu ul.mtree a,
	.header-is-simple.theme-is-dark .responsive-mobile-menu ul.mtree a,
	.theme-is-dark .mobile-social-header .social-header-wrap ul li.social-icon i,
	.theme-is-dark .mobile-social-header .social-header-wrap ul li.contact-text a,
	.theme-is-light .mobile-social-header .social-header-wrap ul li.social-icon i,
	.theme-is-light .mobile-social-header .social-header-wrap ul li.contact-text a,
	.mobile-social-header .social-header-wrap ul li.social-icon i,
	.mobile-social-header .social-header-wrap ul li.contact-text a,
	.mobile-social-header .social-header-wrap ul li.contact-text i,
	.theme-is-light .responsive-mobile-menu #mobile-searchform i,
	.theme-is-dark .responsive-mobile-menu #mobile-searchform i',"color",$mobilemenu_texticons,'');
}
$mobilemenu_linecolors=cinnamon_restaurant_get_option_data('mobilemenu_linecolors');
$mobilemenu_linecolors_rgb=cinnamon_restaurant_hex2RGB($mobilemenu_linecolors,true);
if ($mobilemenu_linecolors) {
	$dynamic_css .= cinnamon_restaurant_change_class('.responsive-mobile-menu ul.mtree li.mtree-node > ul > li:last-child,.theme-is-light .responsive-mobile-menu ul.mtree a,.theme-is-dark .responsive-mobile-menu ul.mtree a,.theme-is-light .responsive-mobile-menu #mobile-searchform input,.theme-is-dark .responsive-mobile-menu #mobile-searchform input',"border-color",$mobilemenu_linecolors,'');
	$dynamic_css .= cinnamon_restaurant_change_class('.theme-is-light .responsive-mobile-menu ul.mtree li.mtree-node > a::before,.theme-is-dark .responsive-mobile-menu ul.mtree li.mtree-node > a::before',"color",$mobilemenu_linecolors,'');
}
$mobilemenu_hover=cinnamon_restaurant_get_option_data('mobilemenu_hover');
if ($mobilemenu_hover) {
	$dynamic_css .= cinnamon_restaurant_change_class('
	.theme-is-light .responsive-mobile-menu ul.mtree li li a:hover,
	.theme-is-dark .responsive-mobile-menu ul.mtree li li a:hover,
	.header-is-simple.theme-is-light .responsive-mobile-menu ul.mtree li.mtree-open > a:hover,
	.header-is-simple.theme-is-dark .responsive-mobile-menu ul.mtree li.mtree-open > a:hover,
	.header-is-simple.theme-is-light .responsive-mobile-menu ul.mtree a:hover,
	.header-is-simple.theme-is-dark .responsive-mobile-menu ul.mtree a:hover,
	.header-is-simple.theme-is-light .responsive-mobile-menu ul.mtree li li a:hover,
	.header-is-simple.theme-is-dark .responsive-mobile-menu ul.mtree li li a:hover,
	.theme-is-light .responsive-mobile-menu ul.mtree li > a:hover,
	.theme-is-light .responsive-mobile-menu ul.mtree a:hover,
	.theme-is-dark .responsive-mobile-menu ul.mtree li > a:hover,
	.theme-is-dark .responsive-mobile-menu ul.mtree a:hover',"color",$mobilemenu_hover,'');
}
$mobilemenu_active=cinnamon_restaurant_get_option_data('mobilemenu_active');
if ($mobilemenu_active) {
	$dynamic_css .= cinnamon_restaurant_change_class('
		.header-is-simple.theme-is-light .responsive-mobile-menu ul.mtree li.mtree-open > a,
		.header-is-simple.theme-is-dark .responsive-mobile-menu ul.mtree li.mtree-open > a,
		.theme-is-light .responsive-mobile-menu ul.mtree li.mtree-open > a,
		.theme-is-dark .responsive-mobile-menu ul.mtree li.mtree-open > a',"color",$mobilemenu_active,'');
}
$rcm_font=cinnamon_restaurant_get_option_data('rcm_font');
if ($rcm_font<>"0" && $rcm_font!="Default Font") {
	$dynamic_css .= cinnamon_restaurant_apply_font ( "rcm_font" , '.dimmer-text' );
}
$rcm_size=cinnamon_restaurant_get_option_data('rcm_size');
if ($rcm_size<>"") {
	$dynamic_css .= '.dimmer-text { font-size:'.$rcm_size.'px; }';
}
$rcm_letterspacing=cinnamon_restaurant_get_option_data('rcm_letterspacing');
if ($rcm_letterspacing<>"") {
	$dynamic_css .= '.dimmer-text { letter-spacing:'.$rcm_letterspacing.'px; }';
}
$rcm_weight=cinnamon_restaurant_get_option_data('rcm_weight');
if ($rcm_weight<>"") {
	$dynamic_css .= '.dimmer-text { font-weight:'.$rcm_weight.'; }';
}
$rcm_textcolor=cinnamon_restaurant_get_option_data('rcm_textcolor');
if ($rcm_textcolor) {
	$dynamic_css .= " .dimmer-text { color:".$rcm_textcolor."; }";
}
$rcm_bgcolor=cinnamon_restaurant_get_option_data('rcm_bgcolor');
if ($rcm_bgcolor) {
	$dynamic_css .= " .dimmer-text { background:".$rcm_bgcolor."; }";
}

//Mobile Specific
$mobile_css = stripslashes_deep( cinnamon_restaurant_get_option_data('mobile_css') );
if (isSet($mobile_css) && $mobile_css!="") {
$dynamic_css .= '
@media only screen and (max-width: 1024px) {
	'.$mobile_css.'
}
@media only screen and (min-width: 768px) and (max-width: 959px) {
	'.$mobile_css.'
}
@media only screen and (max-width: 767px) {
	'.$mobile_css.'
}
@media only screen and (min-width: 480px) and (max-width: 767px) {
	'.$mobile_css.'
}';
}
?>