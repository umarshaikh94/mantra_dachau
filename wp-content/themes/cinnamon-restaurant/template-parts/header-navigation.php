<?php
$custom_menu_call = '';
if ( is_singular() ) {
	$user_choice_of_menu = get_post_meta( get_the_id() , 'pagemeta_menu_choice', true);
	if ( cinnamon_restaurant_page_is_woo_shop() ) {
		$woo_shop_post_id = get_option( 'woocommerce_shop_page_id' );
		$user_choice_of_menu = get_post_meta( $woo_shop_post_id , 'pagemeta_menu_choice', true);
	}
	if ( isSet($user_choice_of_menu) && $user_choice_of_menu <> "default") {
		$custom_menu_call = $user_choice_of_menu;
	}
}
$theme_menu_type = cinnamon_restaurant_get_option_data('header_menu_type');
if (cinnamon_restaurant_is_in_demo()) {
	if ( false != cinnamon_restaurant_demo_get_data('menu_type') ) {
		$theme_menu_type = cinnamon_restaurant_demo_get_data('menu_type');
	}
}

$menu_class = "sf-menu";
if ($theme_menu_type=="minimal-menu") {
	$menu_class = "mtree";
}
if ($theme_menu_type == "minimal-menu") {
?>
<div class="simple-menu">
	<?php
	// WPML
	$wpml_lang_selector_enable= cinnamon_restaurant_get_option_data('wpml_lang_selector_enable');
	if ($wpml_lang_selector_enable) {
	?>
	<div class="mobile-wpml-lang-selector-wrap">
		<div class="flags_language_selector"><?php cinnamon_restaurant_language_selector_flags(); ?></div >
	</div>
	<?php
	}
	?>
	<nav>
	<?php
	wp_nav_menu( array(
		'container' =>false,
		'menu' => $custom_menu_call,
		'theme_location' => 'main_menu',
		'menu_class' => $menu_class,
		'before' => '',
		'after' => '',
		'link_before' => '',
		'link_after' => '',
		'depth' => 0,
		'fallback_cb' => 'mtheme_nav_fallback'
	));
	?>
	</nav>
	<div class="mobile-social-header">
	<?php 
	dynamic_sidebar("mobile_social_header");
	?>
	</div>
	<div class="cleafix"></div>
</div>
<?php
}

function cinnamon_restaurant_main_menu_logo($header_menu_type="header-middle") {
	$header_menu_type = cinnamon_restaurant_get_option_data('header_menu_type');
	if (cinnamon_restaurant_is_in_demo()) {
		if ( false != cinnamon_restaurant_demo_get_data('menu_type') ) {
			$header_menu_type = cinnamon_restaurant_demo_get_data('menu_type');
		}
	}
	$logo_element = '';
	$sticky_main_logo=cinnamon_restaurant_get_option_data('sticky_main_logo');
	$sticky_logo_class='';
	if ($sticky_main_logo<>"") {
		$sticky_logo_class = " sticky-alt-logo-present";
	}
	$logo_element .= '<div class="header-logo-section">';
		$logo_element .= '<div class="logo'.$sticky_logo_class.'">';
			
			$home_url_path = home_url('/');
			$logo_element .= '<a href="'.esc_url($home_url_path).'">';
				$main_logo=cinnamon_restaurant_get_option_data('main_logo');
				$main_logo_inverse=cinnamon_restaurant_get_option_data('main_logo_inverse');
				if ( $main_logo <> "" ) {
					if ( $main_logo_inverse=="" ) {
						$main_logo_inverse = $main_logo;
					}
					$logo_element .= '<img class="logo-theme-main logo-theme-custom" src="'.esc_url($main_logo).'" alt="logo" />';
					$logo_element .= '<img class="logo-theme-main logo-theme-custom logo-theme-inverse" src="'.esc_url($main_logo_inverse).'" alt="logo" />';
				} else {
					$demo_logo_image = 'logo_dark.png';
					$demo_logo_image_inverse = 'logo_bright.png';
					if ($header_menu_type=="center-logo") {
						$demo_logo_image = "logo_center_dark.png";
						$demo_logo_image_inverse = 'logo_center_bright.png';
					}
					$logo_element .= '<img class="logo-theme-main logo-theme-primary logo-theme-dark" src="'.get_template_directory_uri() . '/images/'.$demo_logo_image.'" alt="logo" />';
					$logo_element .= '<img class="logo-theme-main logo-theme-inverse" src="'.get_template_directory_uri() . '/images/'.$demo_logo_image_inverse.'" alt="logo" />';
				}

			$logo_element .= '</a>';
		$logo_element .= '</div>';
	$logo_element .= '</div>';
	return $logo_element;
}
if ( cinnamon_restaurant_page_supports_fullscreen() ) {
	echo '<div class="slideshow-control-item mtheme-fullscreen-toggle fullscreen-toggle-off"><i class="feather-icon-plus"></i></div>';
}
if ( !cinnamon_restaurant_menu_is_vertical() ) {
	if ( is_active_sidebar( 'dashboard_sidebar' ) ) {
?>
	<nav id="sidebarinfo-toggle-menu" class="sidebar-toggle-menu sidebar-toggle-menu-close">
		<span class="sidebar-toggle-menu-trigger"><span>Menu</span></span>
	</nav>
	<div class="sidebar-menu-overlay"></div>
	<div class="sidebarinfo-menu">
		<?php 
		dynamic_sidebar("dashboard_sidebar");
		?>
		<div class="cleafix"></div>
	</div>
<?php
	}
}
if ($theme_menu_type=="minimal-menu") {
?>
	<div class="minimal-menu-overlay"></div>
	<nav id="minimal-toggle-menu" class="mobile-toggle-menu mobile-toggle-menu-close">
		<span class="mobile-toggle-menu-trigger"><span>Menu</span></span>
	</nav>
<?php
}
if ( cinnamon_restaurant_menu_is_vertical() ) {
	get_template_part('template-parts/menu/vertical','menu');
} else {
	?>
	<div class="outer-wrap stickymenu-zone">
	<?php
	$header_menu_top=cinnamon_restaurant_get_option_data('header_menu_top');
	if (cinnamon_restaurant_is_in_demo()) {
		if ( false != cinnamon_restaurant_demo_get_data('headermenutop') ) {
			$header_menu_top = cinnamon_restaurant_demo_get_data('headermenutop');
		}
	}
	if ( $header_menu_top=="enable" ) {
	?>
	<div class="menu-social-header">
	<?php
	if ($theme_menu_type=="left-logo") { echo '<div class="social-left-menu fullpage-item">'; }
	dynamic_sidebar("social_header");
	if ($theme_menu_type=="left-logo") { echo '</div>'; }
	?>
	</div>
	<?php
	}
	// WPML
	$wpml_lang_selector_enable= cinnamon_restaurant_get_option_data('wpml_lang_selector_enable');
	if ($wpml_lang_selector_enable) {
	?>
	<div class="wpml-lang-selector-wrap">
		<div class="flags_language_selector"><?php cinnamon_restaurant_language_selector_flags(); ?></div >
	</div>
	<?php
	}
	?>
		<div class="outer-header-wrap clearfix">
			<?php
			$reservation_button_title = cinnamon_restaurant_get_option_data('reservation_button_title');
			$reservation_page         = cinnamon_restaurant_get_option_data('reservation_page');
			$reservation_url          = cinnamon_restaurant_get_option_data('reservation_url');
			if ($reservation_button_title<>"") {
				$button_link_start = '<span class="button-element sticky-menu-switch button-default-outline button-black-outline">';
				$button_link_end = '</span>';
				$button_toggle = ' menu-button-toggle';
				if ($reservation_page<>'0') {
					$reservation_page_url = get_the_permalink($reservation_page);
					$button_link_start = '<a class="button-element sticky-menu-switch button-default-outline button-black-outline" href="'.esc_url($reservation_page_url).'">';
					$button_link_end = '</a>';
					$button_toggle = '';
				}
				if ( '' !== $reservation_url ) {
					$reservation_page_url = $reservation_url;
					$button_link_start = '<a class="button-element sticky-menu-switch button-default-outline button-black-outline" href="'.esc_url($reservation_page_url).'">';
					$button_link_end = '</a>';
					$button_toggle = '';
				}
				echo '<div class="reservation-button'. esc_attr($button_toggle).'">'.$button_link_start . $reservation_button_title . $button_link_end.'</div>';
			}
			?>
			<nav>
<?php
	$header_menu_type = cinnamon_restaurant_get_option_data('header_menu_type');
	if (cinnamon_restaurant_is_in_demo()) {
		if ( false != cinnamon_restaurant_demo_get_data('menu_type') ) {
			$header_menu_type = cinnamon_restaurant_demo_get_data('menu_type');
		}
	}
	$adjustable = '';
	if ($header_menu_type == "left-logo") {
		$adjustable = "fullpage-item";
	}
?>
				<div class="mainmenu-navigation <?php echo esc_attr($adjustable); ?>">
						<?php
						$header_menu_type = cinnamon_restaurant_get_option_data('header_menu_type');
						if ($header_menu_type=="" || !isSet($header_menu_type) ) {
							$header_menu_type="split-menu";
						}
						echo cinnamon_restaurant_main_menu_logo($header_menu_type);
						if ($theme_menu_type <> "minimal-menu") {
							if ( has_nav_menu( "main_menu" ) ) {
							?>
								<div class="homemenu">
							<?php
								wp_nav_menu( array(
									'container' =>false,
									'menu' => $custom_menu_call,
									'theme_location' => 'main_menu',
									'menu_class' => $menu_class,
									'before' => '',
									'after' => '',
									'link_before' => '',
									'link_after' => '',
									'depth' => 0,
									'fallback_cb' => 'mtheme_nav_fallback'
								));
								if ( !cinnamon_restaurant_get_option_data('mtheme_woocart_menu') ) {
									if ( class_exists( 'woocommerce' ) ) {
										echo '<span class="header-cart header-cart-toggle"><i class="ion-bag"></i></span>';
									}
									do_action('cinnamon_restaurant_header_woocommerce_shopping_cart_counter');
								}
							?>
							</div>
							<?php
							}
						}
						?>
				</div>
			</nav>
		</div>
	</div>
	<?php
}
?>