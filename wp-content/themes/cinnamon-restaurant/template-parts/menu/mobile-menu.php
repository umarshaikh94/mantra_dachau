<div class="responsive-menu-wrap">
	<nav id="mobile-toggle-menu" class="mobile-toggle-menu mobile-toggle-menu-close">
		<span class="mobile-toggle-menu-trigger"><span>Menu</span></span>
	</nav>
	<?php
	$reservation_button_title = '<i class="ion-android-restaurant"></i>';
	$reservation_page         = cinnamon_restaurant_get_option_data('reservation_page');
	$reservation_url          = cinnamon_restaurant_get_option_data('reservation_url');
	if ($reservation_button_title<>"") {
		$button_link_start = '<span class="reservation-button-element">';
		$button_link_end = '</span>';
		$button_toggle = ' menu-button-toggle';
		if ($reservation_page<>'0') {
			$reservation_page_url = get_the_permalink($reservation_page);
			$button_link_start = '<a class="reservation-button-element" href="'.esc_url($reservation_page_url).'">';
			$button_link_end = '</a>';
			$button_toggle = '';
		}
		if ( '' !== $reservation_url ) {
			$reservation_page_url = $reservation_url;
			$button_link_start = '<a class="reservation-button-element" href="'.esc_url($reservation_page_url).'">';
			$button_link_end = '</a>';
			$button_toggle = '';
		}
		echo '<div class="reservation-button'. esc_attr($button_toggle).'">'. $button_link_start . $reservation_button_title . $button_link_end .'</div>';
	}
	?>
	<div class="mobile-menu-toggle">
				<div class="logo-mobile">
						<?php
						$main_logo=cinnamon_restaurant_get_option_data('main_logo');
						$responsive_logo=cinnamon_restaurant_get_option_data('responsive_logo');
						$theme_style=cinnamon_restaurant_get_option_data('theme_style');
						$header_menu_type = cinnamon_restaurant_get_option_data('header_menu_type');

						if ($main_logo<>"" && $responsive_logo=="") {
							$responsive_logo = $main_logo;
						}

						if (cinnamon_restaurant_is_in_demo()) {
							if ( false != cinnamon_restaurant_demo_get_data('theme_style') ) {
								$theme_style = cinnamon_restaurant_demo_get_data('theme_style');
							}
						}
						if (cinnamon_restaurant_is_in_demo()) {
							if ( false != cinnamon_restaurant_demo_get_data('menu_type') ) {
								$header_menu_type = cinnamon_restaurant_demo_get_data('menu_type');
							}
						}

						$home_url_path = home_url('/');

						if ($responsive_logo<>"") {
							$mobile_logo = '<img class="custom-responsive-logo logoimage" src="'.esc_url($responsive_logo).'" alt="logo" />';
						} else {
							if ( $theme_style == "light" ) {
								$mobile_logo = '<img class="logoimage" src="'.esc_url(get_template_directory_uri() . '/images/logo_dark.png').'" alt="logo" />';
							} else {
								$mobile_logo = '<img class="logoimage" src="'.esc_url(get_template_directory_uri() . '/images/logo.png').'" alt="logo" />';
							}
						}
						echo '<a href="'.esc_url($home_url_path).'">' . $mobile_logo . '</a>';
						?>
				</div>
	</div>
</div>
<div class="responsive-menu-overlay"></div>
<div class="responsive-mobile-menu">
	<?php
	// WPML
	$wpml_lang_selector_enable= cinnamon_restaurant_get_option_data('wpml_lang_selector_enable');
	if ($wpml_lang_selector_enable) {
	?>
	<div class="mobile-wpml-lang-selector-wrap">
		<div class="flags_language_selector"><?php cinnamon_restaurant_language_selector_flags(); ?></div >
	</div>
	<?php
	}
	?>
	<nav>
	<?php
	$custom_menu_call = '';
	$user_choice_of_menu = get_post_meta( get_the_id() , 'pagemeta_menu_choice', true);
	if ( cinnamon_restaurant_page_is_woo_shop() ) {
		$woo_shop_post_id = get_option( 'woocommerce_shop_page_id' );
		$user_choice_of_menu = get_post_meta( $woo_shop_post_id , 'pagemeta_menu_choice', true);
	}
	if ( isSet($user_choice_of_menu) && $user_choice_of_menu <> "default") {
		$custom_menu_call = $user_choice_of_menu;
	}
	// Responsive menu conversion to drop down list
	if ( function_exists('wp_nav_menu') ) { 
		wp_nav_menu( array(
		 'container' =>false,
		 'theme_location' => 'mobile_menu',
		 'menu' => $custom_menu_call,
		 'menu_class' => 'mtree',
		 'echo' => true,
		 'before' => '',
		 'after' => '',
		 'link_before' => '',
		 'link_after' => '',
		 'depth' => 0,
		 'fallback_cb' => 'mtheme_nav_fallback'
		 )
		);
	}
	?>
	</nav>
	<div class="mobile-social-header">
	<?php 
	dynamic_sidebar("mobile_social_header");
	?>
	</div>
	<div class="cleafix"></div>
</div>