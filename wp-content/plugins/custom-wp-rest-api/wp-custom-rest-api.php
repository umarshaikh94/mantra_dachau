<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://dipankar-team.business.site
 * @since             1.0.0
 * @package           Customwprest
 *
 * @wordpress-plugin
 * Plugin Name:       Custom Endpoints With Wp Rest Api
 * Plugin URI:        http://wcra.gmnckkp.in
 * Description:       Add Custom Endpoints to the Wordpress REST API? Fantastic! Let’s get started with this plugin
 * Version:           2.1.1
 * Author:            Dipankar Pal
 * Author URI:        https://profiles.wordpress.org/dipankarpal212
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       customwprest
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */

define( 'WCRA_PLUGIN_VERSION', '2.1.1' );
define( 'WCRA_DB', 'wcra_' );
define( 'WCRA_PLUGIN_SLUG', 'custom-wp-rest-api' );
define( 'WCRA_PLUGIN_TEXTDOMAIN', 'customwprest' );

$plugin = plugin_basename( __FILE__ );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-customwprest-activator.php
 */
function wcra_activate_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-customwprest-activator.php';
	WCRA_Activator::activate();
	
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-customwprest-deactivator.php
 */
function wcra_deactivate_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-customwprest-deactivator.php';
	WCRA_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'wcra_activate_plugin' );
register_deactivation_hook( __FILE__, 'wcra_deactivate_plugin' );
add_filter( "plugin_action_links_$plugin", 'wcra_add_settings_link' );


/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-customwprest.php';
require plugin_dir_path( __FILE__ ) . 'admin/class-update.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function wcra_run_customwprest() {

	$plugin = new WCRA_Controller();
	$plugin->run();

}

add_filter("wcra_register_user_callback" , "wcra_register_user_callback_handler");
function wcra_register_user_callback_handler($param){
    global $wpdb;
    $table =  $wpdb->prefix.'app_users';
    $table_meta = $wpdb->prefix.'app_usermeta';
    
    if(empty($param['email']) || empty($param['password']) || empty($param['full_name'])){
        $response = array('response_status' => 'failure', 'details' => "Provided Parameter list is not complete!" );
        return $response;
    }
    
//    *********************check availability of provided email**********************************
    $user_name = $param['email'];
    $email_exists = $wpdb->get_results("select * from $table where user_login  = '$user_name' or user_email = '$user_name' ");
    if(count($email_exists) > 0){
        $response = array('response_status' => 'failure', 'details' => "Provided email already has been taken!" );
        return $response; // stop execution
    }
    
    $api_password = wp_generate_password( 33, false );
    $hash = password_hash($param['password'], PASSWORD_DEFAULT);
    $user_act_key = time().wp_generate_password( 33, false );
    $data = array(
        "user_login" => $param['email'],
        "user_pass" => $hash ,
        "user_nicename" => $param['full_name'],
        "user_email" => $param['email'],
        "user_url" => "",
        "user_registered" => date('Y-m-d H:i:s'),
        "api_password" => "$api_password",
        "user_activation_key" => $user_act_key,
        "user_status" => "0",
        "display_name" => "",

    );
$res = $wpdb->insert($table, $data);
$lastid = $wpdb->insert_id;
$data_mata = array('user_id' => $lastid, 'meta_key' => 'user_address', 'meta_value' => "" );
$res2 = $wpdb->insert($table_meta, $data_mata);
  
if($res){
        $user_details[] = array(
        "user_email" => $param['email'],
        "api_password" => $api_password,
        "user_full_name" => $param['full_name'],
        "user_address" => '',
        "phone_no" => null,
        "city" => null,
        "email_verified" => false
        // "user_registration_date" => $res[0]->user_registered,
    );
    
$inputpass = $param['password'];
$home_url = get_home_url();
$link = $home_url."/email-verification/?uid=$lastid&ac=$user_act_key";
$to = $param['email'];
$subject = "Mantra Account registration";
                   $message= "
                     Dear user, 
                     Your account has been successfully registered in Mantra. Your password is: $inputpass
                     We recommend you to set your password with a secure and easy to remember one.
                     You need to verify your email to complete the registration in Mantra.
                     Please click on the link below or copy following link to your browser to verify your email: 
                     $link
                         
                     Regards 
                     Mantra
                     ";

$headers[] = 'From: Mantra <noreply@mantra-dachu.com>';
$attachments = "";

$email_status = wp_mail( $to, $subject, $message, $headers, $attachments );
$response = array('response_status' => 'successful', 'details' => 'User has been registered successfully', 'user' => $user_details );
} else {
    $response = array('response_status' => 'failure', 'details' => $res );
}
return $response;

}
// -------------------user login--------------------------------
add_filter("wcra_user_login_callback" , "wcra_user_login_callback_handler");
function wcra_user_login_callback_handler($param){
global $wpdb;
    $user_table =  $wpdb->prefix.'app_users';
    $meta_table =  $wpdb->prefix.'app_usermeta';
    $username  = $param['email'];
    $password  = $param['password'];
    
    
    $res = $wpdb->get_results("select * from $user_table where user_login  = '$username' ");
    
    $validate_user = password_verify($password,$res[0]->user_pass );
    if($validate_user){
//        ************update_api_password**************
    $api_password = wp_generate_password( 33, false );
    $data = array(
        'api_password' => $api_password
    );
    $where = array(
        'ID' => $res[0]->ID 
    );
    $update_res = $wpdb->update($user_table,$data,$where );
//        ************ end of update_api_password**************
   $user_id = $res[0]->ID;
            $location_query = $wpdb->get_results("select * from $meta_table where user_id= $user_id and meta_key = 'user_address' ");
   $location = $location_query[0]->meta_value;
    $user_details[] = array(
        "user_email" => $res[0]->user_email,
        "api_password" => $api_password,
        "user_full_name" => $res[0]->user_nicename,
        "user_address" => $location,
        "phone_no" => null,
        "city" => null,
        "email_verified" => false,
        "user_registration_date" => $res[0]->user_registered,
    );
    
    
$response = array('response_status' => 'successful', 'user' => $user_details );
} else {
    $response = array('response_status' => 'failure', 'details' => 'Invalid credentials!' );
}
return $response;
}

// -----------------------get_user_profile ------------------------
add_filter("wcra_get_user_profile_callback" , "wcra_get_user_profile_callback_handler");
function wcra_get_user_profile_callback_handler($param){
    global $wpdb;
    $user_table =  $wpdb->prefix.'app_users';
    $meta_table =  $wpdb->prefix.'app_usermeta';
        if(empty($param['email']) || empty($param['api_password'])){
        $response = array('response_status' => 'failure', 'details' => "Provided Parameter list is not complete!" );
        return $response;
    }
    
    $username  = $param['email'];
    $api_password  = $param['api_password'];
    
    $res = $wpdb->get_results("select * from $user_table where user_login  = '$username'and api_password  = '$api_password' ");

    if(count($res) > 0){
            
            $user_id = $res[0]->ID;
            $location_query = $wpdb->get_results("select * from $meta_table where user_id= $user_id and meta_key = 'user_address' ");
       
    //   print_r($location_query);
    //   die;
        $location = $location_query[0]->meta_value;
        
       $user_details[] = array(
        "user_email" => $res[0]->user_email,
        "user_full_name" => $res[0]->user_nicename,
        "user_address" => $location,
        "user_registration_date" => $res[0]->user_registered,
    );
$response = array('response_status' => 'successful', 'user' => $user_details );
} else {
    $response = array('response_status' => 'failure', 'details' => 'Invalid credentials!' );
} 
    return $response;
}

//-------------------------update user profile------------------------------
add_filter("wcra_update_user_profile_callback" , "wcra_update_user_profile_callback_handler");
function wcra_update_user_profile_callback_handler($param){
  global $wpdb;
        $user_table =  $wpdb->prefix.'app_users';
        $usermeta_table =  $wpdb->prefix.'app_usermeta';
    
           if(empty($param['email']) || empty($param['api_password'])){
        $response = array('response_status' => 'failure', 'details' => "Provided Parameter list is not complete!" );
        return $response;
    } 
    
    
           if(empty($param['full_name']) && empty($param['location'])){
        $response = array('response_status' => 'failure', 'details' => "Nothing provided to update!" );
        return $response;
    } 
    
    
        $username  = $param['email'];
    $api_password  = $param['api_password'];
    
    $res = $wpdb->get_results("select * from $user_table where user_login  = '$username'and api_password  = '$api_password' ");

    if(count($res) > 0){
            $up_data = array(
        'meta_value' => $param['location']
    );
    $where = array(
        'user_id' => $res[0]->ID,
        'meta_key' => 'user_address'
        
    );
        $update_res = "";
        $update_res2 = "";
    if($param['location']){

        $update_res = $wpdb->update($usermeta_table,$up_data,$where );
    }
    if($param['full_name']){
        $update_res2 = $wpdb->update($user_table,['user_nicename'=>$param['full_name']],[ID=>$res[0]->ID] );
            }
            
 $user_id = $res[0]->ID;
    $updates = $wpdb->get_results("select * from $usermeta_table where user_id = $user_id and meta_key = 'user_address'");  
          $res2 = $wpdb->get_results("select * from $user_table where user_login  = '$username'and api_password  = '$api_password' ");      
            
$profile_array= array(
            "user_email" => $res2[0]->user_email,
        "user_full_name" => $res2[0]->user_nicename,
        "user_address" => $updates[0]->meta_value,
    );

            
    if($update_res == 0 && $update_res2 == 1){
    $response = array('response_status' => 'successful', 'user' => $profile_array);
} else if($update_res == 1 && $update_res2 == 0) {
        $response = array('response_status' => 'successful', 'user' => $profile_array);

}else if($update_res == 0 && $update_res2 == 0) {
    $response = array('response_status' => 'failure', 'details' => "Provided details were not updated!", 'user' => $profile_array);
} else {
     $response = array('response_status' => 'successful', 'user' => $profile_array);
    
}

} else {
    $response = array('response_status' => 'failure', 'details' => 'Invalid credentials!' );
}

    


return $response;
}

// --------------------------------menu_list---------------------
add_filter("wcra_get_menu_list_callback" , "wcra_get_menu_list_callback_handler");
function wcra_get_menu_list_callback_handler($param){
 global $wpdb;

    $menu_table1 =  $wpdb->prefix.'terms';
    $menu_table2 =  $wpdb->prefix.'term_taxonomy';
    $menu_table3 =  $wpdb->prefix.'term_relationships';
    $menu_table4 =  $wpdb->prefix.'posts';
    $menu_table5 =  $wpdb->prefix.'postmeta';
    
    $menu_list = $wpdb->get_results("SELECT $menu_table1.name, $menu_table2.* FROM $menu_table1,$menu_table2 WHERE 
    $menu_table1.term_id = $menu_table2.term_id and
    $menu_table2.taxonomy = 'foodcategory' and
    $menu_table1.name <> 'Gewürze' and
    $menu_table2.count > 0");
    

    $menu_sub_list = $wpdb->get_results("SELECT $menu_table1.*,$menu_table2.*,$menu_table3.object_id,$menu_table3.term_taxonomy_id as termtexonomy_id,$menu_table4.ID,$menu_table4.guid,$menu_table4.post_title as sub_menu_title,$menu_table4.post_excerpt as sub_menu_description  
    FROM $menu_table1,$menu_table2,$menu_table3,$menu_table4 WHERE 
    $menu_table1.term_id = $menu_table2.term_id and
    $menu_table2.taxonomy = 'foodcategory' and 
    $menu_table2.count > 0 and
    $menu_table2.term_taxonomy_id = $menu_table3.term_taxonomy_id and
    $menu_table3.object_id = $menu_table4.ID 
    
   ");

   $product_post_guid = $wpdb->get_results("select * from $menu_table4 where post_type = 'attachment' ");
   foreach($product_post_guid as $prg_key => $prg_val){
    foreach($menu_list as $mslg_key => $msl_val){

        // if(strtoupper($menu_list[$mslg_key]->name) == strtoupper('SPEZIALITATEN MIT HUHN')){
        if(strtoupper($menu_list[$mslg_key]->name) == strtoupper($product_post_guid[$prg_key]->post_title)){
        $menu_list[$mslg_key]->menu_icon = $product_post_guid[$prg_key]->guid;
        $menu_list[$mslg_key]->guid_post_id = $product_post_guid[$prg_key]->ID;

        }
}
}
   

   
   
   $product_post = $wpdb->get_results("select * from $menu_table4 where post_type = 'product' ");

foreach($product_post as $pr_key => $pr_val){
    foreach($menu_sub_list as $msl_key => $msl_val){
        if($menu_sub_list[$msl_key]->sub_menu_title == $product_post[$pr_key]->post_title){
        $menu_sub_list[$msl_key]->pro_id = $pr_val->ID;

        }
}
}


   $meta_result = $wpdb->get_results("select * from $menu_table5 where $menu_table5.meta_key = 'pagemeta_price_food' ");
           

    
    $euro = "\xE2\x82\xAc";
    $menu_items = array();
    foreach ($menu_list as $key => $val1){
        foreach ($menu_sub_list as $key2 => $val2){
            if($val1->term_taxonomy_id == $val2->term_taxonomy_id){
                $menu_items[$key]['menu_detail']['term_id'] =  $val1->term_id;
                $menu_items[$key]['menu_detail']['term_taxonomy_id'] =  $val1->term_taxonomy_id;
                $menu_items[$key]['menu_detail']['menu_title'] = $val1->name;
                $menu_items[$key]['menu_detail']['menu_icon'] = $val1->menu_icon;
                $menu_items[$key]['menu_detail']['sub_menu_detail'][] = ['object_id' =>$val2->pro_id,'sub_menu_title' =>$val2->sub_menu_title,'sub_menu_description' =>$val2->sub_menu_description,'obj_id' =>$val2->object_id];
            }
            
    
        }
    }

    foreach($menu_items as $men_key => $men_val){
        foreach($men_val['menu_detail']['sub_menu_detail'] as $sub_key => $sub_val){

        foreach($meta_result as $met_key => $met_val){
            


            if($sub_val['obj_id'] ==$met_val->post_id ){

            
                $menu_items[$men_key]['menu_detail']['sub_menu_detail'][$sub_key]['sub_menu_rate'] = substr($met_val->meta_value,0,4);
                $menu_items[$men_key]['menu_detail']['sub_menu_detail'][$sub_key]['currency'] = $euro;
                

            }

        }

    }

    }

$response = array('response_status' => 'successful', 'menu_list' => $menu_items );

        
return $response;
}

// ------------------- upload_profile_picture ---------------------------
add_filter("wcra_upload_profile_picture_callback" , "wcra_upload_profile_picture_callback_handler");
function wcra_upload_profile_picture_callback_handler($param){

  global $wpdb;
        $user_table =  $wpdb->prefix.'app_users';
        $meta_table =  $wpdb->prefix.'app_usermeta';
    
           if(empty($param['email']) || empty($param['api_password']) || !isset($_FILES['uploaded_file']['name']) && $_FILES['uploaded_file']['name'] == ""){
        $response = array('response_status' => 'failure', 'details' => "Provided Parameter list is not complete!" );
        return $response;
    } 
    
        $username  = $param['email'];
    $api_password  = $param['api_password'];
    
    $res = $wpdb->get_results("select * from $user_table where user_login  = '$username'and api_password  = '$api_password' ");

    if(count($res) > 0){
$user_id = $res[0]->ID;
    $dp_exists = $wpdb->get_results("select * from $meta_table where user_id= $user_id and meta_key = 'user_profile_img' ");

$target_dir = wp_upload_dir()['basedir']."/profile_dp/";
   if (isset($_FILES['uploaded_file']['name']) && $_FILES['uploaded_file']['name'] != "" ) {
            list($txt, $extention) = explode(".", $_FILES['uploaded_file']['name']);
            $ext = strtolower($extention);
            if ($ext == "png" || $ext == "jpg" || $ext == "jpeg"  && $_FILES['uploaded_file']['size'] > 0) {
                    $uploadedFile = $_FILES["uploaded_file"]["tmp_name"];
                  $stripped = preg_replace('/\s+/','', $res[0]->user_nicename);
                        $full_path = $target_dir;
                        $actual_file_name = $res[0]->ID."__".$stripped.date('Y-m-d')."." . $ext;

                        if (move_uploaded_file($uploadedFile, $full_path.$actual_file_name)) {
         if(count($dp_exists)>0){
         $update_data = array(
        'meta_key' => 'user_profile_img',
        'meta_value' => $actual_file_name
    );
    $where = array(
        'user_id' => $res[0]->ID,
    );
    $update_res = $wpdb->update($meta_table,$update_data,$where ); 
         } else {
        $insert_data = array(
            'user_id' => $res[0]->ID,
            'meta_key' => 'user_profile_img',
            'meta_value' => $actual_file_name
    );
    $res = $wpdb->insert($meta_table, $insert_data);
         }                    
                            
   $response = array('response_status' => 'successful', 'details' => 'Image uploaded successfully', 'download_path' => wp_upload_dir()['baseurl']."/profile_dp/".$actual_file_name );
                        } else {
                               $response = array('response_status' => 'failure', 'details' => 'Image uploading failed' );
                        }
                    
            } else {
                $response = array('response_status' => 'failure', 'details' => 'only png, jpg and jpeg formats are acceptable!' );
            }
        } else{
   $response = array('response_status' => 'failure', 'details' => 'An image must be selected to upload!' );
}
} else {
    $response = array('response_status' => 'failure', 'details' => 'Invalid credentials!' );

    
}
    return $response;

}
// -------------------get_profile_picture--------------------------
add_filter("wcra_get_profile_picture_callback" , "wcra_get_profile_picture_callback_handler");
function wcra_get_profile_picture_callback_handler($param){
  global $wpdb;
        $user_table =  $wpdb->prefix.'app_users';
        $meta_table =  $wpdb->prefix.'app_usermeta';
    
           if(empty($param['email']) || empty($param['api_password']) ){
        $response = array('response_status' => 'failure', 'details' => "Provided Parameter list is not complete!" );
        return $response;
    } 
    
        $username  = $param['email'];
    $api_password  = $param['api_password'];
    
    $res = $wpdb->get_results("select * from $user_table where user_login  = '$username'and api_password  = '$api_password' ");

    if(count($res) > 0){
$user_id = $res[0]->ID;
    $dp_exists = $wpdb->get_results("select * from $meta_table where user_id= $user_id and meta_key = 'user_profile_img' ");
    if(count($dp_exists) > 0 ){
        $actual_file_name = $dp_exists[0]->meta_value;
    
    
    $img_url = wp_upload_dir()['baseurl']."/profile_dp/".$actual_file_name;
                $img_file = file_get_contents($img_url);
        header('Content-type: image/jpeg'); 
        echo $img_file;
        die;
    } else {
           $response = array('response_status' => 'failure', 'details' => 'No image found for provided details!' );
    }
    } else {
           $response = array('response_status' => 'failure', 'details' => 'Invalid credentials!' );
    }
return $response;
}

// --------------------change_user_password-----------------------
add_filter("wcra_change_user_password_callback" , "wcra_change_user_password_callback_handler");
function wcra_change_user_password_callback_handler($param){
  global $wpdb;
        $user_table =  $wpdb->prefix.'app_users';
        $meta_table =  $wpdb->prefix.'app_usermeta';
    
           if(empty($param['email']) || empty($param['api_password']) || empty($param['password']) || empty($param['new_password']) ){
        $response = array('response_status' => 'failure', 'details' => "Provided Parameter list is not complete!" );
        return $response;
    } 
    
        $username  = $param['email'];
    $api_password  = $param['api_password'];
        $password  = $param['password'];
    $new_password  = $param['new_password'];
    
    $res = $wpdb->get_results("select * from $user_table where user_login  = '$username'and api_password  = '$api_password' ");

    if(count($res) > 0){
        
    $res = $wpdb->get_results("select * from $user_table where user_login  = '$username' ");
    $where = array("id" => $res[0]->ID);
    $validate_user = password_verify($password,$res[0]->user_pass );
    
    if($validate_user){

        $pass = password_hash($new_password, PASSWORD_DEFAULT);
    $api_password = wp_generate_password( 33, false );

       $update_data = array(
           "user_pass" => $pass,
           "api_password" => $api_password
           );
       $update_status = $wpdb->update($user_table, $update_data, $where);
if($update_status){
        $response = array('response_status' => 'successful', 'details' => 'Password updated successfully.', 'api_password' => $api_password );
} else {
            $response = array('response_status' => 'failure', 'details' => 'Password update failed! Please try again');
}
    } else {
        $response = array('response_status' => 'failure', 'details' => 'Incorrect current password provided!');
    }    
        
    } else {
        return 'invalid credentials';
    }
    
    
    return $response;
   

    

}

// -------------------reset_password--------------------------
add_filter("wcra_reset_user_password_callback" , "wcra_reset_user_password_callback_handler");
function wcra_reset_user_password_callback_handler($param){

  global $wpdb;
        $user_table =  $wpdb->prefix.'app_users';
        $meta_table =  $wpdb->prefix.'app_usermeta';
    
           if(empty($param['email']) ){
        $response = array('response_status' => 'failure', 'details' => "Provided Parameter list is not complete!" );
        return $response;
    } 
    
        $username  = $param['email'];

    $res = $wpdb->get_results("select * from $user_table where user_login  = '$username' ");


    if(count($res) > 0){
$user_hash = time().wp_generate_password( 33, false );
$user_id = $res[0]->ID;
$update_hash = array(
    "user_activation_key" => $user_hash
    );
$where = array(
    "ID" => $user_id
    );


$update_data = $wpdb->update($user_table,$update_hash,$where);
while($update_data != 1){

    $user_rehash = time().wp_generate_password( 33, false );
       $update_data = $wpdb->update($user_table,array(
    "user_activation_key" => $user_rehash
    ),$where);
$user_hash = $user_rehash;
}

$home_url = get_home_url();
$link = $home_url."/password-reset/?uid=$user_id&hsh=$user_hash";
$to = $res[0]->user_login;
$subject = "Mantra Reset Password";
                   $message= "
                     Dear user, 
                     You have received a new reset password request from Mantra.
                     Please click on the link below or copy following link to your browser to reset your password: 
                     $link
                         
                     Regards 
                     Mantra
                     ";

$headers[] = 'From: Mantra <noreply@mantra-dachu.com>';
$attachments = "";

$email_status = wp_mail( $to, $subject, $message, $headers, $attachments );
    if($email_status){
    $response = array('response_status' => 'successful', 'details' => 'Reset Password email sent successfully' );
    } else {
            $response = array('response_status' => 'failure', 'details' => 'Reset password email sending failed' );
    }
} else {
    $response = array('response_status' => 'failure', 'details' => 'Provided email is not registered!' );
}

return $response;
}
// ---------------------------submit_order_details----------------------
add_filter("wcra_submitted_orders_callback" , "wcra_submitted_orders_callback_handler");
function wcra_submitted_orders_callback_handler($param){
   

 global $wpdb;
    $order_table =  $wpdb->prefix.'submitted_orders';



    $email = $param['email'];
    $full_name = $param['full_name'];
    $phone_no = $param['phone_no'];
    $role = $param['role'];
    $items_details = $param['items_details'];
    $address = $param['address'];
    $city = $param['city'];
    $zip_code = $param['zip_code'];
    $order_method = $param['order_method']; //pickup,delivery
    $payment_type = $param['payment_type'];
    $total_amount = $param['total_amount'];
    $remarks = $param['remarks'];
    $creditcard_no = $param['card_no'];
    $verification_code = $param['verification'];
    $expiry_date = $param['expiry_date'];

if($email == "" || $full_name == "" || $role == "" || $items_details == "" || $zip_code == "" || $order_method == "" || $payment_type == "" || $total_amount == "" || $total_amount == 0 ){
        $response = array('response_status' => 'failure', 'details' => 'Some of the required parameter is missing!' );
} else {
    $data = array(
        
            'email' => $email,
            'phone_no' => $phone_no,
            'items_detail' =>  $items_details, 
            'remarks' =>  $remarks, 
            'full_name' => $full_name,
            'address' => $address,
            'city' => $city,
            'zip_code' => $zip_code,
            'role' => $role,
            'order_method' => $order_method ,
            'payment_method' => $payment_type,
            'total_amount' => $total_amount,
            'card_number' => $creditcard_no,
            'security_code' => $verification_code,
            'expiry_date' => $expiry_date,
            'order_status' => 'Order request submitted',
            'payment_provider_response' => '',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
    );
    

    $name = trim($full_name);
    if (strpos($name, ' ') === false) {
        $firstname = $name;
        $lastname = '';

    }else {

    $parts     = explode(" ", $name);
    $lastname  = array_pop($parts);
    $firstname = implode(" ", $parts);
    }

    $parts     = explode(" ", $name);
    $lastname  = array_pop($parts);
    $firstname = implode(" ", $parts);
    

    
  $res = $wpdb->insert($order_table,$data);
        $lastid = $wpdb->insert_id;
//****************Credit card payment*********************
if(strtoupper($payment_type) == 'CREDITCARD'){
$expiry = explode("/",$expiry_date);
$expiry_month = $expiry[0];
$expiry_year = $expiry[1];

$payment_status = pay_via_card($full_name,$total_amount*100,$creditcard_no,$verification_code,$expiry_month,$expiry_year);
if(!$payment_status->errors ){
$_status = $payment_status->transactions[0]->status;
$wc_order = add_wc_order($firstname,$lastname,$email,$phone_no,$address,$city,$zip_code,$items_details,$remarks,$_status);
} else {
    $_status = "error found";
}

$update_data = array(
    "payment_provider_response" => json_encode($payment_status),
    'updated_at' => date('Y-m-d h:i:s'),
    'payment_status' => $_status,
    'payment_order_id' => $payment_status->orderId,
    'order_post_id' => $wc_order

    );
$where = array(
    "id" => $lastid 
    );
$update_order = $wpdb->update($order_table, $update_data, $where);

            $order_response = $wpdb->get_results("select * from $order_table where id = $lastid");
        //   if($payment_status->errors || $payment_status->transactions[0]->status != "SUCCESS"){
           if($payment_status->errors || $payment_status->transactions[0]->status == "FAILURE"){
                            $response = array('response_status' => 'failure', 'order_details' => "payment was not successful!", "payment_error" =>$payment_status->errors[0]->message  );
           } else if($payment_status->transactions[0]->status == "SUCCESS") {
                    $response = array('response_status' => 'successful', 'order_details' => $order_response, "payment_status" => $payment_status->transactions[0]->status, "order_id" => $payment_status->orderId , "transaction_id" => $payment_status->transactions[0]->transactionId,"redirect_url" => "" );
           } else if($payment_status->transactions[0]->status == "PENDING"){
                    $response = array('response_status' => 'successful', 'order_details' => $order_response, "payment_status" => $payment_status->transactions[0]->status , "order_id" => $payment_status->orderId , "transaction_id" => $payment_status->transactions[0]->transactionId,"redirect_url" => "");
           }
} else if(strtoupper($payment_type) == 'PAYPAL'){
    $payment_status = pay_via_paypal($full_name,$total_amount*100,$creditcard_no,$verification_code,$expiry_month,$expiry_year);
if(!$payment_status->errors ){
$_status = $payment_status->transactions[0]->status;
$wc_order = add_wc_order($firstname,$lastname,$email,$phone_no,$address,$city,$zip_code,$items_details,$remarks,$_status);
} else {
    $_status = "error found";
}

$update_data = array(
    "payment_provider_response" => json_encode($payment_status),
    'updated_at' => date('Y-m-d h:i:s'),
    'payment_status' => $_status,
    'payment_order_id' => $payment_status->orderId,
    'order_post_id' => $wc_order
    );
$where = array(
    "id" => $lastid 
    );
$update_order = $wpdb->update($order_table, $update_data, $where);

            $order_response = $wpdb->get_results("select * from $order_table where id = $lastid");
        //   if($payment_status->errors || $payment_status->transactions[0]->status != "SUCCESS"){
           if($payment_status->errors || $payment_status->transactions[0]->status == "FAILURE"){
                            $response = array('response_status' => 'failure', 'order_details' => "payment was not successful!", "payment_error" =>$payment_status->errors[0]->message  );
           } else if($payment_status->transactions[0]->status == "SUCCESS") {
                    $response = array('response_status' => 'successful', 'order_details' => $order_response, "payment_status" => $payment_status->transactions[0]->status, "order_id" => $payment_status->orderId , "transaction_id" => $payment_status->transactions[0]->transactionId );
           } else if($payment_status->transactions[0]->status == "PENDING"){
                    $response = array('response_status' => 'successful', 'order_details' => $order_response, "payment_status" => $payment_status->transactions[0]->status , "order_id" => $payment_status->orderId , "transaction_id" => $payment_status->transactions[0]->transactionId);
           }
                        $response = array('response_status' => 'successful', 'order_details' => $order_response, "payment_status" => $payment_status->transactions[0]->status , "order_id" => $payment_status->orderId , "transaction_id" => $payment_status->transactions[0]->transactionId,
                        "redirect_url" => $payment_status->redirectUrl);
    
}else if(strtoupper($payment_type) == 'CASH ON DELIVERY'){
                $order_response = $wpdb->get_results("select * from $order_table where id = $lastid");
                                $response = array('response_status' => 'successful', 'order_details' => $order_response, "payment_status" => "You have to pay bill upon food delivery/pickup"  );
} else {
     $response = array('response_status' => 'failure', 'description' =>  "Provided payment method is not valid"  );
}


return $response;


}



return $response;

}


function pay_via_card($full_name,$initial_amount,$card_no,$ver_code,$expiry_month,$expiry_year)
{
    
    
    $post_data = array("initialAmount" => (int)$initial_amount,
    "currency"=>"EUR",
    "product" => "creditcard",
    "async" => ["successUrl" => "http://test.mantra-dachau.com/",
    "failureUrl" => "http://test.mantra-dachau.com/",
    "cancelUrl"=> "http://test.mantra-dachau.com/",
    "notifications" => [["notificationUrn"=> "","notificationState"=> ["CREATED"]]]],
    "payment"=> ["cardNumber"=> $card_no,
    "verification"=> "$ver_code",
    "expiryMonth"=> $expiry_month,
    "expiryYear"=> $expiry_year,
    "cardHolder"=> $full_name]
        );
  
   
    $post_fields = json_encode($post_data);
    $scurity_token = base64_encode(MERCHANT_ID.':'.API_PRIVATE_KEY);

    
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => API_PATH."/orders/debit",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => " ",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $post_fields,
      CURLOPT_HTTPHEADER => array(
        "authorization: Basic $scurity_token",
        "cache-control: no-cache",
        "content-type: application/json",
          ),
        ));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  return json_decode("cURL Error #:" . $err);
} else {
  return json_decode($response);
}
}

// --------------------------paypal payment-----------------
function pay_via_paypal($full_name,$initial_amount,$card_no,$ver_code,$expiry_month,$expiry_year)
{
    $home_url = get_home_url();
 
    
    $post_data = array("initialAmount" => (int)$initial_amount,
    "currency"=>"EUR",
    "product" => "paypal",
    "async" => ["successUrl" => $home_url."/payment-successful/",
    "failureUrl" => $home_url."/payment-failure/",
    "cancelUrl"=> $home_url."/payment-cancelled/",
    "notifications" => [["notificationUrn"=> "","notificationState"=> ["CREATED"]]]],

        );
  

    $post_fields = json_encode($post_data);
$scurity_token = base64_encode(MERCHANT_ID.':'.API_PRIVATE_KEY);

    
      $curl = curl_init();
      curl_setopt_array($curl, array(
      CURLOPT_URL => API_PATH."/orders/debit",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => " ",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $post_fields,
      CURLOPT_HTTPHEADER => array(
        "authorization: Basic $scurity_token",
        "cache-control: no-cache",
        "content-type: application/json",

          ),
        ));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  return json_decode("cURL Error #:" . $err);
} else {
  return json_decode($response);
}
}




// --------------------------my_orders-----------------

add_filter("wcra_my_orders_callback" , "wcra_my_orders_callback_handler");
function wcra_my_orders_callback_handler($param){
  global $wpdb;
  
        $user_table =  $wpdb->prefix.'app_users';
        $order_table = $wpdb->prefix.'submitted_orders'; 
        
           if(empty($param['email']) || empty($param['api_password']) ){
        $response = array('response_status' => 'failure', 'details' => "Provided Parameter list is not complete!" );
        return $response;
    } 
    
        $username  = $param['email'];
    $api_password  = $param['api_password'];
    
    $res = $wpdb->get_results("select * from $user_table where user_login  = '$username'and api_password  = '$api_password' ");

    if(count($res) > 0){
            $users_orders =$wpdb->get_results("select * from $order_table where email = '$username'");
    $response = array('response_status' => 'successful', 'orders' => $users_orders );
    }else {
    $response = array('response_status' => 'failure', 'details' => 'Invalid credentials!' ); 
    }
return $response;
}
// ------------------- social_media_login -------------------
add_filter("wcra_social_media_login_callback" , "wcra_social_media_login_callback_handler");
function wcra_social_media_login_callback_handler($param){
    global $wpdb;
    $table =  $wpdb->prefix.'app_users';
    $table_meta = $wpdb->prefix.'app_usermeta';
    
    if(empty($param['email']) || empty($param['full_name'])){
        $response = array('response_status' => 'failure', 'details' => "Provided Parameter list is not complete!" );
        return $response;
    }
    if(empty($param['fb_token']) && empty($param['google_token'])){
        $response = array('response_status' => 'failure', 'details' => "Facebook token or Google token must be provided!" );
        return $response;
    }
//    *********************check availability of provided email**********************************
    $user_name = $param['email'];
    $fb_token = $param['fb_token'];
    $g_token = $param['google_token'];
        $email_exists = $wpdb->get_results("select * from $table where user_login  = '$user_name' ");
        
    if(count($email_exists) > 0){
        if(isset($fb_token)){
            if($email_exists[0]->fb_token != ""){ 
                $acc_exists = $wpdb->get_results("select * from $table where user_login  = '$user_name' and fb_token = '$fb_token' ");
                $acc_count = count($acc_exists);    
            } else {
                     $update_fb_t = $wpdb->update($table,array('fb_token' => $fb_token),array('user_login' => $user_name));
                        if($update_fb_t){
                            $acc_count = 1;
                           }else{
                               $acc_count = 0;
                           }        
                    }
            } else if(isset($g_token)){
                if($email_exists[0]->google_token != ""){
                $acc_exists = $wpdb->get_results("select * from $table where user_login  = '$user_name' and google_token = '$g_token' ");
                $acc_count = count($acc_exists);
                } else {
                   $update_g_t = $wpdb->update($table,array('google_token' => $g_token),array('user_login' => $user_name));
                   if($update_g_t){
                    $acc_count = 1;
                   }else{
                       $acc_count = 0;
                   }
                }
            }
   $user_details[] = array(

                "user_email"=> $email_exists[0]->user_login,
                "api_password"=> $email_exists[0]->api_password,
                "user_full_name"=> $email_exists[0]->user_nicename,
                "user_address"=> "",
                "email_verified" => true,
                // "user_registration_date"=> $acc_exists[0]->user_registered

       );
   
   
    if($acc_count > 0){
        $response = array('response_status' => 'successfull', 'user' => $user_details );
        return $response; // stop execution
    } else {
                $response = array('response_status' => 'failure', 'details' => "No active account found against provided details!" );
        return $response; // stop execution
    } 
    
        
    }else {
    $user_password = wp_generate_password( 10, false );
    $api_password = wp_generate_password( 33, false );
    $hash = password_hash($user_password, PASSWORD_DEFAULT);
    $data = array(
        "user_login" => $param['email'],
        "user_pass" => $hash ,
        "user_nicename" => $param['full_name'],
        "user_email" => $param['email'],
        "user_url" => "",
        "user_registered" => date('Y-m-d H:i:s'),
        "api_password" => "$api_password",
        "user_status" => "0",
        "display_name" => "",
        "fb_token" => $param['fb_token'],
        "google_token" => $param['google_token']

    );

$res = $wpdb->insert($table, $data);
$lastid = $wpdb->insert_id;
$data_mata = array('user_id' => $lastid, 'meta_key' => 'user_address', 'meta_value' => "" );
$res2 = $wpdb->insert($table_meta, $data_mata);
    
if($res){
        $user_details[] = array(
            "user_email" => $param['email'],
            "api_password" => $api_password,
            "user_full_name" => $param['full_name'],
            "user_address" => "",
            "email_verified" => true
            // "user_registration_date" => $res[0]->user_registered
        );
}
}
$response = array('response_status' => 'successful', 'details' => 'User has been registered successfully', 'user' => $user_details );

return $response;
}

// --------------------- get_discount_details-------------------
add_filter("wcra_get_discount_details_callback" , "wcra_get_discount_details_callback_handler");
function wcra_get_discount_details_callback_handler($param){
  global $wpdb;
  
        $discount_table = $wpdb->prefix.'discounts'; 
        $discount_rate = $wpdb->get_results("select * from $discount_table where is_active = 1");
if(count($discount_rate) > 0){
            $discount_  = (float)$discount_rate[0]->discount_rate;
} else{
        $discount_  = (float)"0";
    }
        $response = array('response_status' => 'successful',  'discount_percentage' => $discount_ );
return $response;
} //----- end of discount api -----------



// ----------------- misc --------------------------------------
add_filter("wcra_misc_callback" , "wcra_misc_callback_handler");
function wcra_misc_callback_handler($param){


echo API_PATH."<br>";
echo MERCHANT_ID."<br>";
echo API_PRIVATE_KEY."<br>";
$scurity_token = base64_encode(MERCHANT_ID.':'.API_PRIVATE_KEY);
echo $scurity_token;
die;

 
   
}
// ******* end of misc ******

function add_wc_order($first_name,$last_name,$email,$phone,$address,$city,$postcode,$items_details,$notes,$_status){
  global $woocommerce;

  $address = array(
      'first_name' => $first_name,
      'last_name'  => $last_name,
      'company'    => "",
      'email'      => $email,
      'phone'      => $phone,
      'address_1'  => $address,
      'address_2'  => '',
      'city'       => $city,
      'state'      => "",
      'postcode'   => $postcode,
      'country'    => 'DE'
  );
$someJSON = $items_details;
$item_ids=array();
$item_quantities = array();

  $ordered_items = json_decode($someJSON, true);

  foreach($ordered_items as $id_key => $id_val){
      $product_ids[]=$id_val['object_id'];
      $quantities[] = $id_val['quantity'];
  }


  $order = wc_create_order();
 foreach($product_ids as $key=>$value){
 $order->add_product( get_product($value), $quantities[$key] );
}

$productid = $post->ID;

  $order->set_address( $address, 'billing' );
  
  if($_status == "SUCCESS"){
      $order_status = 'processing';
  } 
  else if($_status == "FAILURE"){
      $order_status = 'failed';
  } 
  else if($_status == "PENDING"){
      $order_status = 'pending payment';
  }

  
$order->update_status( $order_status );
// $order->update_status( 'failure' );
$note = __($notes);
$order->add_order_note( $productid );

return $order->get_id();

}




// ---------------- get_payment_status -------------------------------
add_filter("wcra_get_payment_status_callback" , "wcra_get_payment_status_callback_handler");
function wcra_get_payment_status_callback_handler($param){
 global $wpdb;
 $order_table =  $wpdb->prefix.'submitted_orders';
    
$order_id = $param['order_id'];
$transaction_id = $param['transaction_id'];

    if(empty($param['order_id']) || empty($param['transaction_id']) ){
        $response = array('response_status' => 'failure', 'details' => "Provided Parameter list is not complete!" );
        return $response;
    }
$get_order_post_id = $wpdb->get_results("select order_post_id from $order_table where payment_order_id  = '$order_id' ");
$order_post_id = $get_order_post_id[0]->order_post_id;
$order = new WC_Order($order_post_id);
$scurity_token = base64_encode(MERCHANT_ID.':'.API_PRIVATE_KEY);


$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => API_PATH."/orders/".$order_id."/transactions/".$transaction_id,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic $scurity_token",
    "cache-control: no-cache",
    "content-type: application/json",
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  $res = json_decode("cURL Error #:" . $err);
} else {
  $res = json_decode($response);
  if (!empty($order)) {
      $_status = $res->status;
        if($_status == "SUCCESS"){
      $order_status = 'processing';
  } 
  else if($_status == "FAILURE"){
      $order_status = 'failed';
  } 
  else if($_status == "PENDING"){
      $order_status = 'pending payment';
  }
  else if($_status == "ABORTED"){
      $order_status = 'cancelled';
  }

$order->update_status( $order_status );
      $payment_status = $res->status;
}
}
$response = array('response_status' => 'successful', "payment_status" => $res->status , "order_id" => $res->order->orderId , "transaction_id" => $res->transactionId);

return $response;
}

// ----------------------
wcra_run_customwprest();




