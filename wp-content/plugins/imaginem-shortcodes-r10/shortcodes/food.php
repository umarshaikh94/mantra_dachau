<?php
if (!function_exists('imaginem_codepack_food_label')) {
    function imaginem_codepack_food_label( $label ) {
        $got_label = '';
        switch ($label) {
            case 'food_chef_recommended':
                $got_label = imaginem_codepack_get_option_data('food_chef_recommended');
                break;
            case 'food_new_item':
                $got_label = imaginem_codepack_get_option_data('food_new_item');
                break;
            case 'food_order_item':
                $got_label = imaginem_codepack_get_option_data('food_order_item');
                break;
            
            default:
                # code...
                break;
        }
        return $got_label;
    }
}
if (!function_exists('imaginem_codepack_display_custom_food_labels')) {
    function imaginem_codepack_display_custom_food_labels( $the_page_id ) {

        $labels     = '';
        $max_labels = 10;
        if ( function_exists('cinnamon_get_max_food_labels') ) {
            $max_labels = cinnamon_get_max_food_labels();
        }
        $custom = get_post_custom( $the_page_id );
        for ( $foodlabel=1; $foodlabel <= $max_labels; $foodlabel++ ) {
                            
            $foodlabel_name = cinnamon_restaurant_get_option_data( 'foodlabel-'.$foodlabel );
            if ( '' !== $foodlabel_name) {
                if ( isset( $custom[ 'foodlabel-'.$foodlabel ][0] ) ) {
                    if ( 'yes' === $custom[ 'foodlabel-'.$foodlabel ][0] ) {
                        $labels .= '<div class="food-notice food-custom-label food-custom-label-'. esc_attr( strtolower( $foodlabel_name ) ) .'">'. $foodlabel_name .'</div>';
                    }
                }
            }
        }
        return $labels;
    }
   
}
if (!function_exists('imaginem_codepack_food_price_variations')) {
    function imaginem_codepack_get_food_prices( $id ) {
        
        $output = false;
        $variation_price = '';
        $single_price = '';
        $custom = get_post_custom( $id );
        $repeat_count = 0;
        $currency = imaginem_codepack_get_option_data('food_currency');
        $currency_position = imaginem_codepack_get_option_data('food_currency_position');

        $currency_tag = '<span class="food-currency-symbol">'.$currency.'</span>';

        if (isset($custom['pagemeta_price_food'][0])) {
            $pagemeta_price_food = $custom['pagemeta_price_food'][0];
            $single_price = '<div class="food-item-price">';
            if ($currency_position=="right") {
                $single_price .= $pagemeta_price_food . $currency_tag;
            } else {
                $single_price .= $currency_tag . $pagemeta_price_food;
            }
            $single_price .= '</div>';
        }

        if (isSet($custom['pagemeta_price_repeat'][0])) {
            $pagemeta_price_repeat = $custom['pagemeta_price_repeat'][0];
            if ( !is_array($pagemeta_price_repeat) ) {
                $pagemeta_price_repeat = unserialize($pagemeta_price_repeat);
            }
            if (isSet($pagemeta_price_repeat) && is_array($pagemeta_price_repeat) ) {
                //print_r($pagemeta_price_repeat);
                $variation_price = '<div class="food-item-variations">';
                foreach ($pagemeta_price_repeat['size'] as $value) {
                    if (isSet($value) && $value<>"") {
                        $size = '';
                        $price = '';
                        if ( isSet( $pagemeta_price_repeat['size'][$repeat_count] ) ) {
                            $size = $pagemeta_price_repeat['size'][$repeat_count];
                        }
                        if ( isSet( $pagemeta_price_repeat['price'][$repeat_count] ) ) {
                            $price = $pagemeta_price_repeat['price'][$repeat_count];
                        }
                        $variation_price .= '<div class="food-item-price food-variation-wrap">';
                        if ($currency_position=="right") {
                            $variation_price .= '<span class="food-variation food-variation-size">' . $size . '</span><span class="food-variation food-variation-price">' . $price . $currency_tag . '<span class="food-varitation-sep">|</span></span>';
                        } else {
                            $variation_price .= '<span class="food-variation food-variation-size">' . $size . '</span><span class="food-variation food-variation-price">' . $currency_tag . $price . '<span class="food-varitation-sep">|</span></span>';
                        }
                        $variation_price .= '</div>';
                        $repeat_count++;
                    }
                }
                $variation_price .= '</div>';
            }
        }
        $output = '<div class="food-item-variations-wrap clearfix">'.$variation_price . $single_price.'</div>';
        return $output;
    }
}
/**
 * Portfolio Grid
 */
if (!function_exists('mFoodGrid')) {
    function mFoodGrid($atts, $content = null) {
        extract(shortcode_atts(array(
            "pageid" => '',
            "foodtype_slugs" => '',
            "grid_post_type" => '',
            "category_display" => "true",
            "grid_tax_type" => '',
            "format" => '',
            "columns" => '4',
            "limit" => '-1',
            "gutter" => 'spaced',
            "boxtitle" => 'false',
            "title" => 'true',
            "desc" => 'true',
            "worktype_slugs" => '',
            "pagination" => 'false',
            "type" => 'filter'
        ), $atts));


        $currency = '$';
        
        $output = '';
        
        // Set a default
        $column_type         = "four";
        $portfolioImage_type = "cinnamon-restaurant-image-square-big";
        
        if ($columns == 4) {
            $column_type         = "four";
            $portfolioImage_type = "cinnamon-restaurant-image-square-big";
        }
        if ($columns == 3) {
            $column_type         = "three";
            $portfolioImage_type = "cinnamon-restaurant-image-square-big";
        }
        if ($columns == 2) {
            $column_type         = "two";
            $portfolioImage_type = "cinnamon-restaurant-image-square-big";
        }
        if ($columns == 1) {
            $column_type         = "one";
            $portfolioImage_type = "cinnamon-restaurant-image-full";
        }
        
        if ($format == "portrait") {
            if ($columns == 4) {
                $portfolioImage_type = "cinnamon-restaurant-image-large-portrait";
            }
            if ($columns == 3) {
                $portfolioImage_type = "cinnamon-restaurant-image-large-portrait";
            }
            if ($columns == 2) {
                $portfolioImage_type = "cinnamon-restaurant-image-large-portrait";
            }
            if ($columns == 1) {
                $portfolioImage_type = "cinnamon-restaurant-image-full";
            }
        }
        $gridblock_is_masonary = "";
        if ($format == "masonary") {
            
            $gridblock_is_masonary = "gridblock-masonary ";
            if ($columns == 4) {
                $portfolioImage_type = "cinnamon-restaurant-image-full-medium";
            }
            if ($columns == 3) {
                $portfolioImage_type = "cinnamon-restaurant-image-full-medium";
            }
            if ($columns == 2) {
                $portfolioImage_type = "cinnamon-restaurant-image-full-medium";
            }
            if ($columns == 1) {
                $portfolioImage_type = "cinnamon-restaurant-image-full";
            }
        }
        
        if ($format == "portrait") {
            $protected_placeholder = '/images/blank-grid-portrait.png';
        } else {
            $protected_placeholder = '/images/blank-grid.png';
        }
        $thumbnail_gutter_class = 'portfolio-gutter-' . $gutter . ' ';
        if ($gutter == "nospace") {
            $thumbnail_gutter_class .= 'thumnails-gutter-active ';
        }
        $flag_new_row = true;
        
        
        $output .= '<div id="gridblock-container" class="' . $thumbnail_gutter_class . $gridblock_is_masonary . 'gridblock-' . $column_type . ' foodgridblock clearfix" data-columns="' . $columns . '">';
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        
        $count           = 0;
        $terms           = array();
        $work_slug_array = array();
        //echo $worktype_slugs;
        if ($foodtype_slugs != "") {
            $type_explode = explode(",", $foodtype_slugs);
            foreach ($type_explode as $work_slug) {
                $terms[] = $work_slug;
            }
            query_posts(array(
                'post_type' => 'mtheme_food',
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'paged' => $paged,
                'posts_per_page' => $limit,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'foodcategory',
                        'field' => 'slug',
                        'terms' => $terms,
                        'operator' => 'IN'
                    )
                )
            ));
        } else {
            query_posts(array(
                'post_type' => 'mtheme_food',
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'paged' => $paged,
                'posts_per_page' => $limit
            ));
        }

        $idCount               = 1;
        $portfolio_count       = 0;
        $portfolio_total_count = 0;
        $portfoliofilters      = array();
        
        if (have_posts()):
            while (have_posts()):
                the_post();
                //echo $type, $portfolio_type;
                //
                if ( has_post_thumbnail() ) {

                    $custom                 = get_post_custom(get_the_ID());
                    $portfolio_cats         = get_the_terms(get_the_ID(), 'types');
                    $lightboxvideo          = "";
                    $thumbnail              = "";
                    $customlink_URL         = "";
                    $description            = "";
                    $portfolio_link_type    = "";
                    $portfolio_thumb_header = "Image";
                    $the_only_link          = false;
                    $the_protected_link     = false;

                    $pagemeta_price_food = '';
                    $pagemeta_purchase_food = '';
                    $pagemeta_new_food = 'no';
                    $pagemeta_chef_food = 'no';
                    $pagemeta_hide_food = 'no';

                    if (isset($custom['pagemeta_price_food'][0])) {
                        $pagemeta_price_food = $custom['pagemeta_price_food'][0];
                    }
                    if (isset($custom['pagemeta_new_food'][0])) {
                        $pagemeta_new_food = $custom['pagemeta_new_food'][0];
                    }
                    if (isset($custom['pagemeta_price_food'][0])) {
                        $pagemeta_price_food = $custom['pagemeta_price_food'][0];
                    }
                    if (isset($custom['pagemeta_purchase_food'][0])) {
                        $pagemeta_purchase_food = $custom['pagemeta_purchase_food'][0];
                    }
                    if (isset($custom['pagemeta_chef_food'][0])) {
                        $pagemeta_chef_food = $custom['pagemeta_chef_food'][0];
                    }
                    if (isset($custom['pagemeta_hide_food'][0])) {
                        $pagemeta_hide_food = $custom['pagemeta_hide_food'][0];
                    }

                    
                    if ($portfolio_count == $columns)
                        $portfolio_count = 0;
                    
                    $add_space_class = '';
                    if ($gutter != 'nospace') {
                        if ($title == 'false' && $desc == 'false') {
                            $add_space_class = 'gridblock-cell-bottom-space';
                        }
                    }
                    
                    $protected  = "";
                    $icon_class = "column-gridblock-icon";
                    $portfolio_count++;
                    $portfolio_total_count++;
                    
                    $gridblock_ajax_class = '';
                    if ($type == 'ajax') {
                        $gridblock_ajax_class = "gridblock-ajax ";
                    }

                    if ($pagemeta_hide_food<>"yes") {
                    
                    // Generate main DIV tag with portfolio information with filterable tags
                    $output .= '<div class="gridblock-element foodgrid-element food-is-chef-recommended-'.$pagemeta_chef_food.' isotope-displayed gridblock-element-id-' . get_the_ID() . ' gridblock-element-order-' . $portfolio_total_count . ' ' . $add_space_class . ' gridblock-filterable ';
                    if (is_array($portfolio_cats)) {
                        foreach ($portfolio_cats as $taxonomy) {
                            $output .= 'filter-' . $taxonomy->slug . ' ';
                            if ($pagination == 'true') {
                                if (in_array($taxonomy->slug, $portfoliofilters)) {
                                } else {
                                    $portfoliofilters[] = $taxonomy->slug;
                                }
                            }
                        }
                    }
                    $idCount++;
                    $output .= '" data-portfolio="portfolio-' . get_the_ID() . '" data-id="id-' . $idCount . '">';
                    $output .= '<div class="' . $gridblock_ajax_class . 'gridblock-grid-element gridblock-element-inner" data-portfolioid="' . get_the_id() . '">';
                    if ($pagemeta_chef_food=="yes") {
                        $output .= '<div class="food-notice food-chef-recommended">'.imaginem_codepack_food_label('food_chef_recommended').'</div>';
                    }
                    
                    $output .= '<div class="food-grid-custom-label-wrap">';
                    $output .= imaginem_codepack_display_custom_food_labels( get_the_id() );
                    $output .= '</div>';

                    if ($pagemeta_new_food=="yes") {
                        $output .= '<div class="food-notice food-new-item">'.imaginem_codepack_food_label('food_new_item').'</div>';
                    }
                    if ($pagemeta_purchase_food<>"") {
                        $output .= '<div class="food-order food-purchase-item"><a href="'.esc_url($pagemeta_purchase_food).'">'.imaginem_codepack_food_label('food_order_item').'</a></div>';
                    }
                    $output .= '<div class="gridblock-background-hover">';
                    
                    if ($portfolio_link_type == "Lightbox_DirectURL") {
                        $output .= '<a class="column-gridblock-icon" href="' . get_permalink() . '">';
                        $output .= '<span class="hover-icon-effect"><i class="' . imaginem_codepack_get_portfolio_icon('directlink') . '"></i></span>';
                        $output .= '</a>';
                    }
                    
                    $output .= imaginem_codepack_activate_lightbox($lightbox_type = "default", $ID = get_the_id(), $predefined = imaginem_codepack_featured_image_link(get_the_ID()), $mediatype = "image", $imagetitle = imaginem_codepack_image_title(get_the_ID()), $class = "gridblock-sole-link column-gridblock-lightbox lightbox-image", $set = "portfolio-grid", $data_name = "default");
                    $icon_class = '<i class="' . imaginem_codepack_get_portfolio_icon('lightbox') . '"></i>';

                    if ($boxtitle<>"true") {
                        $output .= '<div class="gridblock-links-wrap">';
                        if (isSet($icon_class)) {
                            $output .= '<span class="column-gridblock-icon">';
                            $output .= '<span class="hover-icon-effect">' . $icon_class . '</span>';
                            $output .= '</span>';
                        }
                        $output .= '</div>';
                    }

                    if ($boxtitle == "true") {
                        $output .= '<div class="boxtitle-hover">';
                        $output .= get_the_title();
                        $output .= '<div class="box-food-price">'. imaginem_codepack_get_food_prices( get_the_ID() ) .'</div>';
                        $output .= '</div>';
                    }

                    $output .= '</a>';

                    $output .= '</div>';
                    
                    $fade_in_class = "";
                    
                    $output .= imaginem_codepack_display_post_image(get_the_ID(), $have_image_url = "", $link = false, $imagetype = $portfolioImage_type, $imagetitle = imaginem_codepack_image_title(get_the_ID()), $class = $fade_in_class . "displayed-image");

                    $output .= '</div>';
                    if ($boxtitle<>"true") {
                        if ($title == 'true' || $desc == 'true') {
                            $output .= '<div class="food-item-wrap">';
                                $output .= '<div class="food-item-header clearfix">';
                                    $output .= '<h3 class="food-item-title">'.get_the_title().'</h3>';
                                $output .= '</div>';
                                $output .= imaginem_codepack_get_food_prices( get_the_ID() );
                                $output .= '<div class="food-item-description">'.get_the_excerpt().'</div>';

                            $output .= '</div>';
                        }
                    }
                    
                    
                    $output .= '</div>';

                    }

                }
            endwhile;
        endif;
        $output .= '</div>';
        
        if ($pagination == 'true') {
            $output .= '<div class="clearfix">';
            $output .= imaginem_codepack_pagination();
            $output .= '</div>';
        }
        
        wp_reset_query();

        return $output;
    }
}
add_shortcode("foodgrid", "mFoodGrid");
/**
 * Food List
 */
if (!function_exists('mFoodList')) {
    function mFoodList($atts, $content = null) {
        extract(shortcode_atts(array(
            "pageid" => '',
            "foodtype_slugs" => '',
            "background_opacity" => '',
            "background_color" => '',
            "padding" => '',
            "button_url" => '',
            "button_text" => '',
            "columns" => '1',
            "title" => '',
            "limit" => '-1',
            "animated" => 'true',
            "item_image" => 'no',
        ), $atts));
        
        $currency = '$';

        if ($animated<>"false") {
            $animated = "true";
        }
        
        $output = '';
        $background_style_tag = '';
        $padding_style_tag = '';

        $uniqueID = uniqid();

        $bg_opacity = 1;

        if ($background_color<>"") {
            $bg_color = mtheme_shortcodefunction_hex_to_rgb($background_color);
            $bg_color = $bg_color[0].','.$bg_color[1].','.$bg_color[2];
        }

        if (isSet($bg_color)) {
            $background_style_tag = 'background: rgba('.$bg_color.','.$bg_opacity.');';
        }
        if ($padding<>"") {
            $padding_style_tag = 'padding: '.$padding.'px;';
        }

        $output .= '<div style="'.$background_style_tag.$padding_style_tag.'" id="food-list-container-'.$uniqueID.'" class="food-list-wrap food-column clearfix">';
        
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }

        $count           = 0;
        if (!isSet($columns)) {
            $columns = '1';
        }
        $terms           = array();
        $work_slug_array = array();

        if ($foodtype_slugs != "") {
            $type_explode = explode(",", $foodtype_slugs);
            foreach ($type_explode as $work_slug) {
                $terms[] = $work_slug;
            }
            query_posts(array(
                'post_type' => 'mtheme_food',
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'paged' => $paged,
                'posts_per_page' => $limit,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'foodcategory',
                        'field' => 'slug',
                        'terms' => $terms,
                        'operator' => 'IN'
                    )
                )
            ));
        } else {
            query_posts(array(
                'post_type' => 'mtheme_food',
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'paged' => $paged,
                'posts_per_page' => $limit
            ));
        }
        
        $idCount               = 1;
        $food_count       = 0;
        $food_total_count = 0;
        $foodfilters      = array();

        if ($title<>"") {
		  $output .= '<h2 class="food-main-title">'.$title.'</h2>';
        }
		if ( $content<>"" ) {
			$output .= '<div class="entry-content">';
			$output .= wpautop( html_entity_decode($content) );
			$output .= '</div>';
		}


        $food_count = 1;
        $food_column = '';

        $food_column = ' food-column-one';
        $food_column_clearfix = " clearfix";

        if ($columns == '2' ) {
            $food_column = ' food-column-two';
            $food_column_clearfix = "";
        }
        if ($columns == '3' ) {
            $food_column = ' food-column-three';
            $food_column_clearfix = "";
        }

        $animation_class = '';
        if ($animated == "true") {
            $animation_class = ' animation-standby animated fadeInUpSlow';
        }

        $output .= '<ul class="food-list '.$food_column.' clearfix">';

        if (have_posts()):
            while (have_posts()):
                the_post();
                //echo $type, $food_type;
                $custom = get_post_custom(get_the_ID());
                $food_cats = get_the_terms(get_the_ID(), 'foodcategory');
                
                $protected  = "";
                $icon_class = "column-gridblock-icon";
                $food_total_count++;

				$pagemeta_price_food = '';
                $pagemeta_purchase_food = '';
                $pagemeta_purchase_food_tag = '';
				$pagemeta_new_food = 'no';
				$pagemeta_chef_food = 'no';
				$pagemeta_hide_food = 'no';

                if (isset($custom['pagemeta_price_food'][0])) {
                    $pagemeta_price_food = $custom['pagemeta_price_food'][0];
                }
                if (isset($custom['pagemeta_new_food'][0])) {
                    $pagemeta_new_food = $custom['pagemeta_new_food'][0];
                }
                if (isset($custom['pagemeta_price_food'][0])) {
                    $pagemeta_price_food = $custom['pagemeta_price_food'][0];
                }
                if (isset($custom['pagemeta_chef_food'][0])) {
                    $pagemeta_chef_food = $custom['pagemeta_chef_food'][0];
                }
                if (isset($custom['pagemeta_purchase_food'][0])) {
                    $pagemeta_purchase_food = $custom['pagemeta_purchase_food'][0];
                }
                if (isset($custom['pagemeta_hide_food'][0])) {
                    $pagemeta_hide_food = $custom['pagemeta_hide_food'][0];
                }
                $food_image = '';
                $food_image_status = false;
                $food_image_class = '';
				if ($item_image=="yes") {
					if ( has_post_thumbnail() ) {
                        $post_thumbnail_id = get_post_thumbnail_id( get_the_id() );
                        $image_data = wp_get_attachment_image_src( $post_thumbnail_id, 'cinnamon-restaurant-image-square-big', false );
                        $image_url = $image_data[0];
						$food_image = '<div class="food-item-image"><img src="'.esc_url($image_url).'" alt="food-item" /></div>';
                        $food_image_status = true;
                        $food_image_class = "food-image-active";
					}
				}

                if ($pagemeta_hide_food<>"yes") {

                    $parity = " food-column-".$food_count;

                    if ($columns == '2' ) {
                        if ($food_count == 2 ) {
                            $food_count = 0;
                        }
                    }
                    if ($columns == '3' ) {
                        if ($food_count == 3 ) {
                            $food_count = 0;
                        }
                    }

                    $food_notice = false;
                     $food_notice_tag = '';
                    if ($pagemeta_chef_food=="yes" || $pagemeta_new_food=="yes") {
                        $food_notice = true;
                        $food_notice_tag = " food-notice-active";
                    }

	                $output .= '<li class="food-element food-element-id-' . get_the_ID() . ' '.$food_image_class.$food_notice_tag.$animation_class.$parity.$food_column_clearfix.' food-element-order-' . $food_total_count . '" data-food="food-' . get_the_ID() . '">';
                        $output .= '<div class="food-item-wrap-outer">';
						$output .= $food_image;
                        $food_count++;
						$output .= '<div class="food-item-wrap">';
							if ($pagemeta_chef_food=="yes") {
								$output .= '<div class="food-notice food-chef-recommended">'.imaginem_codepack_food_label('food_chef_recommended').'</div>';
							}
                            if ($pagemeta_new_food=="yes") {
                                $output .= '<div class="food-notice food-new-item">'.imaginem_codepack_food_label('food_new_item').'</div>';
                            }
                            $output .= imaginem_codepack_display_custom_food_labels( get_the_id() );
                            if ($pagemeta_purchase_food<>"") {
                                $output .= '<div class="food-order food-purchase-item"><a href="'.esc_url($pagemeta_purchase_food).'">'.imaginem_codepack_food_label('food_order_item').'</a></div>';
                            }
							$output .= '<div class="food-item-header clearfix">';
							
                            $output .= '<h3 class="food-item-title">'.get_the_title().'</h3>';
                            $output .= imaginem_codepack_get_food_prices( get_the_ID() );
							
							$output .= '</div>';
							$output .= '<div class="food-item-description">'.get_the_excerpt().'</div>';

                        $output .= '</div>';
						$output .= '</div>';
	                $output .= '</li>';
            	}
				// $output .= $pagemeta_new_food;
				// $output .= $pagemeta_price_food;
				// $output .= $pagemeta_chef_food;
				// $output .= $pagemeta_hide_food;
            endwhile;
        endif;
        $output .= '</ul>';

        if (!empty($button_text)) {
        $output .= '
            <div class="food-menu-button">
                <a class="button-element button-default-outline" href="'.esc_url($button_url).'">
                '.$button_text.'
                </a>
            </div>';
        }

        $output .= '</div>';
        
        wp_reset_query();
        return $output;
    }
}
add_shortcode("foodlist", "mFoodList");
/**
 * Food Tabs
 */
if (!function_exists('mFoodTabs')) {
    function mFoodTabs($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "pageid" => '',
            "foodtype_slugs" => '',
            "columns" => '1',
            "title" => '',
            "limit" => '-1',
            "animated" => 'true',
            "item_image" => 'no',
        ), $atts));
        
        $currency = '$';
        
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        
        $count           = 0;
        if (!isSet($columns)) {
            $columns = '1';
        }
        $terms           = array();
        $work_slug_array = array();
        $tab = array();
        $tabs = '';
        $output = '';
        $count_food_cats=0;

        if ($foodtype_slugs=="" || !isSet($foodtype_slugs) ) {

            $get_food_terms = get_terms('foodcategory');
            $foodtype_slugs = '';

            foreach($get_food_terms as $key => $list) {
                if (isSet($list->slug)) {
                    $count_food_cats++;
                    if ($count_food_cats>1) {
                        $foodtype_slugs .= ',';
                    }
                    $foodtype_slugs .= $list->slug;
                }
            }
        }
        if ($foodtype_slugs != "") {
            $type_explode = explode(",", $foodtype_slugs);
            foreach ($type_explode as $work_slug) {

                $term_title= '';
                $curr_term = get_term_by('slug',$work_slug, 'foodcategory');
                $term_title = $curr_term->name;

                query_posts(array(
                    'post_type' => 'mtheme_food',
                    'orderby' => 'menu_order',
                    'order' => 'ASC',
                    'paged' => $paged,
                    'posts_per_page' => $limit,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'foodcategory',
                            'field' => 'slug',
                            'terms' => $work_slug,
                            'operator' => 'IN'
                        )
                    )
                ));

                $food_total_count = 0;
                $food_count = 1;
                $food_column = '';

                $food_column = ' food-column-one';
                $food_column_clearfix = " clearfix";

                if ($columns == '2' ) {
                    $food_column = ' food-column-two';
                    $food_column_clearfix = "";
                }
                if ($columns == '3' ) {
                    $food_column = ' food-column-three';
                    $food_column_clearfix = "";
                }

                $output = '';
                $tab_shortcode_start ='[tabs type="horizontal"]';
                $tab_shortcode_end ='[/tabs]';

                $uniqueID = uniqid();

                $output .= '<div id="food-tabs-container-'.$uniqueID.'" class="food-tabs-wrap food-column clearfix">';
                $output .= '<ul class="food-list '.$food_column.' clearfix">';

                if (have_posts()):
                            while (have_posts()):
                                the_post();
                                //echo $type, $food_type;
                                $custom = get_post_custom(get_the_ID());
                                $food_cats = get_the_terms(get_the_ID(), 'foodcategory');
                                
                                $protected  = "";
                                $icon_class = "column-gridblock-icon";
                                $food_total_count++;
                                
                                $animation_class = '';
                                if ($animated == "true") {
                                    $animation_class = ' animation-standby-food animated fadeInUpSlow';
                                }

                                $pagemeta_price_food = '';
                                $pagemeta_new_food = 'no';
                                $pagemeta_chef_food = 'no';
                                $pagemeta_hide_food = 'no';

                                if (isset($custom['pagemeta_price_food'][0])) {
                                    $pagemeta_price_food = $custom['pagemeta_price_food'][0];
                                }
                                if (isset($custom['pagemeta_new_food'][0])) {
                                    $pagemeta_new_food = $custom['pagemeta_new_food'][0];
                                }
                                if (isset($custom['pagemeta_price_food'][0])) {
                                    $pagemeta_price_food = $custom['pagemeta_price_food'][0];
                                }
                                if (isset($custom['pagemeta_chef_food'][0])) {
                                    $pagemeta_chef_food = $custom['pagemeta_chef_food'][0];
                                }
                                if (isset($custom['pagemeta_hide_food'][0])) {
                                    $pagemeta_hide_food = $custom['pagemeta_hide_food'][0];
                                }
                                $food_image = '';
                                $food_image_status = false;
                                $food_image_class = '';
                                if ($item_image=="yes") {
                                    if ( has_post_thumbnail() ) {
                                        $post_thumbnail_id = get_post_thumbnail_id( get_the_id() );
                                        $image_data = wp_get_attachment_image_src( $post_thumbnail_id, 'cinnamon-restaurant-image-square-big', false );
                                        $image_url = $image_data[0];
                                        $food_image = '<div class="food-item-image"><img src="'.esc_url($image_url).'" alt="food-item" /></div>';
                                        $food_image_status = true;
                                        $food_image_class = "food-image-active";
                                    }
                                }

                                if ($pagemeta_hide_food<>"yes") {

                                    $parity = " food-column-".$food_count;

                                    if ($columns == '2' ) {
                                        if ($food_count == 2 ) {
                                            $food_count = 0;
                                        }
                                    }
                                    if ($columns == '3' ) {
                                        if ($food_count == 3 ) {
                                            $food_count = 0;
                                        }
                                    }

                                    $output .= '<li class="food-element food-element-id-' . get_the_ID() . ' '.$food_image_class.$parity.$food_column_clearfix.' food-element-order-' . $food_total_count . '" data-food="food-' . get_the_ID() . '">';
                                        $output .= '<div class="food-item-wrap-outer">';
                                        $output .= $food_image;
                                        $food_count++;
                                        $output .= '<div class="food-item-wrap">';
                                            if ($pagemeta_chef_food=="yes") {
                                                $output .= '<div class="food-notice food-chef-recommended">'.imaginem_codepack_food_label('food_chef_recommended').'</div>';
                                            }
                                            if ($pagemeta_new_food=="yes") {
                                                $output .= '<div class="food-notice food-new-item">'.imaginem_codepack_food_label('food_new_item').'</div>';
                                            }
                                            $output .= imaginem_codepack_display_custom_food_labels( get_the_id() );
                                            $output .= '<div class="food-item-header clearfix">';
                                                $output .= '<h3 class="food-item-title">'.get_the_title().'</h3>';
                                                $output .= imaginem_codepack_get_food_prices( get_the_ID() );
                                            
                                            $output .= '</div>';
                                            $output .= '<div class="food-item-description">'.get_the_excerpt().'</div>';

                                        $output .= '</div>';
                                        $output .= '</div>';
                                    $output .= '</li>';
                                }
                            endwhile;
                        endif;

                        wp_reset_query();

                        $output .= '</ul>';
                        $output .= '</div>';
                        //$output = trim(preg_replace('/\s\s+/', ' ', $output));
                        //$output = preg_replace('/<p\b[^>]*>(.*?)<\/p>/i', '', $output);
                        //$output = str_replace(array("\n\r", "\n", "\r"), '', $output );
                        
                        $tabs .= '[tab title="'.esc_attr($term_title).'"]'.$output.'[/tab]';
            }
        }

        $tab_shortcode = '<div class="food-tabs-outer">';
        $tab_shortcode .= $tab_shortcode_start . $tabs . $tab_shortcode_end;
        $tab_shortcode .= '</div>';
        // echo '<pre>';
        // print_r($tab_shortcode);
        // echo '</pre>';
        
        $idCount          = 1;
        $food_count       = 0;
        $food_total_count = 0;
        $foodfilters      = array();

        

        $output .= '<h2 class="food-main-title">'.$title.'</h2>';
        if ( $content<>"" ) {
            $output .= '<div class="entry-content">';
            $output .= wpautop( html_entity_decode($content) );
            $output .= '</div>';
        }

        $output = do_shortcode($tab_shortcode);
        return $output;
    }
}
add_shortcode("foodtabs", "mFoodTabs");/**
 * Food Accordion
 */
if (!function_exists('mFoodAccordion')) {
    function mFoodAccordion($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "pageid" => '',
            "foodtype_slugs" => '',
            "columns" => '1',
            "title" => '',
            "limit" => '-1',
            "animated" => 'true',
            "item_image" => 'no',
        ), $atts));
        
        $currency = '$';
        
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        
        $count           = 0;
        if (!isSet($columns)) {
            $columns = '1';
        }
        $terms           = array();
        $work_slug_array = array();
        $tab = array();
        $tabs = '';
        $output = '';
        $count_food_cats=0;

        if ($foodtype_slugs=="" || !isSet($foodtype_slugs) ) {

            $get_food_terms = get_terms('foodcategory');
            $foodtype_slugs = '';

            foreach($get_food_terms as $key => $list) {
                if (isSet($list->slug)) {
                    $count_food_cats++;
                    if ($count_food_cats>1) {
                        $foodtype_slugs .= ',';
                    }
                    $foodtype_slugs .= $list->slug;
                }
            }
        }
        if ($foodtype_slugs != "") {
            $type_explode = explode(",", $foodtype_slugs);
            foreach ($type_explode as $work_slug) {

                $term_title= '';
                $curr_term = get_term_by('slug',$work_slug, 'foodcategory');
                $term_title = $curr_term->name;

                query_posts(array(
                    'post_type' => 'mtheme_food',
                    'orderby' => 'menu_order',
                    'order' => 'ASC',
                    'paged' => $paged,
                    'posts_per_page' => $limit,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'foodcategory',
                            'field' => 'slug',
                            'terms' => $work_slug,
                            'operator' => 'IN'
                        )
                    )
                ));

                $food_total_count = 0;
                $food_count = 1;
                $food_column = '';

                $food_column = ' food-column-one';
                $food_column_clearfix = " clearfix";

                if ($columns == '2' ) {
                    $food_column = ' food-column-two';
                    $food_column_clearfix = "";
                }
                if ($columns == '3' ) {
                    $food_column = ' food-column-three';
                    $food_column_clearfix = "";
                }

                $output = '';
                $tab_shortcode_start ='[accordions active="-1"]';
                $tab_shortcode_end ='[/accordions]';

                $uniqueID = uniqid();

                $output .= '<div id="food-tabs-container-'.$uniqueID.'" class="food-tabs-wrap food-column clearfix">';
                $output .= '<ul class="food-list '.$food_column.' clearfix">';

                if (have_posts()):
                            while (have_posts()):
                                the_post();
                                //echo $type, $food_type;
                                $custom = get_post_custom(get_the_ID());
                                $food_cats = get_the_terms(get_the_ID(), 'foodcategory');
                                
                                $protected  = "";
                                $icon_class = "column-gridblock-icon";
                                $food_total_count++;
                                
                                $animation_class = '';
                                if ($animated == "true") {
                                    $animation_class = ' animation-standby-food animated fadeInUpSlow';
                                }

                                $pagemeta_price_food = '';
                                $pagemeta_new_food = 'no';
                                $pagemeta_chef_food = 'no';
                                $pagemeta_hide_food = 'no';

                                if (isset($custom['pagemeta_price_food'][0])) {
                                    $pagemeta_price_food = $custom['pagemeta_price_food'][0];
                                }
                                if (isset($custom['pagemeta_new_food'][0])) {
                                    $pagemeta_new_food = $custom['pagemeta_new_food'][0];
                                }
                                if (isset($custom['pagemeta_price_food'][0])) {
                                    $pagemeta_price_food = $custom['pagemeta_price_food'][0];
                                }
                                if (isset($custom['pagemeta_chef_food'][0])) {
                                    $pagemeta_chef_food = $custom['pagemeta_chef_food'][0];
                                }
                                if (isset($custom['pagemeta_hide_food'][0])) {
                                    $pagemeta_hide_food = $custom['pagemeta_hide_food'][0];
                                }
                                $food_image = '';
                                $food_image_status = false;
                                $food_image_class = '';
                                if ($item_image=="yes") {
                                    if ( has_post_thumbnail() ) {
                                        $post_thumbnail_id = get_post_thumbnail_id( get_the_id() );
                                        $image_data = wp_get_attachment_image_src( $post_thumbnail_id, 'cinnamon-restaurant-image-square-big', false );
                                        $image_url = $image_data[0];
                                        $food_image = '<div class="food-item-image"><img src="'.esc_url($image_url).'" alt="food-item" /></div>';
                                        $food_image_status = true;
                                        $food_image_class = "food-image-active";
                                    }
                                }

                                if ($pagemeta_hide_food<>"yes") {

                                    $parity = " food-column-".$food_count;

                                    if ($columns == '2' ) {
                                        if ($food_count == 2 ) {
                                            $food_count = 0;
                                        }
                                    }
                                    if ($columns == '3' ) {
                                        if ($food_count == 3 ) {
                                            $food_count = 0;
                                        }
                                    }

                                    $output .= '<li class="food-element food-element-id-' . get_the_ID() . ' '.$food_image_class.$parity.$food_column_clearfix.' food-element-order-' . $food_total_count . '" data-food="food-' . get_the_ID() . '">';
                                        $output .= '<div class="food-item-wrap-outer">';
                                        $output .= $food_image;
                                        $food_count++;
                                        $output .= '<div class="food-item-wrap">';
                                            if ($pagemeta_chef_food=="yes") {
                                                $output .= '<div class="food-notice food-chef-recommended">'.imaginem_codepack_food_label('food_chef_recommended').'</div>';
                                            }
                                            if ($pagemeta_new_food=="yes") {
                                                $output .= '<div class="food-notice food-new-item">'.imaginem_codepack_food_label('food_new_item').'</div>';
                                            }
                                            $output .= imaginem_codepack_display_custom_food_labels( get_the_id() );
                                            $output .= '<div class="food-item-header clearfix">';
                                                $output .= '<h3 class="food-item-title">'.get_the_title().'</h3>';
                                                $output .= imaginem_codepack_get_food_prices( get_the_ID() );
                                            
                                            $output .= '</div>';
                                            $output .= '<div class="food-item-description">'.get_the_excerpt().'</div>';

                                        $output .= '</div>';
                                        $output .= '</div>';
                                    $output .= '</li>';
                                }
                            endwhile;
                        endif;

                        wp_reset_query();

                        $output .= '</ul>';
                        $output .= '</div>';
                        //$output = trim(preg_replace('/\s\s+/', ' ', $output));
                        //$output = preg_replace('/<p\b[^>]*>(.*?)<\/p>/i', '', $output);
                        //$output = str_replace(array("\n\r", "\n", "\r"), '', $output );
                        
                        $tabs .= '[accordion title="'.esc_attr($term_title).'"]'.$output.'[/accordion]';
            }
        }

        $tab_shortcode = '<div class="food-tabs-outer">';
        $tab_shortcode .= $tab_shortcode_start . $tabs . $tab_shortcode_end;
        $tab_shortcode .= '</div>';
        // echo '<pre>';
        // print_r($tab_shortcode);
        // echo '</pre>';
        
        $idCount          = 1;
        $food_count       = 0;
        $food_total_count = 0;
        $foodfilters      = array();

        

        $output .= '<h2 class="food-main-title">'.$title.'</h2>';
        if ( $content<>"" ) {
            $output .= '<div class="entry-content">';
            $output .= wpautop( html_entity_decode($content) );
            $output .= '</div>';
        }

        $output = do_shortcode($tab_shortcode);
        return $output;
    }
}
add_shortcode("foodaccordion", "mFoodAccordion");
?>