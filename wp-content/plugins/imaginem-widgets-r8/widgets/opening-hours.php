<?php
class mTheme_OpeningHours_Widget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'mtheme_openinghours_widget', 'description' => __( 'Display your opening hours', 'mthemelocal') );
		parent::__construct('openinghours_details',__('Cinnamon Restaurant Opening Hours', 'mthemelocal'), $widget_ops);
		
	}
	
	public function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters('widget_title', empty($instance['title']) ? __('', 'mthemelocal') : $instance['title'], $instance, $this->id_base);
		$text = $instance['text'];
		$mon_open = $instance['mon_open'];
		$mon_closed = $instance['mon_closed'];
		$tue_open = $instance['tue_open'];
		$tue_closed = $instance['tue_closed'];
		$wed_open = $instance['wed_open'];
		$wed_closed = $instance['wed_closed'];
		$thu_open = $instance['thu_open'];
		$thu_closed = $instance['thu_closed'];
		$fri_open = $instance['fri_open'];
		$fri_closed = $instance['fri_closed'];
		$sat_open = $instance['sat_open'];
		$sat_closed = $instance['sat_closed'];
		$sun_open = $instance['sun_open'];
		$sun_closed = $instance['sun_closed'];
		
		echo $before_widget;
		if ( $title)
			echo $before_title . $title . $after_title;
		
		?>
			<ul class="opening-hours">
			<?php if(!empty($text)):?><li class="about_info"><?php echo $text;?></li><?php endif;?>
			<?php if(!empty($mon_open)):?><li class="mon-open"><?php echo $mon_open;?></li><?php endif;?>
			<?php if(!empty($mon_closed)):?><li class="mon-closed"><?php echo $mon_closed;?></li><?php endif;?>
			<?php if(!empty($tue_open)):?><li class="tue-open"><?php echo $tue_open;?></li><?php endif;?>
			<?php if(!empty($tue_closed)):?><li class="tue-closed"><?php echo $tue_closed;?></li><?php endif;?>
			<?php if(!empty($wed_open)):?><li class="wed-open"><?php echo $wed_open;?></li><?php endif;?>
			<?php if(!empty($wed_closed)):?><li class="wed-closed"><?php echo $wed_closed;?></li><?php endif;?>
			<?php if(!empty($thu_open)):?><li class="thu-open"><?php echo $thu_open;?></li><?php endif;?>
			<?php if(!empty($thu_closed)):?><li class="thu-closed"><?php echo $thu_closed;?></li><?php endif;?>
			<?php if(!empty($fri_open)):?><li class="fri-open"><?php echo $fri_open;?></li><?php endif;?>
			<?php if(!empty($fri_closed)):?><li class="fri-closed"><?php echo $fri_closed;?></li><?php endif;?>
			<?php if(!empty($sat_open)):?><li class="sat-open"><?php echo $sat_open;?></li><?php endif;?>
			<?php if(!empty($sat_closed)):?><li class="sat-closed"><?php echo $sat_closed;?></li><?php endif;?>
			<?php if(!empty($sun_open)):?><li class="sun-open"><?php echo $sun_open;?></li><?php endif;?>
			<?php if(!empty($sun_closed)):?><li class="sun-closed"><?php echo $sun_closed;?></li><?php endif;?>
			</ul>
		<?php
		echo $after_widget;

	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['text'] = strip_tags($new_instance['text']);

		$instance['mon_open'] = strip_tags($new_instance['mon_open']);
		$instance['mon_closed'] = strip_tags($new_instance['mon_closed']);
		$instance['tue_open'] = strip_tags($new_instance['tue_open']);
		$instance['tue_closed'] = strip_tags($new_instance['tue_closed']);
		$instance['wed_open'] = strip_tags($new_instance['wed_open']);
		$instance['wed_closed'] = strip_tags($new_instance['wed_closed']);
		$instance['thu_open'] = strip_tags($new_instance['thu_open']);
		$instance['thu_closed'] = strip_tags($new_instance['thu_closed']);
		$instance['fri_open'] = strip_tags($new_instance['fri_open']);
		$instance['fri_closed'] = strip_tags($new_instance['fri_closed']);
		$instance['sat_open'] = strip_tags($new_instance['sat_open']);
		$instance['sat_closed'] = strip_tags($new_instance['sat_closed']);
		$instance['sun_open'] = strip_tags($new_instance['sun_open']);
		$instance['sun_closed'] = strip_tags($new_instance['sun_closed']);
		

		return $instance;
	}

	public function form( $instance ) {
		//Defaults
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$text = isset($instance['text']) ? esc_attr($instance['text']) : '';

		$mon_open = isset($instance['mon_open']) ? esc_attr($instance['mon_open']) : '';
		$mon_closed = isset($instance['mon_closed']) ? esc_attr($instance['mon_closed']) : '';

		$tue_open = isset($instance['tue_open']) ? esc_attr($instance['tue_open']) : '';
		$tue_closed = isset($instance['tue_closed']) ? esc_attr($instance['tue_closed']) : '';

		$wed_open = isset($instance['wed_open']) ? esc_attr($instance['wed_open']) : '';
		$wed_closed = isset($instance['wed_closed']) ? esc_attr($instance['wed_closed']) : '';

		$thu_open = isset($instance['thu_open']) ? esc_attr($instance['thu_open']) : '';
		$thu_closed = isset($instance['thu_closed']) ? esc_attr($instance['thu_closed']) : '';

		$fri_open = isset($instance['fri_open']) ? esc_attr($instance['fri_open']) : '';
		$fri_closed = isset($instance['fri_closed']) ? esc_attr($instance['fri_closed']) : '';

		$sat_open = isset($instance['sat_open']) ? esc_attr($instance['sat_open']) : '';
		$sat_closed = isset($instance['sat_closed']) ? esc_attr($instance['sat_closed']) : '';

		$sun_open = isset($instance['sun_open']) ? esc_attr($instance['sun_open']) : '';
		$sun_closed = isset($instance['sun_closed']) ? esc_attr($instance['sun_closed']) : '';
	?>

		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Intro text:', 'mthemelocal'); ?></label>
		<textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" type="text" ><?php echo esc_textarea($text); ?></textarea></p>
		
		<p><label for="<?php echo $this->get_field_id('mon_open'); ?>"><?php _e('Monday Open:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('mon_open'); ?>" name="<?php echo $this->get_field_name('mon_open'); ?>" type="text" value="<?php echo esc_attr($mon_open); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('mon_closed'); ?>"><?php _e('Monday Closed:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('mon_closed'); ?>" name="<?php echo $this->get_field_name('mon_closed'); ?>" type="text" value="<?php echo esc_attr($mon_closed); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('tue_open'); ?>"><?php _e('Tuesday Open:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('tue_open'); ?>" name="<?php echo $this->get_field_name('tue_open'); ?>" type="text" value="<?php echo esc_attr($tue_open); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('tue_closed'); ?>"><?php _e('Tuesday Closed:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('tue_closed'); ?>" name="<?php echo $this->get_field_name('tue_closed'); ?>" type="text" value="<?php echo esc_attr($tue_closed); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('wed_open'); ?>"><?php _e('Wednesday Open:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('wed_open'); ?>" name="<?php echo $this->get_field_name('wed_open'); ?>" type="text" value="<?php echo esc_attr($wed_open); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('wed_closed'); ?>"><?php _e('Wednesday Closed:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('wed_closed'); ?>" name="<?php echo $this->get_field_name('wed_closed'); ?>" type="text" value="<?php echo esc_attr($wed_closed); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('thu_open'); ?>"><?php _e('Thursday Open:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('thu_open'); ?>" name="<?php echo $this->get_field_name('thu_open'); ?>" type="text" value="<?php echo esc_attr($thu_open); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('thu_closed'); ?>"><?php _e('Thursday Closed:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('thu_closed'); ?>" name="<?php echo $this->get_field_name('thu_closed'); ?>" type="text" value="<?php echo esc_attr($thu_closed); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('fri_open'); ?>"><?php _e('Friday Open:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('fri_open'); ?>" name="<?php echo $this->get_field_name('fri_open'); ?>" type="text" value="<?php echo esc_attr($fri_open); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('fri_closed'); ?>"><?php _e('Friday Closed:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('fri_closed'); ?>" name="<?php echo $this->get_field_name('fri_closed'); ?>" type="text" value="<?php echo esc_attr($fri_closed); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('sat_open'); ?>"><?php _e('Saturday Open:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('sat_open'); ?>" name="<?php echo $this->get_field_name('sat_open'); ?>" type="text" value="<?php echo esc_attr($sat_open); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('sat_closed'); ?>"><?php _e('Saturday Closed:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('sat_closed'); ?>" name="<?php echo $this->get_field_name('sat_closed'); ?>" type="text" value="<?php echo esc_attr($sat_closed); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('sun_open'); ?>"><?php _e('Sunday Open:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('sun_open'); ?>" name="<?php echo $this->get_field_name('sun_open'); ?>" type="text" value="<?php echo esc_attr($sun_open); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('sun_closed'); ?>"><?php _e('Sunday Closed:', 'mthemelocal'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('sun_closed'); ?>" name="<?php echo $this->get_field_name('sun_closed'); ?>" type="text" value="<?php echo esc_attr($sun_closed); ?>" /></p>
<?php
	}

}
function mTheme_OpeningHours_register_widgets() {
	register_widget( 'mTheme_OpeningHours_Widget' );
}
add_action( 'widgets_init', 'mTheme_OpeningHours_register_widgets' );