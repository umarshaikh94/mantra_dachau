<?php
/** OpenTable **/
if(!class_exists('em_opentable')) {
		class em_opentable extends AQ_Block {

		protected $the_options;

		//set and create block
		function __construct() {
			$block_options = array(
				'pb_block_icon' => 'simpleicon-size-fullscreen',
				'pb_block_icon_color' => '#77DD77',
				'name' => __('OpenTable','mthemelocal'),
				'size' => 'span6',
				'tab' => __('Food','mthemelocal'),
				'desc' => __('Add OpenTable form','mthemelocal')
			);

			/*-----------------------------------------------------------------------------------*/
			/*	Lightbox Image/Video
			/*-----------------------------------------------------------------------------------*/

			$mtheme_shortcodes['opentableform'] = array(
				'no_preview' => true,
				'shortcode_desc' => __('Display OpenTable', 'mthemelocal'),
				'params' => array(
					'title' => array(
						'std' => '',
						'type' => 'text',
						'label' => __('OpenTable form title', 'mthemelocal'),
						'desc' => __('OpenTable form title', 'mthemelocal'),
					),
				),
				'shortcode' => '[opentableform title="{{title}}"]',
				'popup_title' => __('Add OpenTable', 'mthemelocal')
			);

			$this->the_options = $mtheme_shortcodes['opentableform'];

			//create the block
			parent::__construct('em_opentable', $block_options);
			// Any script registers need to uncomment following line
			//add_action('mtheme_aq-page-builder-admin-enqueue', array($this, 'admin_enqueue_scripts'));
		}

		function form($instance) {
			$instance = wp_parse_args($instance);

			echo mtheme_generate_builder_form($this->the_options,$instance);
			//extract($instance);
		}

		function block($instance) {

		    wp_enqueue_script('flatpickr');
		    wp_enqueue_style('flatpickr');
		    wp_enqueue_script('chosen');
		    wp_enqueue_style('chosen');

			extract($instance);

			$shortcode = mtheme_dispay_build($this->the_options,$block_id,$instance);

			echo do_shortcode($shortcode);
			
		}
		public function admin_enqueue_scripts(){
			//Any script registers go here
		}

	}
}