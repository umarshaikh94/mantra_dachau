<?php
/* Multimap */
if(!class_exists('em_multigooglemap')) {
	class em_multigooglemap extends AQ_Block {

		protected $the_options;
		protected $the_child_options;

		function __construct() {
			$block_options = array(
				'pb_block_icon' => 'simpleicon-pointer',
				'pb_block_icon_color' => '#836953',
				'name' => __('Google Map','mthemelocal'),
				'size' => 'span6',
				'tab' => __('Elements','mthemelocal'),
				'desc' => __('Add an mapmarker tab set','mthemelocal')
			);
			//create the widget

			$mtheme_shortcodes['multimap'] = array(
			    'params' => array(),
			    'no_preview' => true,
			    'shortcode_desc' => __('Add multimap shortcode. You can add multiple mapmarker tab sections within this generator.', 'mthemelocal'),
			    'shortcode' => '[multimap zoom="{{zoom}}" height="{{height}}" map_marker_image="{{map_marker_image}}"] {{child_shortcode}}  [/multimap]',
			    'popup_title' => __('Insert Multimap Shortcode', 'mthemelocal'),
			 	'params' => array(
			        'height' => array(
			            'std' => '',
			            'type' => 'text',
			            'label' => __('Map Height', 'mthemelocal'),
			            'desc' => __('Map Height', 'mthemelocal'),
			        ),
		            'zoom' => array(
		                'std' => '6',
		                'type' => 'text',
		                'label' => __('Zoom level ( 0 to 21 )', 'mthemelocal'),
		                'desc' => __('Zoom level ( 0 to 21 )', 'mthemelocal'),
		            ),
			        'map_marker_image' => array(
			            'std' => '',
			            'type' => 'uploader',
			            'label' => __('Image as marker', 'mthemelocal'),
			            'desc' => __('Image as marker', 'mthemelocal'),
			        ),
				),
			    'child_shortcode' => array(
			        'params' => array(
			            'title' => array(
			                'std' => 'Title',
			                'type' => 'text',
			                'label' => __('Title', 'mthemelocal'),
			                'desc' => __('Title', 'mthemelocal'),
			            ),
			            'address' => array(
			                'std' => '',
			                'type' => 'text',
			                'label' => __('Address', 'mthemelocal'),
			                'desc' => __('Address', 'mthemelocal'),
			            ),
			            'longitude' => array(
			                'std' => '',
			                'type' => 'text',
			                'label' => __('Longitude', 'mthemelocal'),
			                'desc' => __('Longitude', 'mthemelocal'),
			            ),
			            'latitude' => array(
			                'std' => '',
			                'type' => 'text',
			                'label' => __('Latitude', 'mthemelocal'),
			                'desc' => __('Latitude', 'mthemelocal')
			            )
			        ),
			        'title_field' => 'title',
			        'shortcode' => '[mapmarker title={{title}} address="{{address}}" longitude="{{longitude}}" latitude="{{latitude}}"]',
			        'clone_button' => __('+ Add Another Mapmarker', 'mthemelocal')
			    )
			);
			$this->the_options = $mtheme_shortcodes['multimap'];
			$this->the_child_options = $mtheme_shortcodes['multimap']['child_shortcode'];

			parent::__construct('em_multigooglemap', $block_options);

			//add ajax functions
			add_action('wp_ajax_aq_block_mapmarker_add_new', array($this, 'add_mapmarker_tab'));
		}

		function form($instance) {

			$defaults = array(
				'tabs' => array(
					1 => array(
						'title' => __('New Mapmarker','mthemelocal'),
						'address' => __('','mthemelocal'),
						'longitude' => __('','mthemelocal'),
						'latitude' => __('','mthemelocal'),
					)
				),
				'type'	=> 'tab',
				'entrance_animation' => ''
			);

			$instance = wp_parse_args($instance, $defaults);
			extract($instance);
			?>
			<div class="description cf">
			<?php
			echo mtheme_generate_builder_form($this->the_options,$instance);
			?>
			
				<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
					<?php
					$tabs = is_array($tabs) ? $tabs : $defaults['tabs'];
					$count = 1;
					foreach($tabs as $tab) {
						$this->tab($tab, $count);
						$count++;
					}
					?>
				</ul>
				<p></p>
				<a href="#" rel="mapmarker" class="aq-sortable-add-new button"><?php _e('Add New','mthemelocal'); ?></a>
				<p></p>
			</div>
			<?php
		}

		function tab($tab = array(), $count = 0) {

			$child_field_id = $this->get_field_id('tabs');
			$child_field_name = $this->get_field_name('tabs');
			$child_count = $count;

			mtheme_create_tab( $this->the_child_options, $tab, $child_count, $child_field_id, $child_field_name );

		}

		function block($instance) {
			extract($instance);

			wp_enqueue_script ('googlemaps-api');
			wp_enqueue_script ('jquery-gomap');
			$this->tabbed_method();

$markers = $instance['tabs'];
$zoom = $instance['mtheme_zoom'];
$map_markers = '';
foreach ($markers as $key => $value) {

	if ( isSet($value['address']) && $value['address']<>""  ) {
		$marker_address = "address: '".esc_js($value['address'])."',";
	} else {
		$marker_address = "latitude: ".$value['latitude'].","; 
		$marker_address .= "longitude:".$value['longitude'].",";
	}
	$map_markers .= "{
		".$marker_address."
		icon: '".$instance['mtheme_map_marker_image']."',
		html: { 
		    content: '".$value['title']."',
		    popup: true 
		}
	},";
}
$map_markers = substr($map_markers, 0, -1);

$map_js = "jQuery(function($){	
	if ($.fn.goMap) {
    $('#mtheme-block-".$instance['number'].".mtheme-block-em_multigooglemap .map').goMap({
        mapTypeControl: false,
		navigationControl: true, 
        navigationControlOptions: { 
            position: 'BOTTOM_LEFT', 
            style: 'ZOOM_PAN' 
        },
        zoom: ".$zoom.",
        markers: [".$map_markers."],
        scrollwheel: false,
        scaleControl: true,
        maptype: 'ROADMAP',
        hideByClick:   false,
        oneInfoWindow: true
    });
}
});";

			wp_add_inline_script('jquery-gomap',$map_js);

			$shortcode = mtheme_dispay_build($this->the_options,$block_id,$instance);
			// echo '<pre>'.$shortcode.'</pre>';
			echo do_shortcode($shortcode);
		}
		function tabbed_method() {
		}
		/* AJAX add tab */
		function add_mapmarker_tab() {

			$count = isset($_POST['count']) ? absint($_POST['count']) : false;
			$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'mtheme-block-9999';

			//default key/value for the tab
			$tab = array(
				'title' => __('New Mapmarker','mthemelocal'),
				'address' => __('','mthemelocal'),
				'longitude' => __('','mthemelocal'),
				'latitude' => __('','mthemelocal'),
			);

			if($count) {
				$this->tab($tab, $count);
			} else {
				die(-1);
			}

			die();
		}

		function update($new_instance, $old_instance) {
			$new_instance = mtheme_recursive_sanitize($new_instance);
			return $new_instance;
		}
	}
}
