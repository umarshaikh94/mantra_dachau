<?php
/** Food Grid **/
if(!class_exists('em_food_list')) {
		class em_food_list extends AQ_Block {

		protected $the_options;
		protected $food_tax;

		function init_reg() {
			$the_list = get_terms('foodcategory');
			//print_r($the_list);
			// Pull all the Food Categories into an array
			if ($the_list) {
				$food_categories=array();
				//$food_categories[0]="All the items";
				foreach($the_list as $key => $list) {
					if (isSet($list->slug)) {
						$food_categories[$list->slug] = $list->name;
					}
				}
			} else {
				$food_categories[0]="Food Categories not found.";
			}
			$this->food_store($food_categories);

		}

		function food_store($food_categories) {
			$this->the_options['category_list'] = $food_categories;
		}

		//set and create block
		function __construct() {
			$block_options = array(
				'pb_block_icon' => 'simpleicon-grid',
				'pb_block_icon_color' => '#836953',
				'name' => __('Food List','mthemelocal'),
				'size' => 'span12',
				'tab' => __('Food','mthemelocal'),
				'desc' => __('Generate a Food Grid','mthemelocal')
			);
			add_action('init', array(&$this, 'init_reg'));

			$mtheme_shortcodes['foodlist'] = array(
				'no_preview' => true,
				'shortcode_desc' => __('A Grid based list of food items.', 'mthemelocal'),
				'params' => array(
					'foodtype_slugs' => array(
						'type' => 'category_list',
						'std' => '',
						'label' => __('Choose Food types to list', 'mthemelocal'),
						'desc' => __('Leave blank to list all. Enter comma seperated food type categories. eg. mains,dessert,dinner ', 'mthemelocal'),
						'options' => ''
					),
					'animated' => array(
						'type' => 'select',
						'label' => __('Animate List', 'mthemelocal'),
						'desc' => __('Animate List', 'mthemelocal'),
						'options' => array(
							'false' => 'No',
							'true' => 'Yes'
						)
					),
					'columns' => array(
						'type' => 'select',
						'label' => __('Columns', 'mthemelocal'),
						'desc' => __('No. of Columns', 'mthemelocal'),
						'options' => array(
							'1' => '1',
							'2' => '2',
						)
					),
			        'title' => array(
			            'std' => '',
			            'type' => 'text',
			            'label' => __('Title', 'mthemelocal'),
			            'desc' => __('Title for list', 'mthemelocal'),
			        ),
					'padding' => array(
						'std' => '0',
						'type' => 'text',
						'label' => __('Padding', 'mthemelocal'),
						'desc' => __('Padding around menu','mthemelocal')
					),
					'button_url' => array(
						'std' => '',
						'type' => 'text',
						'label' => __('Button url', 'mthemelocal'),
						'desc' => __('Button url','mthemelocal')
					),
					'button_text' => array(
						'std' => '',
						'type' => 'text',
						'label' => __('Button text', 'mthemelocal'),
						'desc' => __('Button text','mthemelocal')
					),
					'background_color' => array(
						'std' => '',
						'type' => 'color',
						'label' => __('Background color', 'mthemelocal'),
						'desc' => __('Background color', 'mthemelocal'),
					),
			        'content' => array(
			            'std' => '',
			            'textformat' => 'richtext',
			            'type' => 'editor',
			            'label' => __('Text', 'mthemelocal'),
			            'desc' => __('Text', 'mthemelocal'),
			        ),
					'item_image' => array(
						'type' => 'select',
						'label' => __('Item Image', 'mthemelocal'),
						'desc' => __('Item Image', 'mthemelocal'),
						'options' => array(
							'no' => 'No',
							'yes' => 'Yes'
						)
					),
					'limit' => array(
						'std' => '-1',
						'type' => 'text',
						'label' => __('Limit. -1 for unlimited', 'mthemelocal'),
						'desc' => __('Limit items. -1 for unlimited', 'mthemelocal'),
					)
				),
				'shortcode' => '[foodlist title="{{title}}" animated={{animated}} padding="{{padding}}" background_color="{{background_color}}" button_url="{{button_url}}" button_text="{{button_text}}" foodtype_slugs="{{foodtype_slugs}}" item_image="{{item_image}}" columns={{columns}} limit="{{limit}}"]{{content}}[/foodlist]',
				'popup_title' => __('Insert Food List', 'mthemelocal')
			);
			$this->the_options = $mtheme_shortcodes['foodlist'];

			//create the block
			parent::__construct('em_food_list', $block_options);
			// Any script registers need to uncomment following line
			//add_action('mtheme_aq-page-builder-admin-enqueue', array($this, 'admin_enqueue_Block'));
			
		}

		function form($instance) {
			$instance = wp_parse_args($instance);

			echo mtheme_generate_builder_form($this->the_options,$instance);
			//extract($instance);
		}

		function block($instance) {
			extract($instance);
			$shortcode = mtheme_dispay_build($this->the_options,$block_id,$instance);

			echo do_shortcode($shortcode);
			
		}
		function mtheme_enqueue_em_food_list(){
			//Any script registers go here
		}

	}
}